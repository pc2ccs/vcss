@echo off
rem  REM Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
rem 
rem Purpose: to be called by the other scripts to setup the environment
rem

rem Change these (& uncomment) for non-standard installations
rem set libdir=..\lib

rem try development locations first
if exist %0\..\..\dist\vcss.jar set libdir=%0\..\..\dist

rem then try the distribution locations
if exist %0\..\..\lib\vcss.jar set libdir=%0\..\..\lib

if x%libdir% == x goto nolibdir

set vcss_classpath=%libdir%\vcss.jar

goto end

:nolibdir
echo Could not find vcss.jar, please check your installation
rem XXX we really want to do a break here
pause
goto end

:end
rem continue what you were doing...

rem eof vcssenv.bat 
