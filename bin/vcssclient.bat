@echo off
rem  REM Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
rem  
rem  Purpose: start VCSS client, aka RunVCSSClient
rem 

set MAIN=org.icpc.vcss.client.RunVCSSClient

set OPTS=
rem default testing option
if %1@ == -test@ set OPTS=localhost:50443 
if %1@ == -test@ shift

rem
rem Set the default Heap Size for 32-bit and 64-bit Java
rem
set Java64bit_HeapSize=4G
set Java32bit_HeapSize=768M


rem
rem Windows 2000 and beyond syntax
set VCSSBIN=%~dp0
if exist %VCSSBIN%\vcssenv.bat goto continue

rem fallback to path (or current directory)
set VCSSBIN=%0\..
if exist %VCSSBIN%\vcssenv.bat goto continue

rem parent of bin dir
set VCSSBIN=bin
if exist %VCSSBIN%\vcssenv.bat goto continue

rem else rely on PVCSSSTALL variable
set VCSSBIN=%PVCSSSTALL%\bin
if exist %VCSSBIN%\vcssenv.bat goto continue

echo.
echo ERROR: Could not locate scripts.
echo.
echo Please set the variable PVCSSSTALL to the location of
echo   the VERSION file (ex: c:\vcss-0.90-alpha-227) 
echo.
pause
goto end

:continue
call %VCSSBIN%\vcssenv.bat

:CheckJavaVersion
REM Check Java architecture and set max Heap size 
java -Djdk.crypto.KeyAgreement.legacyKDF=true -d64 -version 2>&1 | findstr "Error" > NUL
IF ERRORLEVEL 1 goto :Java64
IF ERRORLEVEL 0 goto :Java32

:Java64
set HeapParam=-Xmx%Java64bit_HeapSize%
goto startit

:Java32
set HeapParam=-Xmx%Java32bit_HeapSize%

IF "%PROCESSOR_ARCHITECTURE%"=="x86" goto :OS32
goto OS64

:OS64
cls
Echo.
Echo WARNING: We detected a 32 bit java running on a 64 bit OS.
Echo.
Echo We recommend that you run PC^2 Server using a 64 bit version of Java to allow
Echo for the best results.
:OS32
Echo.
Echo Note: We have set the Java Heap size to %Java32bit_HeapSize% for your 32-bit Java system,
echo but this may be too small to run the PC^2 Server. 
Echo.
echo You can increase the Heap size to whatever the maximum allowed on your machine 
echo is by editing the Java32bit_HeapSize constant at the start of the 
echo "vcssserver.bat" file.
goto end

:startit
java -Djdk.crypto.KeyAgreement.legacyKDF=true %HeapParam% -cp "%libdir%\*" %MAIN% %OPTS% %1 %2 %3 %4 %5 %6 %7 %8 %9

rem  --------------------
:end
rem  eof vcssserver.bat
