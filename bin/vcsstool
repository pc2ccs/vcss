#!/bin/sh
# Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
#
# Purpose: run the VCSS tools
#

if [ -f VERSION ] ; then
  VERSION=`grep Version VERSION`
elif [ -f ../VERSION ] ; then
  VERSION=`grep Version ../VERSION`
else
  VERSION="Missing VERSION file";
fi

MAIN=org.icpc.vcss.database.DBInfo

# ------------------------------------------------------------
usage()
{
    cat <<SAGE
Usage: $0 [-h] [options]

Purpose: Run vcss system tools

-sdb   run org.icpc.vcss.database.SetupDatabase to define new database tables

-load   run org.icpc.vcss.util.LoadAndUpdateDatabase to load new data into the database

-mkenv  run org.icpc.vcss.util.CreateAWSEnvironment to create an AWS Virtual Private Cloud network/environment

-mkmach run org.icpc.vcss.util.CreateAWSMachineInstances to create a set of AWS machines

-rmdata  run org.icpc.vcss.util.RemoveAllDataFromDatabase  to remove all database data (very dangerous)

-info   run org.icpc.vcss.database.DBInfo ( default )   to display information about current database contents

-ctrlmach run org.icpc.vcss.util.ControlAWSMachineInstances to stop/start AWS machines

-dnslist  run org.icpc.vcss.util.CreateDNSUpdateList  to generate a list of cloud machine hostname/IP for DNS updating

Use --help to show usage for utilities, ex. vcsstool -mkmach --help

$VERSION

SAGE
	exit 4
}
# ------------------------------------------------------------

# ------------------------------------------------------------
# Handle command line
# ------------------------------------------------------------

ARG=$1

case $ARG in 
  -help|-h|-usage|--help) 
          usage
          exit 0
    ;;
esac

while [ "$ARG" != "" ] ; do

	case $ARG in 

		-sdb) 
			MAIN=org.icpc.vcss.database.SetupDatabase
			;;

		-load) 
			MAIN=org.icpc.vcss.util.LoadAndUpdateDatabase
			;;
	
    	-ctrlmach)
			MAIN=org.icpc.vcss.util.ControlAWSMachineInstances
			;;

		-rmdata) 
			MAIN=org.icpc.vcss.util.RemoveAllDataFromDatabase
			;;
			
		-mkmach) 
			MAIN=org.icpc.vcss.util.CreateAWSMachineInstances
			;;

		-mkenv) 
			MAIN=org.icpc.vcss.util.CreateAWSEnvironment
			;;

		-dnslist) 
			MAIN=org.icpc.vcss.util.CreateDNSUpdateList
			;;

		*) ARG=""
			;;

	esac

	if [ "$ARG" != "" ] ; then
		shift 
		ARG=$1
	fi

done

# ------------------------------------------------------------
# MAIN 
# ------------------------------------------------------------

. `dirname $0`/vcssenv

# MacOS or not
if [ -f /mach_kernel ]; then
  # set our dock name (otherwise defaults to Starter)
  PC2XOPS="-Xdock:name=`basename $0`"
else
  PC2XOPS=""
fi

java $PC2XOPS -Xms64M -Xmx768M -cp "$vcss_classpath" $MAIN $*

# vi ts=3:sw=3:ai:ic
# eof vcssver
# ------------------------------------------------------------
