@echo off
rem Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
rem
rem Purpose: run the VCSS tools
rem 

if %1@ == -h@ goto usage
if %1@ == --help@ goto usage

rem
rem Set the default Heap Size for 32-bit and 64-bit Java
rem
set Java64bit_HeapSize=4G
set Java32bit_HeapSize=768M

set MAIN=org.icpc.vcss.database.DBInfo
if %1@ == -sdb@  set MAIN=org.icpc.vcss.database.SetupDatabase
if %1@ == -sdb@  shift

if %1@ == -load@ set MAIN=org.icpc.vcss.util.LoadAndUpdateDatabase
if %1@ == -load@  shift

if %1@ == -mkmach@ set MAIN=org.icpc.vcss.util.CreateAWSMachineInstances
if %1@ == -mkmach@  shift

if %1@ == -mkenv@ set MAIN=org.icpc.vcss.util.CreateVirtualPrivateCloudNetwork
if %1@ == -mkenv@  shift

if %1@ == -ctrlmach@ set MAIN=org.icpc.vcss.util.ControlAWSMachineInstances
if %1@ == -ctrlmach@  shift

if %1@ == -info@ set MAIN=org.icpc.vcss.database.DBInfo
if %1@ == -info@  shift

if %1@ == -rmdata@ set MAIN=org.icpc.vcss.util.RemoveAllDataFromDatabase 
if %1@ == -rmdata@  shift

if %1@ == -dnslist@ set MAIN=org.icpc.vcss.util.CreateDNSUpdateList 
if %1@ == -dnslist@  shift

rem
rem Windows 2000 and beyond syntax
set VCSSBIN=%~dp0
if exist %VCSSBIN%\vcssenv.bat goto continue

rem fallback to path (or current directory)
set VCSSBIN=%0\..
if exist %VCSSBIN%\vcssenv.bat goto continue

rem parent of bin dir
set VCSSBIN=bin
if exist %VCSSBIN%\vcssenv.bat goto continue

rem else rely on VCSSINSTALL variable
set VCSSBIN=%VCSSINSTALL%\bin
if exist %VCSSBIN%\vcssenv.bat goto continue

echo.
echo ERROR: Could not locate scripts.
echo.
echo Please set the variable VCSSINSTALL to the location of
echo   the VERSION file (ex: c:\vcss-0.90-alpha-227) 
echo.
pause
goto end

:continue
call %VCSSBIN%\vcssenv.bat

:CheckJavaVersion
REM Check Java architecture and set max Heap size 
java -Djdk.crypto.KeyAgreement.legacyKDF=true -d64 -version 2>&1 | findstr "Error" > NUL
IF ERRORLEVEL 1 goto :Java64
IF ERRORLEVEL 0 goto :Java32

:Java64
set HeapParam=-Xmx%Java64bit_HeapSize%
goto :runtool

:Java32
set HeapParam=-Xmx%Java32bit_HeapSize%

IF "%PROCESSOR_ARCHITECTURE%"=="x86" goto :OS32
goto OS64

rem ------------------------------------------------------------
rem usage

:usage
@echo off
echo.
echo Usage vcsstool.bat [-h]
echo.
echo Purpose: Run vcss system tools
echo.
echo -sdb    run org.icpc.vcss.database.SetupDatabase to define new database tables
echo.
echo -load   run org.icpc.vcss.util.LoadAndUpdateDatabase to load new data into the database
echo.
echo -mkmach run org.icpc.vcss.util.CreateAWSMachineInstances to create a set of AWS machines
echo. 
echo -mkenv  run org.icpc.vcss.util.CreateAWSEnvironment to create an AWS Virtual Private Cloud network/environment
echo. 
echo -info   run org.icpc.vcss.database.DBInfo ( default )   to display information about current database contents
echo.
echo -rmdata  run org.icpc.vcss.util.RemoveAllDataFromDatabase  to remove all database data (very dangerous)
echo.
echo -ctrlmach run org.icpc.vcss.util.ControlAWSMachineInstances  to start/stop AWS machines
echo.
echo -dnslist run org.icpc.vcss.util.CreateDNSUpdateList  to generate a list of cloud machine hostname/IP for DNS updating
echo.
echo Use --help to show usage for utilities, ex. vcsstool -mkmach --help
echo.
goto end1

rem  --------------------
:OS64
cls
Echo.
Echo WARNING: We detected a 32 bit java running on a 64 bit OS.
Echo.
Echo We recommend that you run PC^2 Server using a 64 bit version of Java to allow
Echo for the best results.
:OS32
Echo.
Echo Note: We have set the Java Heap size to %Java32bit_HeapSize% for your 32-bit Java system,
echo but this may be too small to run the PC^2 Server. 
Echo.
echo You can increase the Heap size to whatever the maximum allowed on your machine 
echo is by editing the Java32bit_HeapSize constant at the start of the 
echo "vcssserver.bat" file.
goto end

:runtool
java -Djdk.crypto.KeyAgreement.legacyKDF=true %HeapParam% -cp "%libdir%\*" %MAIN% %1 %2 %3 %4 %5 %6 %7 %8 %9

rem  --------------------
:end
rem eof vcsstool.bat
