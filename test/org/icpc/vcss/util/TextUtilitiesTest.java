package org.icpc.vcss.util;

import static org.junit.Assert.*;

import java.util.Properties;

import org.junit.Test;

public class TextUtilitiesTest {

	/**
	 * Test substituteProperties where no substitutions done.
	 */
	@Test
	public void testNoVars() {

		String input = "The quick brown fox jumped over the lazy dog's tail";
		String actual = TextUtilities.substituteProperties(input, new Properties());

		assertEquals(input, actual);

	}

	/**
	 * Test substituteProperties in string for 3 variables.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testThreeSubs() throws Exception {

		String expected = "The quick brown fox jumped over the lazy dog's tail";

		String input = "The ${q} brown fox jumped over the ${lazy}'s ${wag}";
		Properties properties = new Properties();
		properties.put("q", "quick");
		properties.put("lazy", "lazy dog");
		properties.put("wag", "tail");

		String actual = TextUtilities.substituteProperties(input, properties);

		assertEquals(expected, actual);

	}

	/**
	 * Test substituteProperties when variable at start of string.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testStart() throws Exception {

		String expected = "The quick brown fox jumped over the lazy dog's tail";

		String input = "${the} quick brown fox jumped over the lazy dog's tail";
		Properties properties = new Properties();
		properties.put("the", "The");
		String actual = TextUtilities.substituteProperties(input, properties);

		assertEquals(expected, actual);

	}

	/**
	 * Test substituteProperties where variable is at end of string.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testEnd() throws Exception {

		String expected = "${the} quick brown fox jumped over the lazy dog's tail";

		String input = "${the} quick brown fox jumped over the lazy dog's ${wag}";
		Properties properties = new Properties();
		properties.put("wag", "tail");
		String actual = TextUtilities.substituteProperties(input, properties);

		assertEquals(expected, actual);
	}

	/**
	 * Test substituteProperties for four variables.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testManyVars() throws Exception {

		String expected = "The quick brown fox jumped over the lazy dog's tail";

		String input = "${the} quick brown fox ${Jumped} over the ${l_dog}'s ${wag}";
		Properties properties = new Properties();
		properties.put("the", "The");
		properties.put("Jumped", "jumped");
		properties.put("l_dog", "lazy dog");
		properties.put("wag", "tail");

		String actual = TextUtilities.substituteProperties(input, properties);

		assertEquals(expected, actual);

	}

	/**
	 * Test substituteProperties using a list.
	 * 
	 * @throws Exception
	 */
	@Test
	public void testSubstituteList() throws Exception {

		String[] inData = { //
				"${the} quick brown fox ${Jumped} over the ${l_dog}'s ${wag}", //
				"The quick brown fox jumped over the lazy dog's tail", //

				"No variables found in this line at all.", //
				"No variables found in this line at all.", //

				"No variable values found ${here} ${at} ${All}", //
				"No variable values found ${here} ${at} ${All}", //

				"${the}${Jumped}${wag}${l_dog}", //
				"Thejumpedtaillazy dog", //

		};

		Properties properties = new Properties();
		properties.put("the", "The");
		properties.put("Jumped", "jumped");
		properties.put("l_dog", "lazy dog");
		properties.put("wag", "tail");

		for (int i = 0; i < inData.length; i += 2) {
			String input = inData[i];
			String expected = inData[i + 1];

			String actual = TextUtilities.substituteProperties(input, properties);
			assertEquals(expected, actual);

		}
	}
	
	/**
	 * Test hasVariables
	 * @throws Exception
	 */
	@Test
	public void testhasVariables() throws Exception {
		
		String expected = "The quick brown fox jumped over the lazy dog's tail";
		assertFalse ("No variables should be found", TextUtilities.hasVariables(expected));
		
		String input = "The ${q} brown fox jumped over the ${lazy}'s ${wag}";
		assertTrue ("Variables should be found ",TextUtilities.hasVariables(input));
	}

}
