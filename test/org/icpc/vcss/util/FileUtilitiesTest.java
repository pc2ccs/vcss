package org.icpc.vcss.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Set;

import org.icpc.vcss.database.DatabaseUtilities;
import org.icpc.vcss.entity.*;
import org.junit.Test;

import javax.persistence.EntityManagerFactory;

/**
 * Tests for FileUtilities
 *
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 *
 */
public class FileUtilitiesTest {

	@Test
	public void testparseCSV() {

		String[] data = {
				// single quote used, will replaced with double quote
				"'ABC'", "ABC", //
				"'a','b','c','d'", "a,b,c,d", //
				"'matchine_prefix','number','password','password','password'",
				"matchine_prefix,number,password,password,password", //
				"'matchine_prefix','number','password'", "matchine_prefix,number,password", //
				"'team', 30, 'secret1', 'secret2', 'secret3'", "team, 30, secret1, secret2, secret3", //
				"'aj', 1, 'ajsecret1'", "aj, 1, ajsecret1", //
				"'server',,'josh'", "server,,josh", //
		};

		for (int i = 0; i < data.length; i += 2) {

			String input = data[i].replaceAll("'", "\"");

			String[] expected = data[i + 1].split(",");
			String[] actual = FileUtilities.parseCSV(input);

			assertEquals("Expected fields", expected.length, actual.length);
		}

	}

	@Test
	public void testparseCSVLine() throws Exception {

		// public static Machine parseLine(String line) throws Exception {

		String input = "\"team\", 30, \"secret1\", \"secret2\", \"secret3\", \"team, 30, secret1, secret2, secret3";
		Machine def = FileUtilities.parseCSVLine(input);
		assertEquals("def name ", "team30", def.getName());

		input = "\"aj\", 1, \"ajsecret1\"";
		def = FileUtilities.parseCSVLine(input);
		assertEquals("def name ", "aj1", def.getName());

		assertEquals("expecting state ", MachineState.NEW, def.getState());

		input = "\"server\",,\"josh\"";
		def = FileUtilities.parseCSVLine(input);
		assertEquals("def name ", "server", def.getName());
	}

	/**
	 * test create machines using CVS lines as input.
	 *
	 * @throws Exception
	 */
	@Test
	public void testCreateMachineDefsCSV() throws Exception {

		String input = "\"aj\", 1, \"ajsecret1\", \"ajsecret2\", \"ajsecret3\"";
//		String input = "\"team\", 21, \"pass1\", \"pass2\", \"pass3\"";

		Machine def = FileUtilities.parseCSVLine(input);

//		System.out.println(" input = " + input);
//		System.out.println("   def = " + def);

		assertNotNull("Expecting Machine Def ", def);

    int num = 0;
		Set<MachineLogin> logins = def.getLogins();
    assertEquals("Expecting logins ", 3, logins.size());

		for (MachineLogin login : logins) {
      String loginName = login.getLogin().getName();
			String password = login.getLogin().getPassword();
      assertTrue("Expecting password starting with ajsecret for " + loginName, password.startsWith("ajsecret"));

      // TODO: sort these logins so things come in order?(otherwise expected login name doesn't line up)
//      System.out.println(def.getName() + " " + num + " " + password + " " + loginName);
      String expectedLoginName = def.getName() + ((char) ('a' + num));
      assertEquals(loginName, expectedLoginName);
      num++;
		}
	}

	public static void main(String[] args) {

		EntityManagerFactory db = DatabaseUtilities.getCurrentDatabase();
		MachineDAO machineDAO = new MachineDAO((db.createEntityManager()));

		if (db != null) {
			System.out.println("Current database class is " + db.getClass().toString());
			List<Machine> defs = machineDAO.getAll();
			defs.sort((final Machine m1, final Machine m2) -> m1.getName().compareTo(m2.getName())); // sort by name
			System.out.println("There are " + defs.size() + " machines in the database");
			for (Machine machine : defs) {
				System.out.println(machine);
			}
		}
	}
}
