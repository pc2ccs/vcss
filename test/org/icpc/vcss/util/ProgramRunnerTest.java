package org.icpc.vcss.util;

import static org.junit.Assert.*;

import java.io.File;
import java.util.List;

import org.icpc.vcss.core.log.Log;
import org.junit.Test;

/**
 * Unit tests.
 * 
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 */
public class ProgramRunnerTest {

	@Test
	public void testRunProgram() {

		String outdir = "testout/" + this.getClass().getSimpleName();
		new File(outdir).mkdirs();
		
		assertTrue("Expecting dir to exist "+outdir, new File(outdir).isDirectory());

		Log log = new Log(outdir, "testRunProgram.log");
		
		String cmdline = "stat .";
		
		RunProgramResults runProgramResults = ProgramRunner.runProgram(cmdline, log);

		List<String> list = runProgramResults.getStderrOutput();
//		list.forEach(t -> System.out.println("debug stderr " + t));
//		System.out.println("debug stderr "+list);
		
		assertEquals("Expected output line count for stderr command: "+cmdline, 0, list.size());

		list = runProgramResults.getStdoutOutput();
//		list.forEach(t -> System.out.println("debug stdout  " + t));
//		System.out.println("debug stdout "+list);
		String osName = System.getProperty("os.name").toLowerCase();
		boolean isMacOs = osName.startsWith("mac os x");
		if (isMacOs)  {
			// output is all on 1 line under macOs
			assertEquals("Expected output line count for stdout command (macos: "+cmdline, 1, list.size());
		} else {
			assertTrue("Expected output line count for stdout command: "+cmdline, list.size() > 4);
		}
		
		assertEquals("Expected exit code ", ExitCode.SUCCESS, runProgramResults.getExitCode());

	}
	
	@Test
	public void testrunProgramMemory() throws Exception {
		
		String outdir = "testout/" + this.getClass().getSimpleName();
		new File(outdir).mkdirs();
		
		assertTrue("Expecting dir to exist "+outdir, new File(outdir).isDirectory());

		Log log = new Log(outdir, "testrunProgramMemory.log");
		
		String cmdline = "stat .";

		RunProgramResults runProgramResults = ProgramRunner.runProgramMemory(cmdline, log);

		List<String> list = runProgramResults.getStderrOutput();
//		list.forEach(t -> System.out.println("debug stderr " + t));
//		System.out.println("debug stderr "+list);
		
		assertEquals("Expected output line count for stderr command: "+cmdline, 1, list.size());

		list = runProgramResults.getStdoutOutput();
//		list.forEach(t -> System.out.println("debug stdout  " + t));
//		System.out.println("debug stdout "+list);
		String osName = System.getProperty("os.name").toLowerCase();
		boolean isMacOs = osName.startsWith("mac os x");
		if (isMacOs)  {
			assertEquals("Expected output line count for stdout command (macos): "+cmdline, 1, list.size());
		} else {
			assertTrue("Expected output line count for stdout command: "+cmdline, list.size() > 4);
		}

		assertEquals("Expected exit code ", ExitCode.SUCCESS, runProgramResults.getExitCode());

	}

}
