// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
"use strict";
var connection = null;
var clientID = 0;
	

  var execute = async () => {
	  
	  var command = document.getElementById("command").value;
	  console.log("Found command: " + command);
	  
	  var obj = new Object();
	      obj.type = "EXECUTE";
	      obj.content = command;
	      obj.sender = "javascript";
	      obj.timeStamp = new Date();
	      
	  var message = JSON.stringify(obj);
	  
	  console.log("Contacting server to send '" + message + "'");
	  var serverUrl = "https://localhost:50443/execute";

	  const response = await fetch(serverUrl, {
	    method: 'PUT',
	    body: message, // string or object
	    headers: {
	      'Content-Type': 'application/json',
	      'Accept': 'application/json'  
	    }
	  });
	  
	  
	  const myJson = await response.json(); //extract JSON from the http response

	  console.log("Got response from server");
	  
	  console.log("Displaying response on page");
	  
	  var data = myJson.data;
	  
	  document.getElementsByName("response")[0].value = data;
	  
	}



function handleKey(evt) {
  if (evt.keyCode === 13 || evt.keyCode === 14) {
    execute();
    }
  }
