// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
"use strict";
var connection = null;
var clientID = 0;

function connect() {
  var serverUrl;

  serverUrl = "wss://localhost:50443/websocket";

  connection = new WebSocket(serverUrl);
  console.log("***CREATED WEBSOCKET");

  connection.onopen = function(evt) {
    console.log("***CONNECTION OPENED");
    document.getElementById("message").disabled = false;
    document.getElementById("send").disabled = false;
    document.getElementById("response").disabled = false;
  };
  console.log("***CREATED ONOPEN METHOD");

  connection.onmessage = function(evt) {
    console.log("***ONMESSAGE");
    var msg = evt.data;
    console.log("Message Received: ");
    console.dir(msg);
    document.getElementsByName("response")[0].value = msg;

  };
  console.log("***CREATED ONMESSAGE METHOD");
  
  connection.onerror = function(evt) {
	    console.log("***ONERROR");
	    var msg = evt.data;
	    console.log("Error: ");
	    console.dir(msg);

	  };
	  console.log("***CREATED ONERROR METHOD");
 
}

function send() {
  var msg = document.getElementById("message").value;
  console.log("***Sending message: " + msg);
  connection.send(msg);
}


function handleKey(evt) {
  if (evt.keyCode === 13 || evt.keyCode === 14) {
    if (!document.getElementById("send").disabled) {
      send();
    }
  }
}