package org.icpc.vcss.entity;

import static org.junit.Assert.assertEquals;

import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;

import org.icpc.vcss.database.DatabaseUtilities;
import org.junit.Test;

public class MachineCommandResultTest {

	@Test
	public void testMCRT() {

		// Only show severe log messages to console log
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);

    EntityManagerFactory emf = DatabaseUtilities.getCurrentDatabase();
    EntityManager em = emf.createEntityManager();
    MachineCommandResultDAO mcrDAO = new MachineCommandResultDAO(em);

    // Create a machine + command + result
    Machine m = new Machine();
    Command c = new Command();
    MachineCommandResult mcr = new MachineCommandResult();
    mcr.setCommand(c);
    mcr.setMachine(m);

    EntityTransaction t = em.getTransaction();
    t.begin();
    em.persist(m);
    em.persist(c);
    em.persist(mcr);
    t.commit();

//    System.out.println(mcr);
//    System.out.println(mcrDAO.getAll());

    MachineCommandId id = new MachineCommandId(m.getId(), c.getId());
    MachineCommandResult mcr2 = mcrDAO.get(id).orElse(null);

		assertEquals(mcr.getResponse(), mcr2.getResponse());
  }
}
