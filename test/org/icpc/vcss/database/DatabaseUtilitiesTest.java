package org.icpc.vcss.database;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.UUID;
import java.util.logging.Level;

import javax.persistence.EntityManagerFactory;

import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineDAO;
import org.icpc.vcss.util.FileUtilities;
import org.junit.Test;

/**
 * Unit tests.
 *
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 */
public class DatabaseUtilitiesTest {

	/**
	 * Smoke test for current database.
	 * @throws Exception
	 */
	@Test
	public void testCurrentDatabase() throws Exception {

		// Only show severe log messages to console log
		java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);

		// TODO suppress MLog logging to console, ex. NFO: MLog clients using java 1.4+ standard logging.
		
		// Add four machines
		String[] lines = { //

				"team30 team30A=ohoh", //
				"team33 team33B=ohmy team33B=password!", //
				"team44 team44A=t44 team44B=t44Bee team44C=team44sea", //
				"team33 team33B=ohmy team33B=password!", //
		};

		EntityManagerFactory database = DatabaseUtilities.getCurrentDatabase();
		MachineDAO machineDAO = new MachineDAO(database.createEntityManager());

		List<Machine> defsb4 = machineDAO.getAll();
		
		for (String line : lines) {
			Machine mdef = FileUtilities.parseLine(machineDAO, line);
			UUID uuid = UUID.randomUUID();
			mdef.setUuid(uuid);
			machineDAO.save(mdef);
		}

		List<Machine> defs = machineDAO.getAll();
		assertEquals("Expcting N new machines", 4, defs.size() - defsb4.size());
	}

}
