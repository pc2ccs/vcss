## How to setup cacerts.vccs with a non-selfsigned cert.

# pre-setup
* place the `fullchain.pem` as `/etc/ssl/certs/STAR.icpc-vcss.org.pem`
* place `privkey.pem` as `/etc/ssl/private/STAR.icpc-vcss.org.key`

# steps
```
# convert to a p12 when prompted for password enter REPLACE_ME
openssl pkcs12 -export -chain -inkey /etc/ssl/private/STAR.icpc-vcss.org.key -in /etc/ssl/certs/STAR.icpc-vcss.org.pem -out ~/jetty.p12 -name "jetty" -CAfile chain.pem 

# change to vcss dir
cd vcss-\*

# import into cacerts.vcss
keytool -importkeystore -deststorepass "REPLACE_ME" -destkeystore cacerts.vcss -srckeystore ../jetty.p12 -srcstoretype PKCS12 -srcstorepass "REPLACE_ME"
```

Where REPLACE_ME is the `keystorePassword` from org.icpc.vcss.webserver.WebServer