#This file shows how a Contest Administrator defines VCSS Cloud machines.
#Each non-comment line defines one cloud machine using a set of comma-separated fields.
#Blank lines and lines starting with "#" (like this one) are ignored.

#The first field in each line identifies the machine type -- e.g. a team, an "autojudge", a CCS server, etc.
#  If no number is present then there can be only one machine of that type (an empty comma-delimited field
#  must still be present; see the examples below).
#  Currently the only recognized machine types are "team", "aj", and "server".

#The second field identifies the machine number -- for example, "1" for the first machine of that type, etc.

#Following these first two fields are arbitrarily many additional fields, each specifying an account password.

#When the file is loaded by the VCSS system, it creates one machine for each line, creating one account on
# the machine for each password entry; the account names are of the form "machineType" followed by 
# "machineNumber" followed by a dash followed by a single lower-case letter. For example, the following line:

team, 1, secret1, secret2

# will create a machine named "team1" with accounts "team1-a" and "team1-b" with passwords "secret1" and "secret2".

# The following shows additional examples of machine creation:

"team", 2, "secret1", "secret2", "secret3"
team, 3, secret21, secret22, secret23
"team", 4, "3secret1", "3secret2", "3secret3"
"team", 30, "secret1", "secret2", "secret3"
"aj", 1, "ajsecret1", "ajsecret2", "ajsecret3"
"aj", 2, "aj2secret1", "aj2secret2", "aj2secret3"
"server",,"josh"
 
