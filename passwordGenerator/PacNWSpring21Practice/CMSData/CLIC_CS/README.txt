File format:

Contest.tsv
contest Id\tcontest name\tshort name\tcontest date

Groups.tsv
group Id\tgroup name

Teams.tsv
team number\tteam Id\tgroup Id\tteam name\tinst name\tinst short name\tcountry\tgeneric fields

Teams2.tsv
team number\tteam id\tgroup id\tteam name\tinstitution name\tinstitution short name\tcountry\tinstitution id

Institutions.tsv
inst id\tinst name\tinst short name

