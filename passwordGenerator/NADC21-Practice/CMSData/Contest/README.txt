File format:

Team.tab
team Id\tteam Name\tinstitution-unit Id\tsite Id\tcoach Id\tteam status\tdate created\tgeneric Fields

TeamPerson.tab
person Id\tteam Id\tsite Id\tteam role\tbadge role\twhether need TShirt

Site.tab
site Id\tsite name\ttype\tsite capacity\tC\tsite location\texecution date

School.tab
institution-unit Id\tinstitution Id\tinstitution-unit name\tinstitution-unit short name\tinstitution-unit homepage\tcountry\twhether has graduate program(Y/N)

Contest.tab
contest Id\tcontest full name\tcontest short name\tcontest certificate line 1\tcontest certificate line 2\tcontest abbreviation\tcontest geographic area\thosts\tsponsors\tstart date\tend date\temail\thome page\tstanding URL\tprimary site\tM

Staff.tab
person Id\tsite Id\tbadge role\tcertificate role\twhether need T shirt

Person.tab
person Id\ttitle\tfirst name\tlast name\tprefered name\tcertificate name\tinstitution\tjob title\temail\tmissing\ttelephone\temergency phone\temergency contact name\tmobile\tfax\taddress line 1\taddress line 2\taddress line 3\tcity\tstate\tzip code\tcountry\twhether include email when publish(Y/N)\twhether inform other contests(Y/N)\tACM Id\tDate of birth\tPlace of birth\tsex\thome town\thome country\tshirt size\tdegree\tstudy area\tdegree start date\tgraduate date
