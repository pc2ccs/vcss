// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.samps.websocket.toUpperCase;

import java.io.IOException;
import java.util.Date;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.websocket.jsr356.server.deploy.WebSocketServerContainerInitializer;
import org.icpc.vcss.samps.websocket.toUpperCase.ToUpperServerSocketEndpoint;

/**
 * Test Web Server: see how to get JSR356 websockets working.
 * 
 * @author John Clevenger (pc2@ecs.csus.edu)
 */
public class ToUpperWebServer {

	private Server jettyServer = null;



	/**
	 * Starts a Jetty webserver with a single websocket service which converts received messages to
	 * upper case and echoes them back.
	 */
	public void startWebServer() {

		try {
			int port = 50443;

			jettyServer = new Server();
			
	        ServerConnector connector = new ServerConnector(jettyServer);
	        connector.setPort(port);
	        jettyServer.addConnector(connector);
	        
			ServletContextHandler context = new ServletContextHandler(ServletContextHandler.SESSIONS);
			context.setContextPath("/");
			jettyServer.setHandler(context);
			
	        // Add javax.websocket support
//	        ServerContainer container = WebSocketServerContainerInitializer.configureContext(context);  //deprecated
	        WebSocketServerContainerInitializer.configure(context,
	                (servletContext, wsContainer) ->
	                {
	                    // This lambda will be called at the appropriate place in the
	                    // ServletContext initialization phase where you can initialize
	                    // and configure your websocket container.

	                    // Configure defaults for container
	                    wsContainer.setDefaultMaxTextMessageBufferSize(65535);

	                    // Add WebSocket endpoint to javax.websocket layer
	                    wsContainer.addEndpoint(ToUpperServerSocketEndpoint.class);
	                }
	        		);

	        // Add websocket service class endpoint to server container
//	        container.addEndpoint(ToUpperServerSocket.class);


			jettyServer.start();
			showMessage("Started web server on port " + port);
			jettyServer.join();
			
		} catch (NumberFormatException e) {
			showMessage("Unable to start web services: invalid port number: " + e.getMessage(), e);
		} catch (IOException e1) {
			showMessage("Unable to start web services: " + e1.getMessage(), e1);
		} catch (Exception e2) {
			showMessage("Unable to start web services: " + e2.getMessage(), e2);
		}
	}

	private void showMessage(final String message, Exception ex) {
		System.out.println(new Date() + " " + message);
		ex.printStackTrace();
	}

	private void showMessage(String message) {
		System.out.println(new Date() + " " + message);
	}


	/**
	 * This method starts the VSS web server.
	 * 
	 * @param args not used
	 */
	public static void main(String[] args) {

		new ToUpperWebServer().startWebServer();

	}

}
