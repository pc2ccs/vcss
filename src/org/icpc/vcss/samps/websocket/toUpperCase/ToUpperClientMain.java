// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.samps.websocket.toUpperCase;

import java.net.URI;

import javax.websocket.ContainerProvider;
import javax.websocket.WebSocketContainer;

public class ToUpperClientMain {

	public static void main(String[] args) {

		try {
			WebSocketContainer container = ContainerProvider.getWebSocketContainer();

			try {
				 String dest = "ws://127.0.0.1:50443/jsr356toUpper";
				 
				ToUpperClientSocketEndpoint socket = new ToUpperClientSocketEndpoint();
				
				System.out.println("Connecting to server...");
				container.connectToServer(socket, new URI(dest));
				System.out.println("Returned from connecting to server.");
				
				socket.getLatch().await();
				
				socket.sendMessage("echo356");
				socket.sendMessage("test356");
				Thread.sleep(10000l);
				
//			} finally {
//				// Force lifecycle stop when done with container.
//				// This is to free up threads and resources that the
//				// JSR-356 container allocates. But unfortunately
//				// the JSR-356 spec does not handle lifecycles (yet)
//				LifeCycle.stop(container);
			} catch (Throwable t) {
				t.printStackTrace(System.err);
			}

		} catch (Throwable t) {
			t.printStackTrace(System.err);
		}

	}
}
