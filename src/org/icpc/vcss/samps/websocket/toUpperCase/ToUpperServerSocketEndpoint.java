// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.samps.websocket.toUpperCase;

import java.io.IOException;

import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
 
@ServerEndpoint("/jsr356toUpper")
public class ToUpperServerSocketEndpoint {
 
    @OnOpen
    public void onOpen(Session session) {
        System.out.println("WebSocket opened: " + session.getId());
    }
    @OnMessage
    public void onMessage(String txt, Session session) throws IOException {
        System.out.println("Message received: " + txt);
        session.getBasicRemote().sendText(txt.toUpperCase());
    }
 
    @OnClose
    public void onClose(CloseReason reason, Session session) {
    	String reasonValue ;
    	if (reason.getReasonPhrase()==null || reason.getReasonPhrase().length()==0) {
    		reasonValue = "Unknown reason";
    	} else {
    		reasonValue = reason.getReasonPhrase();
    	}
    	
        System.out.printf("Closing client %s websocket due to %s", session.getId(), reasonValue);

    }
    
    @OnError
    public void onError(Session session, Throwable t) {
    	System.out.println("Websocket error : client " + session.getId() + " " + t.getMessage());
    }
}
