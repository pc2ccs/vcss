// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.samps.websocket.toUpperCase;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
 
@ClientEndpoint
//@WebSocket
public class ToUpperClientSocketEndpoint { //extends Endpoint {
 
    CountDownLatch latch = new CountDownLatch(1);
    private Session session;
 
    @OnOpen
    public void onOpen(Session session) {
        System.out.println("Connected to server");
        this.session = session;
        latch.countDown();
    }
 
    @OnMessage
    public void onMessage(String message, Session session) {
        System.out.println("Message received from server:" + message);
    }
 
    @OnClose
    public void onClose(CloseReason reason, Session session) {
        System.out.println("Closing a WebSocket due to " + reason.getReasonPhrase());
    }
    
    @OnError
    public void onError(Session session, Throwable thrown) {
    	System.out.println("Client endpoint error: client = " + session.getId() + "; message = " + thrown.getMessage());
    }
 
    public CountDownLatch getLatch() {
        return latch;
    }
 
    public void sendMessage(String str) {
        try {
            session.getBasicRemote().sendText(str);
        } catch (IOException e) {
 
            e.printStackTrace();
        }
    }

//	@Override
//	public void onOpen(Session session, EndpointConfig arg1) {
//		System.out.println("Client endpoing onOpen() called");
//        this.session = session;
//        latch.countDown();		
//	}
}