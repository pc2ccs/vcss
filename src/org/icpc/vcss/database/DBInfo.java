package org.icpc.vcss.database;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.icpc.vcss.core.VersionInfo;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineDAO;

/**
 *
 *
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 *
 */
public class DBInfo {

	private static void usage() {

		VersionInfo vi = new VersionInfo();

		String[] versionInfo = vi.getSystemVersionInfoMultiLine();

		String[] usage = { //
				"Usage: DBInfo [--help]   ", //
				"", //
				"Purose: contacts database in vcss.properties and prints machines", //
				"", //
		};

		for (String string : usage) {
			System.out.println(string);
		}

		for (String string : versionInfo) {
			System.out.println(string);
		}

	}

	public static void main(String[] args) {

		try {
			if (args.length > 0 && "--help".equals(args[0])) {
				usage();
			} else {
				printInfo();
			}

		} catch (Exception e) {
			e.printStackTrace(System.err);
		}
	}

	private static void printInfo() {
    EntityManagerFactory entityManagerFactory = DatabaseUtilities.getCurrentDatabase();
    MachineDAO machineDAO = new MachineDAO(entityManagerFactory.createEntityManager());

		System.err.println("Run at "+new Date());

    List<Machine> defs = machineDAO.getAll();
    defs.sort((final Machine m1, final Machine m2) -> m1.getName().compareTo(m2.getName())); // sort by name
    System.out.println("There are "+defs.size()+" machines in the database");
    for (Machine machine : defs) {
      System.out.println(machine);
    }
	}

}
