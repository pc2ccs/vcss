package org.icpc.vcss.database;

import java.io.PrintStream;
import java.sql.Timestamp;
import java.util.Date;

/**
 * Utilities.
 * 
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 */
public class SysUtil {

	/**
	 * Print partial stack trace, only print lines that contain the string vcss.
	 */
	public static void printStackTrace(PrintStream printStream, Exception e) {
		printStackTracePattern(printStream, e, "vcss");
	}

	/**
	 * Prints a stack trace, prints stack elements which only matches pattern.
	 * 
	 * Example to only print stack trace elements with csus:
	 * 
	 * <pre>
	 *    Utilities.printStackTrace(System.err,e,"csus");
	 * 
	 * prints:
	 * 
	 * java.sql.SQLException: No value specified for parameter 7
	 *    Matching: csus
	 *      at edu.csus.ecs.pc2.db.adapters.MySqlDatabaseAdapter(MySqlDatabaseAdapter.java:274)
	 *      at edu.csus.ecs.pc2.db.adapters.MySqlDatabaseAdapter(MySqlDatabaseAdapter.java:2526)
	 *      at edu.csus.ecs.pc2.db.adapters.MySqlDatabaseAdapter(MySqlDatabaseAdapter.java:2442)
	 *      at edu.csus.ecs.pc2.db.adapters.DatabaseAdapterTest(DatabaseAdapterTest.java:237)
	 * </pre>
	 * 
	 * @param printStream
	 * @param e
	 * @param pattern
	 */
	public static void printStackTracePattern(PrintStream printStream, Exception e, String pattern) {

		printStream.println(e.toString());
		printStream.println("   Matching: " + pattern);
		StackTraceElement[] st = e.getStackTrace();
		for (StackTraceElement stackTraceElement : st) {

			String className = stackTraceElement.getClassName();

			if (className.indexOf(pattern) != -1) {
				printStream.println("    at " + //
						className + "." + //
						stackTraceElement.getMethodName() + "(" + //
						stackTraceElement.getFileName() + ":" + //
						stackTraceElement.getLineNumber() + ")" //
				);
			}
		}
	}

    /**
     * Return timestamp as of now.
     * @return
     */
    public static Timestamp getTimestampNow() {
            return new Timestamp(new Date().getTime());
    }

	public static String[] splitSQLStatements(String[] tableSQL, boolean b) {
		// copy from v10
		return null;
	}

	public static String[] splitSQLStatements(String initialSQL, boolean b) {
		// TODO copy from v10
		return null;
	}

}
