// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.database;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.icpc.vcss.core.Constants;
import org.icpc.vcss.core.Utilities;
import org.icpc.vcss.entity.Login;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineDAO;
import org.icpc.vcss.entity.MachineLogin;
import org.icpc.vcss.util.FileUtilities;

/**
 * These contains utilities used when using the Database.
 *
 * @author Troy Boudreau, ICPC Development Team (pc2@ecs.csus.edu)
 */
public class DatabaseUtilities {

	/**
	 * Table required in database.
	 */
	public static final String VCSS_REQUIRED_TABLE_NAME = "machines";;

	/**
	 * A SQL file that contains DB SQL to create initial vcss tables, etc.
	 *
	 */
	public static final String INITIAL_MYSQL_TABLES_SQL_FILENAME = "data/sql/initial.mysql.tables.sql";

	private static EntityManagerFactory entityManagerFactory = null;

	static String PAD = "   ";

	/**
	 * Reads vcss.properties file AND/OR ENV variable to get connection details and
	 * return database. ENV overrides vcss.properties file.
	 *
	 * @return EntityManagerFactory a configured JPA EntityManagerFactory
	 */
	public static EntityManagerFactory getCurrentDatabase() {
		if (entityManagerFactory != null) {
			return entityManagerFactory;
		}

		Properties properties = new Properties();

		// 1st load default vcss.properties from jar file(if present)
		InputStream in = DatabaseUtilities.class.getResourceAsStream("/" + Constants.VCSS_PROPERTIES_FILENAME);
		if (in != null) {
			BufferedReader reader = new BufferedReader(new InputStreamReader(in));
			try {
				properties.load(reader);
			} catch (IOException e) {
				System.err
						.println("Failed to load file from jar file, ignoring: " + Constants.VCSS_PROPERTIES_FILENAME);
				e.printStackTrace();
			}
		}

		// 2nd load vcss.properties(from current path)
		if (Utilities.fileExists(Constants.VCSS_PROPERTIES_FILENAME)) {
			FileReader reader;
			try {
				reader = new FileReader(new File(Constants.VCSS_PROPERTIES_FILENAME));
				properties.load(reader);
				reader.close();
			} catch (FileNotFoundException e) {
				System.err.println(
						"getCurrentDatabase() Utilities.fileExists said it existed, but FileReader disagrees, ignoring "
								+ Constants.VCSS_PROPERTIES_FILENAME);
				e.printStackTrace();
			} catch (IOException e) {
				System.err.println(
						"getCurrentDatabase() Error reading file, ignoring " + Constants.VCSS_PROPERTIES_FILENAME);
				e.printStackTrace();
			}
		}

		// 3rd load from environment
		// Read through the environment and pull out any env vars that start with our
		// prefix.
		// E.g. the following env vars will map to the corresponding properties
		// VCSS_HIBERNATE_DIALECT -> hibernate.dialect
		// VCSS_overrideClientUUID -> overrideClientUUID
		for (Map.Entry<String, String> entry : System.getenv().entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			// If the env var doesn't start with our prefix, ignore it.
			if (!key.startsWith(Constants.ENV_PREFIX)) {
				continue;
			}
			String property_name = key.substring(Constants.ENV_PREFIX.length()).replace("_", ".");
			if (property_name.equals(property_name.toUpperCase())) {
				property_name = property_name.toLowerCase();
			}
			properties.put(property_name, value);
		}

		try {
			entityManagerFactory = Persistence.createEntityManagerFactory("vcss", properties);
		} catch (Exception e) {
			try {
				properties.store(System.err, "Ini file contents");
			} catch (IOException e1) {
				; // ignore
			}
			System.err.println("Error attempting to initialize database");
			e.printStackTrace(System.err);
		}

		// TODO LOG
		return entityManagerFactory;
	}

	/**
	 * Dump all Machine in a db.
	 *
	 * @see #dumpMachine(String, IDatabase, UUID)
	 * @param comment
	 * @param db
	 */
	public static void dumpAllMachines(String comment, MachineDAO machineDAO) {
		List<Machine> defs = machineDAO.getAll();
		defs.sort((final Machine m1, final Machine m2) -> m1.getName().compareTo(m2.getName())); // sort by name
		System.out.println(defs.size() + " machines in database");
		for (Machine machine : defs) {
			dumpMachine(comment, machine);
		}
	}

	public static void dumpMachine(String comment, Machine md) {
		if (comment == null) {
			comment = "";
		}

		System.err.flush();
		System.out.flush();
		System.out.println(comment + " Dump Machine id=" + md.getId());

		System.out.println(PAD + "md= " + md.getId());

		System.out.println(PAD + " hostname   ='" + md.getHostname() + "'");
		System.out.println(PAD + " ipaddr     ='" + md.getIpAddr() + "'");
		System.out.println(PAD + " name       ='" + md.getName() + "'");
		System.out.println(PAD + " connected  ='" + md.getConnected() + "'");
		System.out.println(PAD + " registered ='" + md.getRegistered() + "'");
		System.out.println(PAD + " state      ='" + md.getState() + "'");
		System.out.println(PAD + " uuid       ='" + md.getUuid() + "'");
		System.out.println(PAD + " added      ='" + md.getAdded() + "'");
		System.out.println(PAD + " updated    ='" + md.getUpdated() + "'");

		Set<MachineLogin> logins = md.getLogins();
		System.out.println(PAD + logins.size() + " logins");
		for (MachineLogin ml : logins) {
			Login login = ml.getLogin();
			System.out.println(PAD + " login=" + login.getName() + " password='" + login.getPassword() + "'");
		}

		if (comment.length() > 0) {
			System.out.println(comment + " END");
		}
		System.out.flush();
	}

	/**
	 * Load/Update Machine Definitions from CSV filename.
	 *
	 *
	 * @param csvFileName
	 * @throws IOException
	 */
	public static void loadDatabaseFromCSVFile(String csvFileName, MachineDAO machineDAO) throws IOException {

		String[] lines = FileUtilities.loadFile(csvFileName);
		int linenumber = 0;
		for (String line : lines) {
			linenumber++;

			// ignore blank and comment lines
			if (line.trim().length() != 0 && !line.trim().startsWith("#")) {

				try {
					Machine def = FileUtilities.parseCSVLine(line);

					List<Machine> existingDefs = machineDAO.FindAllByName(def.getName());

					if (existingDefs.size() > 0) {
						// ignored
//						System.out.println("Machine name already exists in DB '"+def.getName()+"', not added.");
					} else {
						machineDAO.save(def);
					}

				} catch (Exception e) {
					System.err.println(csvFileName + ":" + linenumber + " " + e.getMessage());
				}
			}
		}
	}
}
