package org.icpc.vcss.database;

/**
 * PC^2 Database Exception.
 * 
 * @author Douglas A. Lane, PC^2 Team, pc2@ecs.csus.edu
 */
public class DatabaseException extends RuntimeException {
    /**
     * 
     */
    private static final long serialVersionUID = 7900571541147966678L;

    public DatabaseException(String string) {
        super(string);
    }

    public DatabaseException(String string, Exception e1) {
        super(string, e1);
    }


}
