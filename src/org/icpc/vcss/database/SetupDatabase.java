package org.icpc.vcss.database;

import org.icpc.vcss.core.VersionInfo;

/**
 * Initialize database tables.
 * 
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 */
public class SetupDatabase {

	private static void usage() {

		String[] message = { //
				"SetupDatabase [--help]", //
				"", //
				"Purpose: for the RDBMS database specified in vcss.properties ensures that tables are created", //
				"", //
				new VersionInfo().getSystemVersionInfo(), "",//
		};

		for (String string : message) {
			System.out.println(string);
		}

	}

	private static void checkAndEnsureVCSSTables() {
		throw new RuntimeException("Not yet implemented");
//		try {
//
//			Properties properties = FileUtilities.loadPropertiesFile(Constants.VCSS_PROPERTIES_FILENAME);
//			DBConnectData data = DBConnectData.createConnectionData(properties);
//
//			Class.forName(data.getJdbcClassName());
//			Connection conn = DatabaseUtilities.createConnection(data);
//
//			boolean tablesExist = DatabaseUtilities.tableExists(conn, DatabaseUtilities.VCSS_REQUIRED_TABLE_NAME);
//
//			boolean passes = DatabaseUtilities.ensureDatabaseTables(conn);
//			if (passes) {
//				if (tablesExist) {
//					System.err.println("Note: tables already existed");
//				}
//				System.out.println(
//						new Date() + "Pass - tables created/exist for database '" + data.getDatabaseName() + "'");
//			} else {
//				System.out.println(
//						new Date() + " Fail - Tables were NOT created for database '" + data.getDatabaseName() + "'");
//			}
//
//		} catch (Exception e) {
//			e.printStackTrace();
//			System.out.flush();
//			System.err.println(e.getMessage());
//		}

	}

	public static void main(String[] args) {

		if (args.length > 0) {
			if (args[0].contentEquals("--help")) {
				usage();
			}
			System.err.println("Unknown command option, use --help to list help");
		} else {
			checkAndEnsureVCSSTables();
		}
	}

}
