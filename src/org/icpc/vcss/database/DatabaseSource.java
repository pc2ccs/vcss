package org.icpc.vcss.database;

/**
 * Source for the database.
 * 
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 *
 */
public enum DatabaseSource {
	
	/**
	 * not set
	 */
	UNSET, //
	
	/**
	 * 
	 * @see InMemoryDatabase
	 */
	IN_MEMORY, //
	
	/**
	 * 
	 * @see MySqlDatabaseAdapter
	 */
	MYSQL,

}
