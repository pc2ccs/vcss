package org.icpc.vcss.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.icpc.vcss.entity.Login;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineDAO;

public class FileUtilities {

	/**
	 * Load database from file.
	 *
	 * @param filename
	 * @throws IOException
	 */
	public static void loadDatabase(String filename, MachineDAO machineDAO) throws IOException {

		String[] lines = loadFile(filename);
		int linenumber = 0;
		for (String line : lines) {
			linenumber++;

			// ignore blank and comment lines
			if (line.trim().length() != 0 && !line.trim().startsWith("#")) {

				try {
					Machine def = parseLine(machineDAO, line);
					machineDAO.save(def);
				} catch (Exception e) {
					System.err.println(filename + ":" + linenumber + " " + e.getMessage());
				}
			}
		}
	}

	public static Machine parseLine(MachineDAO machineDAO, String line) throws Exception {

		String[] fields = line.trim().split(" ");

		// team30 bruce=Fiddle [props: name="value" name2="value2"

		if (fields.length < 2) {
			throw new Exception("Too few fields (" + fields.length + ") on line '" + line + "'");
		}

		int idx = 0;
		String name = fields[idx++];
		idx++;

		Machine machine = new Machine(name);

		boolean addingLogins = true;
		for (int i = idx - 1; i < fields.length; i++) {
			String value = fields[i];
			if (addingLogins && "props:".contentEquals(value.trim())) {
				addingLogins = false;
			} else if (addingLogins) {
				String[] loginPair = value.split("=");
				machine.addLogin(new Login(loginPair[0], loginPair[1]));
			} else {
				// Add properties
				String[] nv = value.split("=");
				// TODO: FIXME
				// machineDAO.setProperty(machine, nv[0], nv[1]);
				throw new RuntimeException("FileUtilties:parseLine() - Additional properties not supported yet");
			}
		}

		return machine;
	}

	public static String[] loadFile(String filename) throws IOException {
		return loadFile(filename, Long.MAX_VALUE);
	}

	/**
	 * Load string array with file contents.
	 *
	 * @param filename
	 * @param maximumLinesReturned minimum value is 1.
	 * @return lines from file
	 * @throws IOException
	 */
	public static String[] loadFile(String filename, long maximumLinesReturned) throws IOException {
		List<String> lines = new ArrayList<>();

		if (filename == null) {
			throw new IllegalArgumentException("filename is null");
		}

		if (new File(filename).exists()) {
			BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(filename), "UTF8"));

			long lineCount = 0;

			String line = in.readLine();
			while (lineCount < maximumLinesReturned && line != null) {
				lineCount++;
				lines.add(line);
				line = in.readLine();
			}
			in.close();
			in = null;
		}

		return (String[]) lines.toArray(new String[lines.size()]);
	}

	/**
	 * Write lines to file.
	 *
	 * @param filename
	 * @param lines
	 * @throws FileNotFoundException
	 */
	public static void writeLinesToFile(String filename, String[] lines) throws FileNotFoundException {

		PrintWriter printWriter = new PrintWriter(new FileOutputStream(filename, false), true);
		for (String line : lines) {
			printWriter.println(line);
		}
		printWriter.close();

	}

	public static void storeDB(String outFileName, MachineDAO machineDAO) throws FileNotFoundException {

		PrintWriter printWriter = new PrintWriter(new FileOutputStream(outFileName, false), true);

		List<Machine> defs = machineDAO.getAll();

		Date now = new Date();

		printWriter.println("# ");
		printWriter.println("# Created by storeDB on " + now);
		printWriter.println("# ");
		printWriter.println();

		defs.sort((final Machine m1, final Machine m2) -> m1.getName().compareTo(m2.getName())); // sort by name
		for (Machine machine : defs) {
			printWriter.println(machine);
		}
		printWriter.close();

	}

	public static Machine parseCSVLine(String line) {

		// matchine_prefix, number, password, password, passwor
		// matchine_prefix, number, password
		// "team", 30, "secret1", "secret2", "secret3"
		// "aj", 1, "ajsecret1"
		// "server",,"josh"

		String[] fields = parseCSV(line);
		return parseCSVLine(fields);
	}

	public static Machine parseCSVLine(String[] fields) {

		Machine def = new Machine("unset");

		if (fields.length > 2) {

			String machinePrefix = fields[0].trim();
			String number = fields[1].trim();
			String hostName = machinePrefix;

			boolean noNumberSpecified = number == null || number.length() == 0 || "0".equals(number);

			if (!noNumberSpecified) {
				hostName = machinePrefix + number;

				for (int i = 0; i < fields.length - 2; i++) {
					String loginName = machinePrefix + number +  ((char) ('a' + i));
					String password = fields[i + 2];
					def.addLogin(new Login(loginName, password));
				}

			} else {

				for (int i = 0; i < fields.length - 2; i++) {
					String loginName = machinePrefix + "-" + ((char) ('a' + i));
					String password = fields[i + 2];
					def.addLogin(new Login(loginName, password));
				}
			}

			def.setName(hostName);

		} else {
			// TODO how best to handle, throw exception, log, return null?
			def = null;
		}

		return def;
	}

	/**
	 * A crude CSV line parse.
	 *
	 * @param line
	 * @return array of strings
	 */
	public static String[] parseCSV(String line) {
		// SOMEDAY replace with a robust CVS parsing method

		String[] fields = line.split(",");

		for (int i = 0; i < fields.length; i++) {
			String string = fields[i].trim();
			if (string.startsWith("\"")) {
				string = string.substring(1);
			}
			if (string.endsWith("\"")) {
				string = string.substring(0, string.length() - 1);
			}
			fields[i] = string;
		}
		return fields;
	}

	/**
	 * Loads properties from the specified file and returns them in a Properties
	 * object.
	 *
	 * @param filename - the file to load from
	 * @return a Properties object containing the Properties given in the file
	 * @throws IOException
	 */
	public static Properties loadPropertiesFile(String filename) throws IOException {
		Properties properties = new Properties();
		FileReader reader = new FileReader(new File(filename));
		properties.load(reader);
		return properties;
	}

}
