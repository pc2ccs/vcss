package org.icpc.vcss.util;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class StandaloneDNSUpdate {
	
    private boolean updateDNS(String domainToProcess, boolean removeFlag) {
    	
		System.out.println("Checking subdomain " + domainToProcess);
		
//		//construct a client with which to make the GoDaddy API call
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		
		try {
			
			System.out.println ("Fetching current DNS list from GoDaddy...");
			
			//construct the basic GoDaddy API http request
		    HttpGet request = new HttpGet("https://api.godaddy.com/v1/domains/icpc-vcss.org/records/A");

		    
		    //add headers and body to the request
		    request.addHeader("accept", "application/json");
		    request.addHeader("content-type", "application/json");
		    request.addHeader("Authorization", "sso-key eoMJEu9vgZu2_Jbbk4qLJvBam9NKL8kSTLw:KyGXoKu2RUZHsmaRAEkbEF");
		    //execute the http request (send it to the GoDaddy API)
		    CloseableHttpResponse response = httpClient.execute(request);
		    
		    //get the http response code
		    StatusLine statusLine = response.getStatusLine();
		    int statusCode = statusLine.getStatusCode();

		    if (statusCode != HttpStatus.SC_OK) {
		    	System.out.println("***** Something went wrong with fetching DNS list *********");
		    	return false;
		    } else {

		    	System.out.println ("Processing DNS list returned from GoDaddy");
		    	
		    	//returned json looks like this
		    	//[ {"data":"54.176.162.21","name":"unknowncloudvm_ip-172-31-12-218.pacnwpractice","ttl":600,"type":"A"}]

		    	//define an object which knows how to map JSON to objects
		    	ObjectMapper mapper = new ObjectMapper();
		    	
		    	//map the GoDaddy JSON response to an array of DNSEntry objects
		    	List<DNSEntry> dnsEntryList = mapper.readValue(response.getEntity().getContent(), new TypeReference<List<DNSEntry>>(){});
		    	
//		    	System.out.println ("Got the following DNS entries:");
//		    	for (DNSEntry entry : dnsEntryList) {
//		    		System.out.println ("   " + entry.getData() + ":" + entry.name);
//		    	}

		    	List<DNSEntry> keepers = new ArrayList<DNSEntry>();
		    	List<DNSEntry> weepers = new ArrayList<DNSEntry>();
		    	
		    	// go through the map and look for the subDomaintoRemove in the attribute "name" , removing object
		    	int removedCount = 0;
		    	int keptCount = 0;
		    	for (DNSEntry entry : dnsEntryList) {
		    		if (entry.getName().endsWith("."+domainToProcess)) {
		    			weepers.add(entry);
		    			removedCount++;
		    			//note that "removing" an entry simply means "don't ADD it to the 'keepers' list"
	    				//System.out.println ("  Removing " + entry.getData() + ":" + entry.getName());
		    		} else {
		    			keepers.add(entry);
		    			keptCount++;
		    		}
		    	}
		    	
		    	if (removeFlag) {
			    	//convert keepers to a JSON array
			    	String result = mapper.writeValueAsString(keepers) ;
			    	
			    	// call GoDaddy to push the updated json
	    			System.out.println ("  Removed " + removedCount + " entries");
	    			System.out.println ("  Kept " + keptCount + " entries");

	    			System.out.println ("Sending the following to GoDaddy:");
			    	System.out.println(result);
	
					//construct the basic GoDaddy API http request
				    HttpPut putRequest = new HttpPut("https://api.godaddy.com/v1/domains/icpc-vcss.org/records/A");
	
				    //add headers to the request
				    putRequest.addHeader("accept", "application/json");
				    putRequest.addHeader("content-type", "application/json");
				    putRequest.addHeader("Authorization", "sso-key eoMJEu9vgZu2_Jbbk4qLJvBam9NKL8kSTLw:KyGXoKu2RUZHsmaRAEkbEF");
				    
				    //add body to the request
				    StringEntity jsonEntity = new StringEntity(result);
				    putRequest.setEntity(jsonEntity);
				    
				    //execute the http request (send it to the GoDaddy API)
				    CloseableHttpResponse putResponse = httpClient.execute(putRequest);
				    
				    //get the http response code
				    StatusLine responseStatusLine = putResponse.getStatusLine();
				    int putStatusCode = responseStatusLine.getStatusCode();
	
				    if (putStatusCode != HttpStatus.SC_OK) {
				    	System.out.println("***** Something went wrong with posting the updated DNS list *********");
				    	System.out.println("Response code = " + putStatusCode + "; response reason = " + responseStatusLine.getReasonPhrase());
				    	return false;
				    } else {
				    	return true;
				    }
		    	} else {
		    		if (weepers.size() > 0) {
			    		System.out.println ("DNS entries to be removed: " + weepers.size() + " of " + (keptCount + removedCount));
				    	for (DNSEntry entry : weepers) {
			    			System.out.println ("   " + entry.getName() + ":" + entry.getData());
				    	} 
		    		} else {
			    		System.out.println ("No DNS entries to be removed.");
		    		}
		    	}
		    }
		} catch (Exception ex) {
		    System.out.println("Exception during DNS update: " + ex.toString());
		    return false;
		} finally {
		    try {
				httpClient.close();
			} catch (Exception e) {
			    System.out.println("Exception closing DNS update httpClient: " + e.toString());
			}
		}
		return true;
	}
	
	private static class DNSEntry {
		@JsonProperty
		private String data ;
		
		@JsonProperty
		private String name ;
		
		@JsonProperty
		private int ttl ;
		
		@JsonProperty
		private String type ;
		
		@JsonCreator
		public DNSEntry() {
			
		}
		
		public String getData() {
			return data;
		}
		
		public String getName() {
			return name;
		}
	}
	
	private StandaloneDNSUpdate() {
		try {
		} catch (Exception e) {
			System.out.print("******* Exception during testDNS:");
			e.printStackTrace();
			System.out.flush();
			System.err.println(e.getMessage());
		}
	}

	public static void main(String[] args) {
		StandaloneDNSUpdate test = new StandaloneDNSUpdate();
		
		// set to false to list the entries that will be removed
		// set to true to actually remove entries from Godaddy
		boolean update = false;
		
		//subdomain entries to look at, command will process entries ending with "." + domainToProcess 
		String domainToProcess = "nadctest2.25mar21";
		
		//TODO: do not do any update if there are zero entries to be removed.
		boolean result = test.updateDNS(domainToProcess, update);
		
		if (update) {
			if (result) {
				System.out.println("Update Succeeded!"); 
			} else {
				System.out.println("Something went wrong deleting!");
			}
		} else {
			if (result) {
				System.out.println("Listing Succeeded!"); 
			} else {
				System.out.println("Something went wrong listing!");
			}
			
		}
	}
}
