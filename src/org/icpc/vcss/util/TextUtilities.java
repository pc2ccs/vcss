package org.icpc.vcss.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class TextUtilities {

	/**
	 * Substitutes property values for variables in String list.
	 * 
	 * See {@link #substituteProperties(String, Properties)} for
	 * details about substitution
	 * 
	 * @param list
	 * @param properties key and value pairs
	 * @return new list with value substituted
	 */
	public static List<String> substitueProperties(List<String> list, Properties properties) {

		List<String> newList = new ArrayList<String>();

		for (String string : newList) {
			newList.add(substituteProperties(string, properties));
		}

		return newList;
	}
	
	/**
	 * Does the string have at least one variable?
	 * 
	 * A variable is a name/string enclosed by ${ }, ex ${ip_address}
	 * @param string
	 * @return
	 */
	public static boolean hasVariables(String string) {
		MatchedVariable matchedVariable = matchNextVariable(string, 0);
		return matchedVariable != null;
	}

	/**
	 * Search for variable in string.
	 * 
	 * For variable INFO, will find ${INFO} in string.
	 * 
	 * @param s input string
	 * @param offset start searching at this offent into string
	 * @return null if not found, else returns MatchedVariable
	 */
	public static MatchedVariable matchNextVariable(String s, int offset) {

		int startIndex = s.indexOf("${", offset);

		if (startIndex != -1) {
			int endIndex = s.indexOf("}", startIndex + 2);
			if (endIndex != -1) {
				String name = s.substring(startIndex + 2, endIndex);
				return new MatchedVariable(startIndex, endIndex + 1, name);
			}
		}

		return null;
	}

	/**
	 * Substitutes property values for variables.
	 * 
	 * Searches for ${VAR}, if property with key "VAR" found 
	 * replaces variable with value in properties.  if VAR value
	 * not found will leave variable in place.
	 * 
	 * Will find and replace all variables and strings in string.
	 * 
	 * @param string
	 * @param properties key and value pairs
	 * @return string with substituted variables.
	 */
	public static String substituteProperties(String string, Properties properties) {

		MatchedVariable matchedVariable = matchNextVariable(string, 0);

		if (matchedVariable != null) {
			String outstring = "";
			int nextOffset = 0;

			while (matchedVariable != null) {

				// add string before variable
				int prefixLen = matchedVariable.getStartOffset() - nextOffset;
				if (prefixLen > 0) {
					outstring += string.substring(nextOffset, matchedVariable.getStartOffset());
				}

				// lookup value for variable
				String value = properties.getProperty(matchedVariable.getName());

				if (value != null) {
					outstring += value;
				} else {
					outstring += string.substring(matchedVariable.getStartOffset(), matchedVariable.getEndOffset());
				}

				nextOffset = matchedVariable.getEndOffset();
				matchedVariable = matchNextVariable(string, nextOffset);
			}

			// add string after last variable.
			if (nextOffset < string.length()) {
				outstring += string.substring(nextOffset);
			}

			return outstring;

		} else {
			return string;
		}
	}

}
