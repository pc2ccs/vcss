package org.icpc.vcss.util;

import java.util.List;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.AmazonEC2Exception;
import com.amazonaws.services.ec2.model.DescribeNetworkInterfacesRequest;
import com.amazonaws.services.ec2.model.DescribeNetworkInterfacesResult;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.NetworkInterface;

public class StandaloneAWSCallsTest {

	private AmazonEC2 ec2;

	private String getAWSInstanceId(String targetIpAddress) {

		// the value we are going to return
		String instanceId = null;

		try {
			DescribeNetworkInterfacesRequest newRequest = new DescribeNetworkInterfacesRequest();

			Filter filter = new Filter();

			filter.withName("association.public-ip").withValues(targetIpAddress);
			newRequest.withFilters(filter);

			DescribeNetworkInterfacesResult result = ec2.describeNetworkInterfaces(newRequest);

			if (result != null) {
				List<NetworkInterface> ni = result.getNetworkInterfaces();
				if (ni.size() == 1) {
					instanceId = ni.get(0).getAttachment().getInstanceId();
					System.out.println("Found InstanceId " + instanceId + " for machine " + targetIpAddress);
					return instanceId;
				} else {
					if (ni.size() == 0) {
						System.out.println("PROBLEM: Couldnt find InstanceId for machine " + targetIpAddress);

					} else {
						String ids = "";
						for (int i = 0; i < ni.size(); i++) {
							ids = ni.get(i).getAttachment().getInstanceId() + ", ";
						}
						System.out.println(
								"PROBLEM: Found multiple InstanceId's " + ids + " for machine " + targetIpAddress);
					}
					return null;
				}
			} else {
				System.out.println("PROBLEM: Couldnt find InstanceId for machine " + targetIpAddress);
				return null;
			}

		} catch (AmazonEC2Exception e) {
			System.out.println("Exception getting AWS InstanceId: " + e.getErrorMessage());

			return null;
		}
	}

	private StandaloneAWSCallsTest() {
		try {

			// build a client that knows how to talk to AWS
			ec2 = AmazonEC2ClientBuilder.defaultClient();

		} catch (Exception e) {
			System.out.print("******* Exception during testAWS:");
			e.printStackTrace();
			System.out.flush();
			System.err.println(e.getMessage());
		}
	}

	public static void main(String[] args) {
		StandaloneAWSCallsTest test = new StandaloneAWSCallsTest();
		System.out.println(test.getAWSInstanceId("54.176.244.87"));
	}

}
