package org.icpc.vcss.util;

import java.io.File;
import java.util.Date;
import java.util.List;

import org.icpc.vcss.core.VersionInfo;
import org.icpc.vcss.database.DatabaseUtilities;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineDAO;

import javax.persistence.EntityManagerFactory;

/**
 * Program to read custom account file and add or update machine defs in
 * database.
 *
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 */
public class LoadAndUpdateDatabase {

	private static void usage() {

		String[] usage = { //
				"Usage: LoadAndUpdateDatabase [--help] [-v] account_file  ", //
				"", //
				" -v    dump all machine definitions before showing summary", //
				"", //
				"account_File - a CSV file with entries for creating hosta/account", //
				"", //
		};

		for (String string : usage) {
			System.out.println(string);
		}
		
		String[] versionLInes = new VersionInfo().getSystemVersionInfoMultiLine(); 
		for (String string : versionLInes) {
			System.out.println(string);
		}

	}

	public static void main(String[] args) {

		if (args.length == 0 || "--help".equals(args[0])) {
			usage();
		} else {

			int idx = 0;
			String arg1 = args[idx];
			boolean verbose = false;
			if (arg1.equals("-v")) {
				verbose = true;
				idx++;
			}

			try {
				String filename = args[idx];
				if (new File(filename).exists()) {

					EntityManagerFactory database = DatabaseUtilities.getCurrentDatabase();
					MachineDAO machineDAO = new MachineDAO(database.createEntityManager());

					List<Machine> allDefs = machineDAO.getAll();
					int defsBefore = allDefs.size();

					DatabaseUtilities.loadDatabaseFromCSVFile(filename, machineDAO);

					if (verbose) {
						DatabaseUtilities.dumpAllMachines("Loaded from accounts_file", machineDAO);
					}

					allDefs = machineDAO.getAll();
					int defsAfter = allDefs.size();

					int defsAdded = defsAfter - defsBefore;

					System.out.println("Load Summary (at " + new Date() + ")");
					System.out.println("Input filename                : " + filename);
					System.out.println("Number of defs before load    : " + defsBefore);
					System.out.println("Number of defs added          : " + defsAdded);
					System.out.println("Current Number of defs        : " + defsAfter);

				} else {
					System.err.println("Cannot read or find file " + filename + ", nothing was loaded.");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		System.exit(0);
	}

}
