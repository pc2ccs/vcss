// Copyright (C) 1989-2019 PC2 Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.logging.Level;

import org.icpc.vcss.core.log.Log;

/**
 * Methods to run program.
 * 
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 */
public class ProgramRunner {

	/**
	 * extra buffer space for the error message to be included in any output.
	 */
	private static final int ERRORLENGTH = 50;

	/**
	 * Run cmdline, using memory to capture output.
	 * 
	 * @param cmdline
	 * @param log
	 * @return
	 */
	public static RunProgramResults runProgram(String cmdline, Log log) {
		return runProgram(cmdline, log, 2000000L);
	}
	
	/**
	 * Run cmdline, using in memory buffer capture output.
	 * @param cmdline
	 * @param log
	 * @return
	 */
	public static RunProgramResults runProgramMemory(String cmdline, Log log) {
		return runProgramMemory(cmdline, log, 2000000L);
	}

	/**
	 * Run cmdline, using in memory buffer capture output.
	 * @param cmdline
	 * @param log
	 * @param maxFileSize
	 * @return
	 */
	public static RunProgramResults runProgramMemory(String cmdline, Log log, long maxFileSize) {

		String message;

		RunProgramResults runProgramResults = new RunProgramResults(cmdline);

		int returnValue = -1;

		try {

			// buffer filled with stdout of program
			ByteArrayOutputStream byteArrayOutputStreamStdout = new ByteArrayOutputStream(65000);
			BufferedOutputStream captureStdout = new BufferedOutputStream(byteArrayOutputStreamStdout);

			// buffer filled witht stderr of program
			ByteArrayOutputStream byteArrayOutputStreamStderr = new ByteArrayOutputStream(65000);
			BufferedOutputStream captureStderr = new BufferedOutputStream(byteArrayOutputStreamStderr);
			
			log.info("Running '" + cmdline + "'");

			String[] env = null;
			Process process = Runtime.getRuntime().exec(cmdline, env);

			// This reads from the stdout of the child process
			BufferedInputStream childOutput = new BufferedInputStream(process.getInputStream());
			// The reads from the stderr of the child process
			BufferedInputStream childError = new BufferedInputStream(process.getErrorStream());

			IOCollector stdoutCollector = new IOCollector(log, childOutput, captureStdout, maxFileSize + ERRORLENGTH);
			IOCollector stderrCollector = new IOCollector(log, childError, captureStderr, maxFileSize + ERRORLENGTH);

			stdoutCollector.start();
			stderrCollector.start();

			// SOMEDAY - use code below to feed stdin into program. dal
//                if (stdinFilename != null ) {
//                    log.info("Using STDIN from file " + stdinFilename);
//
//                    BufferedOutputStream out = new BufferedOutputStream(process.getOutputStream());
//                    BufferedInputStream in = new BufferedInputStream(new FileInputStream(stdinFilename));
//                    byte[] buf = new byte[32768];
//                    int c;
//                    try {
//                        while ((c = in.read(buf)) != -1) {
//                            out.write(buf, 0, c);
//                        }
//                    } catch (java.io.IOException e) {
//                        log.info("Caught a " + e.getMessage() + " while sending input file to process's stdin channel");
//                    }
//
//                    in.close();
//                    out.close();
//                }

			
			// wait for the process to finish execution...
			returnValue = process.waitFor();

			log.info("execution process returned exit code " + returnValue);
			runProgramResults.setProgramReturnCode(returnValue);

			stdoutCollector.join();
			stderrCollector.join();

			captureStdout.close();
			captureStderr.close();

			process.destroy();

			runProgramResults.setExitCode(ExitCode.SUCCESS);

			String [] lines= byteArrayOutputStreamStdout.toString().split("\n");
			runProgramResults.addToStdout(Arrays.asList(lines));

			lines = byteArrayOutputStreamStderr.toString().split("\n");
			runProgramResults.addToStderr(Arrays.asList(lines));

		} catch (IOException e) {
			message = "Exec failure: IOException executing '" + cmdline + "': " + e.getMessage();
			runProgramResults.setException(new ExecuteException(message, e.getCause()));
			log.log(Level.SEVERE, message, e);
			throw new ExecuteException("Exception attempting to run program: " + e.getMessage());
		} catch (Exception e) {
			message = "Exec failure: Exception executing '" + cmdline + "': " + e.getMessage();
			runProgramResults.setException(new ExecuteException(message, e.getCause()));
			log.log(Level.SEVERE, message, e);
			throw new ExecuteException("Exception attempting to run program: " + e.getMessage());
		}

		return runProgramResults;
	}
	
	
	/**
	 * Run cmdline, using disk files to capture output.
	 * 
	 * @param cmdline
	 * @param log
	 * @param maxFileSize
	 * @return
	 */
	public static RunProgramResults runProgram(String cmdline, Log log, long maxFileSize) {

		RunProgramResults runProgramResults = new RunProgramResults(cmdline);

		try {

			File tempStdoutfile = File.createTempFile("runProgram", "stdout.txt");
			File tempStderrfile = File.createTempFile("runProgram", "stderr.txt");

			return runProgram(cmdline, log, 2000000L, tempStdoutfile, tempStderrfile);
		} catch (Exception e) {
			String message = "Exec failure: creating temporary output files  " + e.getMessage();
			runProgramResults.setException(new ExecuteException(message, e.getCause()));
			log.log(Level.SEVERE, message, e);
		}

		return runProgramResults;
	}
	
	/**
	 * Run cmdline, using disk files to capture output.
	 * 
	 * @param cmdline
	 * @param log
	 * @param maxFileSize
	 * @param stdoutFile 
	 * @param stderrFile
	 * @return
	 */
	public static RunProgramResults runProgram(String cmdline, Log log, long maxFileSize, File stdoutFile, File stderrFile) {

		String message;

		RunProgramResults runProgramResults = new RunProgramResults(cmdline);

		int returnValue = -1;

		try {
			
			// stream to capture stdout output
			BufferedOutputStream stdoutlog = new BufferedOutputStream(new FileOutputStream(stdoutFile, false));
			
			// stream to capture stderr output
			BufferedOutputStream stderrlog = new BufferedOutputStream(new FileOutputStream(stderrFile, false));

			log.info("Running '" + cmdline + "'");

			String[] env = null;
			Process process = Runtime.getRuntime().exec(cmdline, env);

			// This reads from the stdout of the child process
			BufferedInputStream childOutput = new BufferedInputStream(process.getInputStream());
			// The reads from the stderr of the child process
			BufferedInputStream childError = new BufferedInputStream(process.getErrorStream());

			IOCollector stdoutCollector = new IOCollector(log, childOutput, stdoutlog, maxFileSize + ERRORLENGTH);
			IOCollector stderrCollector = new IOCollector(log, childError, stderrlog, maxFileSize + ERRORLENGTH);

			stdoutCollector.start();
			stderrCollector.start();

			// SOMEDAY - use code below to feed stdin into program. dal
//                if (stdinFilename != null ) {
//                    log.info("Using STDIN from file " + stdinFilename);
//
//                    BufferedOutputStream out = new BufferedOutputStream(process.getOutputStream());
//                    BufferedInputStream in = new BufferedInputStream(new FileInputStream(stdinFilename));
//                    byte[] buf = new byte[32768];
//                    int c;
//                    try {
//                        while ((c = in.read(buf)) != -1) {
//                            out.write(buf, 0, c);
//                        }
//                    } catch (java.io.IOException e) {
//                        log.info("Caught a " + e.getMessage() + " while sending input file to process's stdin channel");
//                    }
//
//                    in.close();
//                    out.close();
//                }

			
			// wait for the process to finish execution...
			returnValue = process.waitFor();

			log.info("execution process returned exit code " + returnValue);
			runProgramResults.setProgramReturnCode(returnValue);

			stdoutCollector.join();
			stderrCollector.join();

			stdoutlog.close();
			stderrlog.close();

      process.destroy();

			runProgramResults.setExitCode(ExitCode.SUCCESS);

			String[] lines = FileUtilities.loadFile(stdoutFile.getAbsolutePath());
			runProgramResults.addToStdout(Arrays.asList(lines));

			lines = FileUtilities.loadFile(stderrFile.getAbsolutePath());
			if (lines.length == 0)  {
				lines = new String[0];
			}
			runProgramResults.addToStderr(Arrays.asList(lines));

		} catch (IOException e) {
			message = "Exec failure: IOException executing '" + cmdline + "': " + e.getMessage();
			runProgramResults.setException(new ExecuteException(message, e.getCause()));
			log.log(Level.SEVERE, message, e);
			throw new ExecuteException("Exception attempting to run program: " + e.getMessage());
		} catch (Exception e) {
			message = "Exec failure: Exception executing '" + cmdline + "': " + e.getMessage();
			runProgramResults.setException(new ExecuteException(message, e.getCause()));
			log.log(Level.SEVERE, message, e);
			throw new ExecuteException("Exception attempting to run program: " + e.getMessage());
		}

		return runProgramResults;
	}

	
}
