package org.icpc.vcss.util;
/**
 * Offset and name for a variable in a string.
 * 
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 *
 */
 class MatchedVariable {
	private int startOffset;
	private int endOffset;
	private String name;

	public MatchedVariable(int startOffset, int endOffset, String name) {
		super();
		this.startOffset = startOffset;
		this.endOffset = endOffset;
		this.name = name;
	}

	public int getStartOffset() {
		return startOffset;
	}

	public int getEndOffset() {
		return endOffset;
	}

	public String getName() {
		return name;
	}

	public boolean isVariableFound() {
		return endOffset != -1;
	}

}