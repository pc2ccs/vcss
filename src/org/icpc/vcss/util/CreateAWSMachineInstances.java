package org.icpc.vcss.util;

import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import org.icpc.vcss.core.Constants;
import javax.persistence.EntityManagerFactory;

import org.icpc.vcss.core.Utilities;
import org.icpc.vcss.core.VersionInfo;
import org.icpc.vcss.database.DatabaseUtilities;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineDAO;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.InstanceType;
import com.amazonaws.services.ec2.model.ResourceType;
import com.amazonaws.services.ec2.model.RunInstancesRequest;
import com.amazonaws.services.ec2.model.RunInstancesResult;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.TagSpecification;

/**
 * Ensure/create instances based on current number of defined machines.
 *
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 * @author John Clevenger, VCSS Development Team (pc2@ecs.csus.edu)
 */
public class CreateAWSMachineInstances {

	public static final int FATAL_INSUFFICIENT_ARGUMENTS = 1;
	public static final int FATAL_INSUFFICIENT_PROPERTIES_IN_CONTEST_PROPERTIES_FILE = 2;
	public static final int FATAL_EXCEPTION_READING_CONTEST_PROPERTIES_FILE = 3;

	private static void usage() {

		String[] message = { //
				"CreateAWSMachineInstances [--help] mach_def_csv_file contest_properties_file", //
				"CreateAWSMachineInstances [--help] number contest_properties_file", //
				"", //
				"Purpose: to read machine def csv, load vcss DB, create cloud machines, start machines", //
				"", //
				"mach_def_csv_file - a CSV file that contains machine defs including user logins and passwords", //
				"  line format: machine_type, [machine_number], password1 [,password2 [...]]", //
				"  Ex. line: team, 6, pass1, pass2, pass3", //
				"  ", //
				"contest_properties_file  - a Properties file (lines in the form 'key-value' as created by CreateAWSEnvironment", //
				" ", //
				"number - unconditionally create 'number' machines instances", //
				"", //
				new VersionInfo().getSystemVersionInfo(), //
		};

		for (String string : message) {
			System.out.println(string);
		}

	}

	private static void createMachines(String machineDefFilename, String contestPropsFilename, int overrideNumber) {

		try {

			EntityManagerFactory emf = DatabaseUtilities.getCurrentDatabase();
			MachineDAO machineDAO = new MachineDAO(emf.createEntityManager());

			try {

				int newInstanceCount = 0;

				if (overrideNumber > 0 ) {

					newInstanceCount = overrideNumber;

				} else {

					System.out.println("Updating machine definitions in database from file " + machineDefFilename);
					newInstanceCount = loadMachineDefinitionsIntoDB(machineDAO, machineDefFilename);

					System.out.println("Added " + newInstanceCount + " machine definitions to the database");
				}

				if (newInstanceCount <= 0) {
					System.err.println("No machines to be created");
				} else {

					System.out.println("Creating " + newInstanceCount + " new machines");

					awsCreateMachines(newInstanceCount, contestPropsFilename);
				}

			} catch (Exception e) {
				e.printStackTrace();
				System.out.flush();
				System.err.println(e.getMessage());
			}

		} catch (Exception e) {
			e.printStackTrace();
			System.out.flush();
			System.err.println(e.getMessage());
		}

	}

	/**
	 * Load machine defs from file into database.
	 *
	 * @param database
	 * @param machineDefFilename
	 * @return number of new machine (def) rows created in DB.
	 * @throws IOException
	 */
	private static int loadMachineDefinitionsIntoDB(MachineDAO machineDAO, String machineDefFilename) throws IOException {

		List<Machine> allDefs = machineDAO.getAll();
		int defsBefore = allDefs.size();

		DatabaseUtilities.loadDatabaseFromCSVFile(machineDefFilename, machineDAO);

		allDefs = machineDAO.getAll();
		int defsAfter = allDefs.size();

		return defsAfter - defsBefore;
	}

	/**
	 * This method creates the specified number of AWS machine instances using the AWS properties
	 * defined in the specified contestPropertiesFile.
	 *
	 * @param numberToCreate the number of AWS machine instances to create.
	 * @param contestPropertiesFilename a Properties file containing the properties required for connection to AWS.
	 */
	private static void awsCreateMachines(int numberToCreate, String contestPropertiesFilename) {

		//get the properties from the properties file
		Properties props = null;
		try {
			props = FileUtilities.loadPropertiesFile(contestPropertiesFilename);
		} catch (IOException e) {
			System.err.println ("IOException reading contest properties file '" + contestPropertiesFilename + "': " + e.getMessage());
			fatalError("IOException reading contest properties file '" + contestPropertiesFilename + "': " + e.getMessage(), FATAL_EXCEPTION_READING_CONTEST_PROPERTIES_FILE);
		}

		String contestName = props.getProperty(Constants.AWS_CONTEST_NAME_KEY);
        String name = contestName + "-" + UUID.randomUUID();

        String ami_id = props.getProperty(Constants.AWS_AMI_ID_KEY);

        final AmazonEC2 ec2 = AmazonEC2ClientBuilder.defaultClient();

		//Create a new tag using the contest ID
		Tag newResourceTag = new Tag(Constants.AWS_CONTEST_ID_KEY, contestName);

        Tag nameTag = new Tag("Name",name);

		TagSpecification tagSpec = new TagSpecification().withTags(newResourceTag, nameTag);
		tagSpec.setResourceType(ResourceType.Instance);

        RunInstancesRequest runRequest = new RunInstancesRequest()
        							.withImageId(ami_id)
        							.withInstanceType(InstanceType.C52xlarge)
        							.withMinCount(1)
        							.withMaxCount(numberToCreate)
//        							.withMaxCount(1)
        							.withKeyName(props.getProperty(Constants.AWS_KEYPAIR_NAME_KEY))
        							.withSecurityGroupIds(props.getProperty(Constants.AWS_SECURITYGROUP_ID_KEY))
        							.withSubnetId(props.getProperty(Constants.AWS_SUBNET_ID_KEY))
        							.withTagSpecifications(tagSpec);

        RunInstancesResult runResponse = ec2.runInstances(runRequest);

        String instanceId = runResponse.getReservation().getInstances().get(0).getInstanceId();

        System.out.printf("Successfully started EC2 instance %s based on AMI %s", instanceId, ami_id);

	}

//	private static String removeBackslashes(String inString) {
//		String retStr = inString.replaceAll("\\\\", "");
//		return retStr;
//	}


	public static void main(String[] args) {

		/**
		 * Number of machines to create.
		 */
		int number = 0;

		// TODO replace quick and dirty command line parser with a real command line
		// parser.

		if (args.length > 0) {

			if (args[0].contentEquals("--help")) {
				usage();
				System.exit(0);
			}

			String buildNumber = new VersionInfo().getBuildNumber();
			if (buildNumber.isEmpty()) {
				buildNumber = "<DevelopUnset>";
			}

			System.out.println(new Date() + " CreateAWSMachineInstances (build " + buildNumber + ") started");

			int cmdIdx = 0;

			if (args.length - cmdIdx < 2) {
				usage();
				fatalError("Too few arguments, expected 2 parameters: mach_defs file or number, and properties file", FATAL_INSUFFICIENT_ARGUMENTS);
			}

			String source = args[cmdIdx++];
			String machineDefFilename = source;
			String contestPropsFilename = args[cmdIdx++];

			//make sure the contest properties file has all the properties we need to call AWS
			if (!validatePropertiesFile(contestPropsFilename)) {
				fatalError("Contest Properties file does not contain the required properties", FATAL_INSUFFICIENT_PROPERTIES_IN_CONTEST_PROPERTIES_FILE);
			}

			//the first arg could be either a number or a machine_def file; check to see if it's a file
			if (!Utilities.fileExists(source)) {
				//it's not a file; get the explicit number machines to be created
				number = Integer.parseInt(source);
			}

			//at this point the value of "number" is zero if a machine_defs file was provided, or else it is the numeric value
			// from the command line.  In either case, create the required machines
			createMachines(machineDefFilename, contestPropsFilename, number);

			System.out.println(new Date() + " CreateAWSMachineInstances done");
			System.out.println("\nIMPORTANT: any newly-created machines are now RUNNING; be sure to shut them down to avoid charges...");

		} else {
			usage();
			fatalError("Too few parameters", FATAL_INSUFFICIENT_ARGUMENTS);
		}

	}

	/**
	 * This method examines the specified Contest Properties file to insure that all properties which are
	 * required in order to communicate properly with AWS are present in the file and are not null or empty.
	 *
	 * @param contestPropsFilename the properties file to be checked.
	 *
	 * @return true if all required AWS properties are present in the file and have non-null, non-empty values; false otherwise.
	 */
	private static boolean validatePropertiesFile(String contestPropsFilename) {

		//create a list of the properties which must appear in the Contest Properties file
		String [] requiredPropsNames = {Constants.AWS_CONTEST_ID_KEY, Constants.AWS_CONTEST_NAME_KEY, Constants.AWS_KEYPAIR_ID_KEY,
										Constants.AWS_KEYPAIR_NAME_KEY, Constants.AWS_SECURITYGROUP_ID_KEY, Constants.AWS_VPC_ID_KEY,
										Constants.AWS_SUBNET_ID_KEY, Constants.AWS_AMI_ID_KEY} ;
		List<String> requiredPropsList = Arrays.asList(requiredPropsNames);

		try {
			//read the properties file into a Properties object
			Properties props = FileUtilities.loadPropertiesFile(contestPropsFilename);

			//check that each required property was found in the file and has a non-null, non-empty value
			for (String prop : requiredPropsList) {
				if (!props.containsKey(prop) || props.getProperty(prop)==null || props.getProperty(prop).contentEquals("")) {
					System.err.println("Missing contest property: " + prop);
					return false;
				}
			}

			//all required properties were found in the file
			return true;

		} catch (IOException e) {
			System.err.println("IOException reading contest properties file '" + contestPropsFilename + "': " + e.getMessage());
			return false;
		}
	}

	private static void fatalError(String string, int exitCode) {
		System.err.println("CreateAWSMachineInstances: Error " + string);
		System.exit(exitCode);
	}

}
