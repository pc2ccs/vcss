package org.icpc.vcss.util;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.icpc.vcss.core.Constants;
import org.icpc.vcss.core.Utilities;
import org.icpc.vcss.core.VersionInfo;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.AssociateRouteTableRequest;
import com.amazonaws.services.ec2.model.AttachInternetGatewayRequest;
import com.amazonaws.services.ec2.model.AuthorizeSecurityGroupIngressRequest;
import com.amazonaws.services.ec2.model.CreateInternetGatewayRequest;
import com.amazonaws.services.ec2.model.CreateInternetGatewayResult;
import com.amazonaws.services.ec2.model.CreateKeyPairRequest;
import com.amazonaws.services.ec2.model.CreateKeyPairResult;
import com.amazonaws.services.ec2.model.CreateRouteRequest;
import com.amazonaws.services.ec2.model.CreateRouteTableRequest;
import com.amazonaws.services.ec2.model.CreateRouteTableResult;
import com.amazonaws.services.ec2.model.CreateSecurityGroupRequest;
import com.amazonaws.services.ec2.model.CreateSecurityGroupResult;
import com.amazonaws.services.ec2.model.CreateSubnetRequest;
import com.amazonaws.services.ec2.model.CreateSubnetResult;
import com.amazonaws.services.ec2.model.CreateVpcRequest;
import com.amazonaws.services.ec2.model.CreateVpcResult;
import com.amazonaws.services.ec2.model.IpPermission;
import com.amazonaws.services.ec2.model.IpRange;
import com.amazonaws.services.ec2.model.KeyPair;
import com.amazonaws.services.ec2.model.ModifySubnetAttributeRequest;
import com.amazonaws.services.ec2.model.ResourceType;
import com.amazonaws.services.ec2.model.SecurityGroup;
import com.amazonaws.services.ec2.model.Tag;
import com.amazonaws.services.ec2.model.TagSpecification;
import com.amazonaws.services.ec2.model.Vpc;

/**
 * This class creates an Amazon Web Services Virtual Private Cloud (VPC) network environment for a VCSS contest.
 * It reads the user-specified Contest Properties file, which must at least contain a "contestName" property.
 * If any necessary AWS environment values are NOT contained in the Contest Properties file, it invokes
 * the AWS API to create the necessary resource(s), followed by updating the Contest Properties file to reflect
 * the newly-created resources.
 * 
 * AWS resources which the class insures (creating them if necessary) include:  a ContestId Tag; a Virtual Private Cloud Id;
 * a Keypair (including a public key, private key, and keypairId) for accessing the AWS contest resources; and a Security Group Id
 * (where an AWS "Security Group" identifies such things as firewall settings for the Virtual Private Cloud network).
 * 
 * TODO: the generated values should be stored in the current database (for example, in a "Contest" table),
 * not in property files.  Note that this will require altering other tools (for example, {@link CreateAWSMachineInstances})
 * to obtain its input from the database rather than the command line or a property file.
 * 
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 * @author John Clevenger, VCSS Development Team (pc2@ecs.csus.edu)
 */
public class CreateAWSEnvironment {

	public static final int FATAL_INSUFFICIENT_ARGUMENTS = 1;
	public static final int FATAL_CONTEST_PROPERTIES_FILE_NOT_FOUND = 2;
	public static final int FATAL_EXCEPTION_LOADING_CONTEST_PROPERTIES_FILE = 3;
	public static final int FATAL_MISSING_CONTEST_NAME_PROPERTY = 4;
	
	public static final int FATAL_AWS_VPC_CREATION_FAILED = 10;
	public static final int FATAL_EXCEPTION_DURING_ENVIRONMENT_CREATION = 11;
	public static final int FATAL_EXCEPTION_DURING_AWS_KEYPAIR_CREATION = 12;
	public static final int FATAL_EXCEPTION_DURING_PROPERTIES_FILE_UPDATE = 13;
	
	private AmazonEC2 ec2Client;
	
	private List<String> propertyUpdates = new ArrayList<String>();

	/**
	 * This method reads the specified Contest Properties file and uses the information in it to insure (or create
	 * if necessary) the AWS resources for the specified contest.  It automatically updates the specified 
	 * Contest Properties file with any newly-created AWS properties.
	 * 
	 * @param contestPropertiesFilename the name of the Properties file describing the contest to be set up.
	 */
	private void run(String contestPropertiesFilename) {

		try {
			
			//get whatever contest properties have already been defined
			Properties contestProps = null;
			try {
				contestProps = FileUtilities.loadPropertiesFile(contestPropertiesFilename);
			} catch (IOException e) {
				fatalError("Exception loading contest properties file '" + contestPropertiesFilename + "': " + e.getMessage(), 
								FATAL_EXCEPTION_LOADING_CONTEST_PROPERTIES_FILE);
			}
			
			//build a client that knows how to talk to AWS
			ec2Client = AmazonEC2ClientBuilder.defaultClient();
			
			//make sure there is at least a Contest Name in the properties
			String contestName = contestProps.getProperty(Constants.AWS_CONTEST_NAME_KEY);
			if (contestName==null) {
				fatalError("Contest Properties file is missing required " + Constants.AWS_CONTEST_NAME_KEY + " property", FATAL_MISSING_CONTEST_NAME_PROPERTY);
			}
			
			//define an AWS "Tag" to be applied to any new resources we create
			Tag newResourceTag;
			
			//see if we already have an AWS ContestId tag for this named contest
			String awsContestIdTagValue = contestProps.getProperty(Constants.AWS_CONTEST_ID_KEY);
			
			if(awsContestIdTagValue==null) {
				
				//there was not a contest Id tag in the properties; create a new tag using the contest name
				newResourceTag = new Tag(Constants.AWS_CONTEST_ID_KEY, contestName);
				
				//add the tag to the list of things that need to be updated in the Contest Properties file
				propertyUpdates.add(newResourceTag.getKey() + "=" + newResourceTag.getValue());
				
			} else {
				
				//there WAS a contest Id tag already defined in the contest properties; use that for tagging new resources
				newResourceTag = new Tag(Constants.AWS_CONTEST_ID_KEY, awsContestIdTagValue);
			}
			
			
			//ensure that the VPC exists (creating and tagging it if not).  Creating the VPC includes
			// creating a subnet and a set of firewall rules for the VPC.
			createVPC(newResourceTag, contestProps);
			
			//ensure we have a KeyPair for accessing the network
			createKeyPair(newResourceTag, contestProps);
							
			//update the contest properties file with new entries, if any
			updateContestPropertiesFile(contestPropertiesFilename, propertyUpdates);
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.flush();
			System.err.println(e.getMessage());
			fatalError("Exception during Contest Network Environment creation: " + e.getMessage(), FATAL_EXCEPTION_DURING_ENVIRONMENT_CREATION);
		}

	}


//	private String removeBackslashes(String inString) {
//		String retStr = inString.replaceAll("\\\\", "");
//		return retStr;
//	}


	/**
	 * This method checks the specified Contest Properties to see if there is already a VPCId
	 * for the contest; if not, it invokes the AWS API to create a Virtual Private Cloud network,
	 * tagging it with the specified {@link Tag}, and adds the new VPCId to the list of items
	 * which need to be updated in the Contest Properties file.
	 * Creating a new VPC includes creating a new {@link SecurityGroup} for the Virtual Private Network
	 * and saving the SecurityGroupId in the Contest Properties file
	 * as well as creating a set of firewall rules for the VPC.
	 * 
	 * TODO: the VPC ID should be stored in the database, not in a file.
	 * 
	 * @param resourceTag the {@link Tag} to be added to the VPC if a new VPC is created.
	 * @param contestProps a Properties object holding the contest properties, checked to see if a new VPC needs to be created.
	 * 
	 */
	private void createVPC(Tag resourceTag, Properties contestProps) {

		//check to see if there is already a VPCId for the contest (if so, we don't need to create a VPC)
		if (contestProps.getProperty(Constants.AWS_VPC_ID_KEY)==null) {
			
			//there's no VPC ID in the properties, which means no VPC has yet been created, so...

			try {
												
				//define an AWS "Tag" for the VPC name
				Tag vpcNameTag;
				
				//see if we have a contest name in the properties
				String vpcNameTagValue = contestProps.getProperty(Constants.AWS_CONTEST_NAME_KEY);
				if(vpcNameTagValue==null) {
					
					//there was not a contest name in the properties; create a new tag using a generic name
					vpcNameTag = new Tag("Name", "Unknown_Contest_Name");
										
				} else {
					
					//there WAS a contest name already defined in the contest properties; use that for the Name tag
					vpcNameTag = new Tag("Name", vpcNameTagValue);
				}

				//add the name tag and the resource tag to a TagSpecification
				TagSpecification tagSpec = new TagSpecification().withTags(resourceTag,vpcNameTag);
				
				//mark the tagSpec as being intended to identify VPCs
				tagSpec.setResourceType(ResourceType.Vpc);
				
				//construct a VPC-creation request
				CreateVpcRequest newVPCReq = new CreateVpcRequest()
								.withCidrBlock("10.0.0.0/20")
								.withTagSpecifications(tagSpec);
				
				//send the VPC-creation request to the AWS API
				CreateVpcResult result = ec2Client.createVpc(newVPCReq);
				
				//add the VPC Id to the list of things that need to be updated in the Contest Properties file
				Vpc vp = result.getVpc();
				String vpcId = vp.getVpcId();
				propertyUpdates.add(Constants.AWS_VPC_ID_KEY + "=" + vpcId);
				
				//update the tagSpec as being intended to identify subnets
				tagSpec.setResourceType(ResourceType.Subnet);
				
		        //construct a request for building a subnet within the VPC
		        CreateSubnetRequest subnetReq = new CreateSubnetRequest()
		        							.withVpcId(vpcId)
		        							.withCidrBlock("10.0.0.0/20")
		        							.withTagSpecifications(tagSpec);
		        
		        //send the subnet-creation request to the AWS API
		        CreateSubnetResult subnetRes = ec2Client.createSubnet(subnetReq);
		        
				//add the Subnet Id to the list of things that need to be updated in the Contest Properties file
		        String subnetId = subnetRes.getSubnet().getSubnetId();
				propertyUpdates.add(Constants.AWS_SUBNET_ID_KEY + "=" + subnetId);

				//update the tagspec as being intended to identify SecurityGroups
				tagSpec.setResourceType(ResourceType.SecurityGroup);
				
				//construct a request for building a SecurityGroup
				CreateSecurityGroupRequest sgReq = new CreateSecurityGroupRequest()
								.withGroupName("VCSS." + contestProps.getProperty(Constants.AWS_CONTEST_NAME_KEY))
								.withDescription("VCSS." + contestProps.getProperty(Constants.AWS_CONTEST_NAME_KEY) + ".SecurityGroup")
								.withTagSpecifications(tagSpec)
								.withVpcId(vpcId);

		        //send the SecurityGroup-creation request to the AWS API
				CreateSecurityGroupResult sgRes = ec2Client.createSecurityGroup(sgReq);
				
				//add the SecurityGroup Id to the list of things that need to be updated in the Contest Properties file
		        String sgId = sgRes.getGroupId();
				propertyUpdates.add(Constants.AWS_SECURITYGROUP_ID_KEY + "=" + sgId);

				IpRange ip_range = new IpRange()
					    .withCidrIp("0.0.0.0/0");
				
				//construct a request to set the Ingress rules for the SecurityGroup to allow SSH (22) and RDP (3389)
				IpPermission ipPermissionSSH = new IpPermission()
								.withIpProtocol("tcp")
								.withFromPort(22)
								.withToPort(22)
							    .withIpv4Ranges(ip_range);
				
				IpPermission ipPermissionRDP = new IpPermission()
								.withIpProtocol("tcp")
								.withFromPort(3389)
								.withToPort(3389)
							    .withIpv4Ranges(ip_range);
				
				AuthorizeSecurityGroupIngressRequest sgIngressReq = new AuthorizeSecurityGroupIngressRequest()
								.withGroupId(sgId)
								.withIpPermissions(ipPermissionSSH, ipPermissionRDP);
				
				//call AWS with the request (the response doesn't contain anything we need to save...)
				ec2Client.authorizeSecurityGroupIngress(sgIngressReq);
								
				//update the tagspec as being intended to identify InternetGateways
				tagSpec.setResourceType(ResourceType.InternetGateway);
				
				//construct a request for building an Internet Gateway for the VPC
				CreateInternetGatewayRequest gatewayReq = new CreateInternetGatewayRequest().withTagSpecifications(tagSpec);
				
				//send the create-gateway request to the AWS API
				CreateInternetGatewayResult gatewayRequestResult = ec2Client.createInternetGateway(gatewayReq);

				//get the gateway ID out of the result
				String gatewayId = gatewayRequestResult.getInternetGateway().getInternetGatewayId();
				
				//attach the gateway to the VPC
				AttachInternetGatewayRequest attachReq = new AttachInternetGatewayRequest()
								.withInternetGatewayId(gatewayId)
								.withVpcId(vpcId);
				ec2Client.attachInternetGateway(attachReq);
				
				//update the tagspec for a different resource type
				tagSpec.setResourceType(ResourceType.RouteTable);
				
				//create a routing table for the VPC
				CreateRouteTableRequest routeTableReq = new CreateRouteTableRequest()
								.withTagSpecifications(tagSpec)
								.withVpcId(vpcId);
				CreateRouteTableResult routeTableResult = ec2Client.createRouteTable(routeTableReq);
				
				//get the route table Id out of the result
				String routeTableId = routeTableResult.getRouteTable().getRouteTableId();
				
				//create a route that points all traffic (0.0.0.0/0) to the internet gateway
				CreateRouteRequest routeReq = new CreateRouteRequest()
								.withRouteTableId(routeTableId)
								.withDestinationCidrBlock("0.0.0.0/0")
								.withGatewayId(gatewayId);
				ec2Client.createRoute(routeReq);
				
				//associate the route table with the subnet
				AssociateRouteTableRequest associateRouteTableReq = new AssociateRouteTableRequest()
								.withSubnetId(subnetId)
								.withRouteTableId(routeTableId);
				ec2Client.associateRouteTable(associateRouteTableReq);
				
				//set launched instances so they automatically receive a public IP address
				ModifySubnetAttributeRequest modifySubnetReq = new ModifySubnetAttributeRequest()
								.withSubnetId(subnetId)
								.withMapPublicIpOnLaunch(true);
				ec2Client.modifySubnetAttribute(modifySubnetReq);
				
				
				System.out.println("Created VPC with Id " + vpcId + "\n   subnet Id " + subnetId + "\n   security group " + sgId
										+ "\n   gatewayId " + gatewayId + "\n   routeTableId " + routeTableId);
				
			} catch (AmazonServiceException e) {
				e.printStackTrace();
				System.out.flush();
				System.err.println(e.getMessage());
				fatalError("AmazonServiceException during AWS VPC creation: " + e.getMessage(), FATAL_EXCEPTION_DURING_ENVIRONMENT_CREATION);
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.flush();
				System.err.println(e.getMessage());
				fatalError("Exception during AWS VPC creation: " + e.getMessage(), FATAL_EXCEPTION_DURING_ENVIRONMENT_CREATION);
			}
			
		} else {
			System.out.println("Contest properties already contains VPC Id " + contestProps.getProperty(Constants.AWS_VPC_ID_KEY));
		}
	}


	/**
	 * This method checks the specified Contest Properties to see if there is already an AWS KeyPair
	 * for the contest; if not, it invokes the AWS API to create a KeyPair,
	 * tagging it with the specified {@link Tag}, and adds the new KeyPair to the list of items
	 * which need to be updated in the Contest Properties file.
	 * 
	 * TODO: the KeyPair should be stored in the database, not in a file.
	 * 
	 * @param resourceTag the {@link Tag} to be added to the KeyPair if a new KeyPair is created.
	 * @param contestProps a Properties object holding the contest properties, checked to see if a new KeyPair needs to be created.
	 * 
	 */
	private void createKeyPair(Tag resourceTag, Properties contestProps) {
				
		//check to see if there is already a keypair for the contest (if so, we don't need to create one)
		if (contestProps.getProperty(Constants.AWS_KEYPAIR_ID_KEY)==null) {
			
			//there's no KeyPair Id in the properties, which means no KeyPair has yet been created, so...

			try {
				
				//construct an AWS keypair request with the desired properties
				String keyPairName =  getContestId(contestProps) + ".keypair";
				TagSpecification tagSpec = new TagSpecification().withTags(resourceTag);
				tagSpec.setResourceType(ResourceType.KeyPair);
				CreateKeyPairRequest request = new CreateKeyPairRequest().withKeyName(keyPairName).withTagSpecifications(tagSpec);
				
				//request a keypair from AWS
				CreateKeyPairResult result = ec2Client.createKeyPair(request);

				//get the keypair out of the result
				KeyPair keypair = result.getKeyPair();

				//add the KeyPair Id to the list of things that need to be updated in the Contest Properties file
				String keypairID = keypair.getKeyPairId();
				propertyUpdates.add(Constants.AWS_KEYPAIR_ID_KEY + "=" + keypairID);
				
				//add the KeyPair name to the list of things that need to be updated in the Contest Properties file
				String keypairName = keypair.getKeyName();
				propertyUpdates.add(Constants.AWS_KEYPAIR_NAME_KEY + "=" + keypairName);
				
				//add the private key to the list of things that need to be updated in the Contest Properties file
				String privatekey = keypair.getKeyMaterial();
				propertyUpdates.add(Constants.AWS_KEYPAIR_PRIVATE_KEY_KEY + "=" + privatekey);

				System.out.println("Created KeyPair with Id " + keypairID);
				
			} catch (AmazonServiceException e) {
				e.printStackTrace();
				System.out.flush();
				System.err.println(e.getMessage());
				fatalError("AmazonServiceException during AWS Keypair creation: " + e.getMessage(), FATAL_EXCEPTION_DURING_AWS_KEYPAIR_CREATION);
				
			} catch (Exception e) {
				e.printStackTrace();
				System.out.flush();
				System.err.println(e.getMessage());
				fatalError("Exception during AWS Keypair creation: " + e.getMessage(), FATAL_EXCEPTION_DURING_AWS_KEYPAIR_CREATION);
			}
			
		} else {
			System.out.println("Contest properties already contains Keypair Id " + contestProps.getProperty(Constants.AWS_KEYPAIR_ID_KEY));
		}
	}

	
	/**
	 * This method updates the specified contestPropertiesFile by appending to it the properties in the given list.
	 * The method APPENDS the specified properties to the file; it does not alter any existing contents.
	 * 
	 * @param contestPropertiesFilename the name of the file to be updated.
	 * @param propertyUpdates a list of Strings to be added to the file, presumed to be in the form "key=value".
	 */
	private void updateContestPropertiesFile(String contestPropertiesFilename, List<String> propertyUpdates) {

		PrintWriter printWriter = null;
		try {
			
			//create a PrintWriter which will append to the file
			printWriter = new PrintWriter(new FileOutputStream(contestPropertiesFilename, true), true);
			
			//append each propertyUpdate line to the file
			for (String line : propertyUpdates) {
				printWriter.println(line);
			}
			printWriter.close();
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			System.out.flush();
			System.err.println(e.getMessage());
			fatalError("Exception during contest properties file update: " + e.getMessage(), FATAL_EXCEPTION_DURING_AWS_KEYPAIR_CREATION);
		}
	}

	/**
	 * Returns the ContestId from the specified Contest Properties object if found in the Contest Properties object;
	 * otherwise, looks in the current list of pending updates to the Contest Properties (field "propertyUpdates") and
	 * if a ContestId is found there, returns that.
	 * If no ContestId is found in either location, returns null.
	 * 
	 * @param contestProps the ContestProperties to be searched.
	 * 
	 * @return the ContestId from contestprops if found, or else the ContestId from the "pending Contest Properties update" list
	 * 			if found there; otherwise, null.
	 */
	private String getContestId(Properties contestProps) {
		
		//try getting the contest Id out of the specified Properties
		String contestId = contestProps.getProperty(Constants.AWS_CONTEST_ID_KEY);
		
		if (contestId==null) {
			
			//not found in Properties; look to see if maybe there's a new contestId in the pending updates list
			for (String updateLine : propertyUpdates) {
				if (updateLine.startsWith(Constants.AWS_CONTEST_ID_KEY+"=")) {
					contestId = updateLine.substring(updateLine.indexOf("=")+1);
					break;
				}
			}
		}
		
		return contestId;
	}

	/**
	 * The main entry point for CreateAWSEnvironment: insures/creates an AWS Virtual Private Cloud environment 
	 * for the contest defined in the specified properties file.
	 * 
	 * This program expects that AWS access credentials are stored in ~/.aws/credentials and ~/.aws/config, as described 
	 * in AWS documentation at https://docs.aws.amazon.com/credref/latest/refdocs/file-location.html. 
	 * 
	 * @param args [0]: a String giving the name of the Properties file for the contest.
	 */
	public static void main(String[] args) {


		if (args.length > 0) {

			if (args[0].equals("--help")) {
				usage();
				System.exit(0);
			}

			String buildNumber = new VersionInfo().getBuildNumber();
			if (buildNumber.isEmpty()) {
				buildNumber = "<DevelopUnset>";
			}

			System.out.println(new Date() + " CreateAWSContestEnvironment (build " + buildNumber + ") started");

			String contestPropertiesFilename = args[0];
			
			//make sure the properties file exists
			if (!Utilities.fileExists(contestPropertiesFilename)) {
				fatalError("Contest properties file '" + contestPropertiesFilename + "' not found", FATAL_CONTEST_PROPERTIES_FILE_NOT_FOUND);
			}
			
			CreateAWSEnvironment creator = new CreateAWSEnvironment();
			creator.run(contestPropertiesFilename);

			System.out.println(new Date() + " CreateAWSEnvironment done");

		} else {
			usage();
			fatalError("Missing contest properties filename argument", FATAL_INSUFFICIENT_ARGUMENTS);
		}

	}

	private static void usage() {

		String[] usageMessage = { //
				"usage: java CreateAWSEnvironment  [--help]  contestPropertiesFile", //
				"", //
				"Purpose: to create an AWS Virtual Private Cloud network environment for use by VCSS cloud machines", //
				"", //
				"contestPropertiesFile - a file containing the AWS Contest Properties", //
				"", //
				"", //
				new VersionInfo().getSystemVersionInfo(), //
		};

		for (String string : usageMessage) {
			System.out.println(string);
		}

	}

	private static void fatalError(String string, int exitCode) {
		System.err.println("CreateAWSEnvironment: Error " + string);
		System.exit(exitCode);
	}

}
