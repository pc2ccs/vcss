package org.icpc.vcss.util;

/**
 * Exception in attempt to setup or execute a run.
 * 
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 */
public class ExecuteException extends RuntimeException {

	private static final long serialVersionUID = 4121203215386723703L;

	public ExecuteException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExecuteException(String message) {
		super(message);
	}

}
