// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.util;

import java.util.Arrays;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Table;

import org.icpc.vcss.core.VersionInfo;
import org.icpc.vcss.database.DatabaseUtilities;
import org.icpc.vcss.entity.Command;
import org.icpc.vcss.entity.CommandDAO;
import org.icpc.vcss.entity.Login;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineCommandResult;
import org.icpc.vcss.entity.MachineDAO;
import org.icpc.vcss.entity.MachineLogin;

/**
 * Remove all rows from the database - very dangerous
 *
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 */
public class RemoveAllDataFromDatabase {

	/**
	 * Option to actual perform truncate.
	 */
	private static final String EXECUTE_OPTION = "--execute";

	/**
	 * Entities to truncate
	 */
	private static final Class<?>[] tableNames = {
			Command.class,
			MachineCommandResult.class,
			Login.class,
			Machine.class,
			MachineLogin.class,
	};

	private static void usage() {

		String[] usage = { //
				"Usage: RemoveAllDataFromDatabase [--help] " + EXECUTE_OPTION, //
				"", //
				"Truncate tables: " + Arrays.toString(tableNames), //
				"", //
				EXECUTE_OPTION + " remove rows in database", //
				"", //
				"Reads vcss.properties file for database credentials/connection settings", //
				"", //
		};

		for (String string : usage) {
			System.out.println(string);
		}

		String[] versionLInes = new VersionInfo().getSystemVersionInfoMultiLine();
		for (String string : versionLInes) {
			System.out.println(string);
		}

	}

	public static void main(String[] args) {

		if (args.length == 0 || "--help".equals(args[0])) {
			usage();
		} else if (EXECUTE_OPTION.equals(args[0])) {

			showDBStatus("\nBefore:");

			removeAllTableRows();

			showDBStatus("\nAfter:");

		} else {
			System.err.println("To remove all data, specify --execute");

		}
	}

	/**
	 * Shows the status of the database (specifically, the number of machines, commands, and properties), preceded by the specified message.
	 *
	 * @param msg a String to be displayed prior to listing DB status.
	 */
	private static void showDBStatus(String msg) {
		EntityManagerFactory entityManagerFactory = DatabaseUtilities.getCurrentDatabase();
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		MachineDAO machineDAO = new MachineDAO(entityManager);
    CommandDAO commandDAO = new CommandDAO(entityManager);


    System.out.println(msg);

    List<Machine> machines = machineDAO.getAll();
    System.out.println("There are " + machines.size() + " machines.");

    List<Command> commands = commandDAO.getAll();
    System.out.println("There are " + commands.size() + " commands.");
	}

	private static void removeAllTableRows() {

		EntityManagerFactory entityManagerFactory = DatabaseUtilities.getCurrentDatabase();
		EntityManager entityManager = entityManagerFactory.createEntityManager();
		MachineDAO machineDAO = new MachineDAO(entityManager);
		CommandDAO commandDAO = new CommandDAO(entityManager);

		entityManager.getTransaction().begin();
		for (Class<?> entityClass : tableNames) {
			Table t = (Table)entityClass.getAnnotation(Table.class);
			String tableName = t.name();
			System.out.println("Deleting all rows from table: " + tableName);
		    String query = new StringBuilder("DELETE FROM ")
                    .append(tableName)
                    .toString();
		    entityManager.createNativeQuery(query).executeUpdate();

		}
		entityManager.getTransaction().commit();

		List<Machine> machines = machineDAO.getAll();
		System.out.println("There are " + machines.size() + " machines.");
		List<Command> commands = commandDAO.getAll();
		System.out.println("There are " + commands.size() + " commands.");
	}
}
