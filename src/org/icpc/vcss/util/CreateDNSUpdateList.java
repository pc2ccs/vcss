package org.icpc.vcss.util;

import java.util.List;

import javax.persistence.EntityManagerFactory;

import org.icpc.vcss.core.VersionInfo;
import org.icpc.vcss.database.DatabaseUtilities;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineDAO;

/**
 * This class outputs a list of cloud machine names and IP addresses in the
 * format needed for updating a set of DNS entries for the machines. It accepts
 * an optional command line argument "domainName"; if present then the specified
 * domain name is appended to the name of each machine, with a "." separating
 * the machine name from the specified domain name.
 *
 * The result of running the class is a series of lines on stdout, one machine
 * per line, of cloud machine names (with domain name appended if specified),
 * followed by a comma, followed by the machine's IP address.
 *
 * @author John Clevenger, VCSS Development Team <pc2@ecs.csus.edu>
 */
public class CreateDNSUpdateList {

	public static void main(String[] args) {

		String domainName = "";
		boolean addDomainName = false;

		if (args.length > 0) {

			if (args[0].contentEquals("--help")) {
				usage();
				System.exit(0);
			} else {
				// there is a command line argument that's not "--help"; assume it is a domain
				// name
				// to be appended to machine hostnames.
				domainName = args[0];
				addDomainName = true;
			}
		}

		// Note: the following block of code is effectively the same as executing the
		// SQL query:  select machines.name, value from machines left join propvalue on
		//              (propvalue.property_id = 3 and machines.id = propvalue.machine_id)
		//              where name not like 'localVM%';

		// get all the known machines out of the database
    EntityManagerFactory emf = DatabaseUtilities.getCurrentDatabase();
    MachineDAO machineDAO = new MachineDAO(emf.createEntityManager());
		List<Machine> allMachinesList = machineDAO.getAll();

		// check all machines
		boolean foundCloudMachine = false;
		for (Machine machine : allMachinesList) {

			// process only cloud machines
			if (isCloudMachine(machine)) {

				//need to know whether to print out a "not found" message at the end
				foundCloudMachine = true;

				// output the machine name (with domain name appended if provided) and the IP
				String machineName = machine.getName();
				if (addDomainName) {
					machineName += "." + domainName;
				}
				String ipAddr = machine.getIpAddr();

				System.out.println(machineName + "," + ipAddr);

			}
		}

		if (!foundCloudMachine) {
			System.out.println("No cloud machines found");
		}

	}

	/**
	 * Returns an indication of whether the specified machine is a "cloud machine".
	 *
	 * TODO: Currently a primitive test is used: non-cloud machines are assumed to
	 * have names that start with "localVM". This should be replaced by looking at a
	 * flag in the Machine indicating whether it is a cloud or local VM
	 * (as soon as the database is updated to track that status).
	 *
	 * @param machine the machine whose status is to be checked.
	 *
	 * @return true if the specified machine is a "cloud machine"; false otherwise.
	 */
	private static boolean isCloudMachine(Machine machine) {
		if (!machine.getHostname().startsWith("localVM")) {
			return true;
		} else {
			return false;
		}
	}

	private static void usage() {

		String[] message = { //
				"Usage:  java CreateDNSUpdateList [--help] [domainName]", //
				"", //
				"Purpose: to generate a list of AWS machine hostnames and IPs whose DNS entries need to be updated", //
				"", //
				"  --help: display this message and exit", //
				"", //
				"  domainName: an optional string to be appended to the end of each hostname", //
				"", //
				new VersionInfo().getSystemVersionInfo(), //
		};

		for (String string : message) {
			System.out.println(string);
		}

	}
}
