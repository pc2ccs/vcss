package org.icpc.vcss.util;

public class UUIDValidator {

	/**
	 * This method checks whether the specified String matches a valid, non-NIL UUID as specified by ITEF RFC 4122.
	 * The test for a match is NOT case-sensitive (because RFC 4122 explicitly allows either lower- or upper-case
	 * hex digits).
	 * 
	 * @param uuidString a UUID represented as a String.
	 * @return true if the specified uuidString is a valid ITEF RFC 4122 UUID string; false if not.
	 * @see <a href="https://tools.ietf.org/html/rfc4122">http://itef.rfc422</a>
	 */
	public static boolean isValidUUID(String uuidString) {
		
		if (uuidString==null || uuidString.trim().equals("")) {
			return false;
		}
		if (uuidString.matches("(?i)^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-b][0-9a-f]{3}-[0-9a-f]{12}$")) { // (?i)==ignore case
			return true;
		} else {
			return false;			
		}
	}

    /**
     * This method checks to see if the given uuidString is missing a leading zero (only has 7 hex digits
     * in the first element).  If so, it adds a leading zero.  It also converts the string to all lower-case.
     * 
     * The method assumes that the input is reasonably close to a UUID string; e.g. that it has hyphen delimiters.
     * 
     * @param uuidString a string representing a UUID.
     * 
     * @return a new String which is the same as the input String EXCEPT that 
     * 			(1) if the input string starts with fewer than 8 characters preceding the first 
     * 				"-" character, an 8th leading-zero digit is added; and
     * 			(2) the returned String is all lower-case.
     */
	public static String massageUUIDString(String uuidString) {

		if (uuidString==null || uuidString.equals("")) {
			return uuidString;
		}
		
		//get the dash-delimited fields of the string
		String [] fields = uuidString.split("-");
		
		//if the first field is missing leading zeros, add them
		while (fields[0].length()<8) {
			fields[0] = "0" + fields[0];
		}

		
		//rebuild the (updated) string from the fields
		String newString = "";
		int added = 0;
		for (String field : fields) {
			newString += field;
			added++;
			if (added<fields.length) {
				newString += "-";
			}
		}
		
		newString = newString.toLowerCase();
		
		return newString;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
