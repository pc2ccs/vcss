// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.util;

import java.util.Arrays;
import java.util.Date;

import org.icpc.vcss.core.VersionInfo;

/**
 * This class lists and controls AWS instances.
 * 
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 */
public class ControlAWSMachineInstances {

	private static final String LISTMACH_OPTION = "--listmach";
	private static final String START_MACH_OPTION = "--startmach";
	private static final String STOP_MACH_OPTION = "--stopmach";
	private static final String TERMINATE_OPTION = "--terminate";

	private static void usage() {

		String[] message = { //
				"ControlAWSMachineInstances [--help] [options]", //
				"", //
				"Purpose: to read machine def csv, load vcss DB, create cloud machines, start machines", //
				"", //
				LISTMACH_OPTION + " - list instances ", //
				TERMINATE_OPTION + " - terminate (delete) instance", //
				STOP_MACH_OPTION + " - stop instance ", //
				START_MACH_OPTION + " - start instance ", //
				"", //
				LISTMACH_OPTION + "  TODO replace with actual arguments ", //
				TERMINATE_OPTION + "  TODO replace with actual arguments ", //
				STOP_MACH_OPTION + "  TODO replace with actual arguments ", //
				START_MACH_OPTION + "  TODO replace with actual arguments ", //
				"", //
				new VersionInfo().getSystemVersionInfo(), //
		};

		for (String string : message) {
			System.out.println(string);
		}

	}

	static void fatalError(String string, int exitCode) {
		System.err.println("CreateAWSMachineInstances: Error " + string);
		System.exit(exitCode);
	}

	public static void main(String[] args) {

		try {

			if (args.length > 0) {

				if (args[0].contentEquals("--help")) {
					usage();
					System.exit(0);
				}

				String buildNumber = new VersionInfo().getBuildNumber();
				if (buildNumber.isEmpty()) {
					buildNumber = "<DevelopUnset>";
				}

				System.out.println(new Date() + " ControlAWSMachineInstances (build " + buildNumber + ") started");

				String optionOne = args[0];
				String[] rest = new String[args.length - 1];
				System.arraycopy(args, 1, rest, 0, args.length - 1);

				if (LISTMACH_OPTION.contentEquals(optionOne)) {

					listMachine(rest);

				} else if (TERMINATE_OPTION.contentEquals(optionOne)) {

					terminateMachine(rest);

				} else if (STOP_MACH_OPTION.contentEquals(optionOne)) {

					stopMachine(rest);

				} else if (START_MACH_OPTION.contentEquals(optionOne)) {

					startMachine(rest);

				} else {

				}

			} else {
				usage();
				System.exit(5);
			}

		} catch (Exception e) {
			e.printStackTrace(System.err);
		}

	}

	private static void startMachine(String[] args) {
		System.out.println("TODO process startMachine with " + Arrays.toString(args));

	}

	private static void stopMachine(String[] args) {

		System.out.println("TODO process stopMachine with " + Arrays.toString(args));

	}

	private static void terminateMachine(String[] args) {

		System.out.println("TODO process terminateMachine with " + Arrays.toString(args));

	}

	private static void listMachine(String[] args) {

		System.out.println("TODO process listMachine with " + Arrays.toString(args));

	}
}
