package org.icpc.vcss.util;

public class StringUtilities {

	/**
	 * Returns true if the specified String is "yes", "no", "true", or "false".
	 * The comparison is not case-sensitive.
	 * 
	 * @param token the string to be checked
	 * @return true if the string represents a common boolean term; false otherwise.
	 */
	public static boolean isBoolean(String token) {
		if (token==null) {
			return false;
		}
		if (token.equalsIgnoreCase("yes") || token.equalsIgnoreCase("no") 
				|| token.equalsIgnoreCase("true") || token.equalsIgnoreCase("false")) {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
