package org.icpc.vcss.util;

import java.net.InetSocketAddress;

import javax.websocket.Session;

import org.eclipse.jetty.websocket.jsr356.JsrSession;

public class WebsocketSessionUtilities {

	/**
	 * This method returns a String containing the IP address associated with the specified
	 * remote websocket client {@link Session}, or null if the IP address could not be determined.
	 * 
	 * Note: JSR-356 (javax.websocket) does not expose the sender's IP address in a websocket {@link Session}.  
	 * The approach taken in the implementation of the method is to cast the received Session to a Jetty JSR356 
	 * {@link JsrSession} and use {@link JsrSession#getRemoteAddress()} to fetch the address.
	 * This works because the Session parameter passed to this class's {@link #onOpen(Session)} 
	 * and {@link #onMessage(String, Session)} methods implement javax.websocket.Session -- and so does {@link JsrSession}.
	 * If that implementation were to change, this method would have to be adjusted accordingly.
	 * 
	 * @param session a {@link javax.websocket.Session} representing a client websocket connection.
	 * @return a String containing the client's remote IP address, or null if the IP address could not be obtained.
	 */
	public static String getRemoteIPFromSession(Session session) {
	
		String retIPString = null;
		
		try {
			
			JsrSession jSession = (JsrSession) session;
			InetSocketAddress addr = jSession.getRemoteAddress();
			retIPString = addr.toString();
			//removing leading "/" typically added by getRemoteAddress()
			if (retIPString.startsWith("/") && retIPString.length()>1) {
				retIPString = retIPString.substring(1);
			}
			//remove any ":port" from the address
			if (retIPString.contains(":")) {
				retIPString = retIPString.substring(0, retIPString.indexOf(":"));
			}
			
		} catch (ClassCastException e) {
			System.out.println("WebsocketSessionUtilities.getRemoteIPFromSession(): exception casting javax.websocket.Session to Jetty JsrSession: " + e.getMessage());
			//TODO: is there a better way to handle this situation?  As is, since ClassCastException is a RuntimeException
			// (meaning it is not a checked exception), this will propagate all the way out and terminate the JVM.
			// Of course, it may be that this is what we want to happen under such a drastic circumstance... ?
			throw e;
		}
		
		return retIPString;
	}


	/**
	 * This method checks whether the specified hostname is a valid hostname.
	 * 
	 * Note: currently the method is unimplemented and simply returns "true", because it's not clear what
	 * constitutes "valid hostname".  
	 * 
	 * @param hostname a String containing a hostname to be checked for validity.
	 * 
	 * @return true if the specified hostname is a valid computer host name; false if not.
	 */
	public static boolean isValidHostname(String hostname) {
		//TODO: implement this method properly
		System.out.println("WARNING: WebsocketSessionUtilities.isValidHostname() not implemented; returning 'true'");
		return true;
	}


}
