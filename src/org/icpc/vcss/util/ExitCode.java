package org.icpc.vcss.util;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Exit codes for programs.
 * 
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 *
 */
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ExitCode {
	/**
	 * Not set/initialized, program not executed.
	 */
	@JsonProperty("99")
	NOT_INITIALIZED(99),
	/**
	 * Successful run/execution.
	 */
	@JsonProperty("5")
	SUCCESS(5),
	/**
	 * Failed run/execution.
	 */
	@JsonProperty("43")
	FAILED(43);

	int codeValue;

	ExitCode(int codeValue) {
		this.codeValue = codeValue;
	}

	public int getCodeValue() {
		return codeValue;
	}
	
    /**
     * Get an enum element based on its string value.
     */
    public static ExitCode fromString(String text) {
        for (ExitCode code : ExitCode.values()) {
            if (code.codeValue==Integer.parseInt(text)) {
                return code;
            }
        }
        return null;
    }
    
    @JsonCreator
    public static ExitCode forValues(@JsonProperty("codeValue") int value) {
        for (ExitCode code : ExitCode.values()) {
            if (code.codeValue == value) {
                return code;
            }
        }

        return null;
    }

}
