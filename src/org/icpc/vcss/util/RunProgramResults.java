package org.icpc.vcss.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Lob;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

import org.icpc.vcss.core.log.Log;

/**
 * Results for {@link ProgramRunner#runProgram(String, Log) method.
 *
 * <br> Contains return code, stdout, stderr output and program exception.
 *
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 */
@Embeddable
public class RunProgramResults {

	/**
	 * Code/status for executed program.
	 */
	@JsonProperty
	private ExitCode exitCode = ExitCode.NOT_INITIALIZED;

	@JsonProperty
	private String commandLine = "";

	@JsonProperty
	@ElementCollection
  @Lob
	@Column(name = "errline", columnDefinition = "longtext")
	private List<String> stdoutOutput = new ArrayList<String>();

	@JsonProperty
  @ElementCollection
  @Lob
	@Column(name = "outline", columnDefinition = "longtext")
	private List<String> stderrOutput = new ArrayList<String>();

	@JsonProperty
	@JsonInclude(Include.NON_NULL)
	private Exception exception;

	@JsonProperty
	private Integer programReturnCode = -1;

	public RunProgramResults() {
	}

	public RunProgramResults(@JsonProperty("commandLine") String commandLine) {
		this.commandLine = commandLine;
	}

	public String getCommandLine() {
		return commandLine;
	}

	public void setCommandLine(String commendLine) {
		this.commandLine = commendLine;
	}

	public List<String> getStdoutOutput() {
		return stdoutOutput;
	}

	public void addToStdout(Collection<? extends String> collection) {
		stdoutOutput.addAll(collection);
	}

	public void addToStderr(Collection<? extends String> collection) {
		stderrOutput.addAll(collection);
	}

	public List<String> getStderrOutput() {
		return stderrOutput;
	}

	public ExitCode getExitCode() {
		return exitCode;
	}

	@JsonIgnore  //causes deserialization errors; not needed because "exitCode" is a JSON Property.
	public int getExitCodeValue() {
		return exitCode.getCodeValue();
	}

	public void setExitCode(ExitCode exitCode) {
		this.exitCode = exitCode;
	}

	public Exception getException() {
		return exception;
	}

	public void setException(Exception exception) {
		this.exception = exception;
	}

	public void setProgramReturnCode(int returnValue) {
		programReturnCode = returnValue;
	}

	public int getProgramReturnCode() {
		return programReturnCode;
	}

}
