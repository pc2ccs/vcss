// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.websocketMessage;

import javax.websocket.DecodeException;
import javax.websocket.Decoder;
import javax.websocket.EndpointConfig;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * This class decodes a JSON string into a VCSS {@link WebsocketMessage} object.
 * 
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
public class WebsocketMessageJSONDecoder implements Decoder.Text<WebsocketMessage> {

	@Override
	public WebsocketMessage decode(final String jsonText) throws DecodeException {
//		System.out.println("decode called with: " + jsonText);
		ObjectMapper om = new ObjectMapper();
		WebsocketMessage ret;
		try {
			ret = om.readValue(jsonText, WebsocketMessage.class);
		} catch (JsonProcessingException e) {
			// TODO logging - when log instance available use log.log statement
//			log.log(Level.WARNING, "Failed to decode message '"+jsonText+"'", e);
			e.printStackTrace();
			throw new DecodeException(jsonText, "Failed to decode message: '"+jsonText+"' " + e.getMessage());
		}
		return ret;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void init(EndpointConfig arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean willDecode(String arg0) {
		// TODO Auto-generated method stub
		return true;
	}

}
