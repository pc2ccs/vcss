// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.websocketMessage;

import javax.websocket.EncodeException;
import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;


/**
 * This class encodes a VCSS {@link WebsocketMessage} into a JSON string.
 * 
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
public class WebsocketMessageJSONEncoder implements Encoder.Text<WebsocketMessage> {

	@Override
	public String encode(final WebsocketMessage message) throws EncodeException {
	  ObjectMapper om = new ObjectMapper();
	  String encoded;
	  try {
	    encoded = om.writeValueAsString(message);
	  } catch (JsonProcessingException e) {
	    e.printStackTrace();
		throw new EncodeException(message, "failed to encode");
	  }
	  return encoded;
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void init(EndpointConfig arg0) {
		// TODO Auto-generated method stub
		
	}


}
