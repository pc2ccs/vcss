// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.websocketMessage;

/**
 * This enum defines the types of websocket messages which go back and forth between a VCSS client machine
 * and the VCSS server.
 * 
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 */
public enum WebsocketMessageType {

    /**
     * Indicates an unknown message type.
     */
    UNDEFINED("undefined"),

    /**
     * A message containing a egistration request from a client to the server.
     */
    REGISTRATION_REQUEST("registration_request"),
    
    /**
     * A message containing the server's response to a registration request from a client, sent from server back to client.
     */
    REGISTRATION_RESPONSE("registration_response"),
    
    /**
     * A message containing a Command being sent from the server to be executed on a client machine.
     */
    COMMAND("command"),
    
    /**
     * A message sent from a client to the server containing the response from the client regarding a Command 
     * message it previously received. 
     */
    COMMAND_RESPONSE("command_response");

    private String textName = "undefined";
    
    
    /**
     * Set override text name/description for enum element during element construction.
     */
    WebsocketMessageType(String textName) {
        this.textName = textName;
    }
    
    /**
     * Get an enum element based on its textName.
     */
    public static WebsocketMessageType fromString(String text) {
        for (WebsocketMessageType b : WebsocketMessageType.values()) {
            if (b.textName.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }
    
    @Override
    public String toString() {
        return textName;
    }

}
