package org.icpc.vcss.websocketMessage.server;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.attribute.UserPrincipal;
import java.nio.file.attribute.UserPrincipalLookupService;
import java.util.Base64;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;

import javax.websocket.EncodeException;
import javax.websocket.Session;

import org.icpc.vcss.client.RunVCSSClient;
import org.icpc.vcss.core.log.Log;
import org.icpc.vcss.entity.Command;
import org.icpc.vcss.entity.ExecuteCommand;
import org.icpc.vcss.entity.ExecuteCommandResult;
import org.icpc.vcss.entity.GetCommand;
import org.icpc.vcss.entity.GetCommandResult;
import org.icpc.vcss.entity.PutCommand;
import org.icpc.vcss.entity.PutCommandResult;
import org.icpc.vcss.util.ExitCode;
import org.icpc.vcss.util.ProgramRunner;
import org.icpc.vcss.util.RunProgramResults;
import org.icpc.vcss.websocketMessage.WebsocketMessage;
import org.icpc.vcss.websocketMessage.WebsocketMessageType;
import org.icpc.vcss.websocketMessage.client.WSCommandResponseMsg;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * This class is a concrete subclass of {@link WebsocketMessage} representing
 * messages containing {@link Command}s sent from the server to client.
 * 
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
@JsonTypeName("command")
public class WSCommandMsg extends ServerWebsocketMessage {

	@JsonProperty
	private Command command;
	
	@JsonIgnore
	private Log log;
	
	/**
	 * Constructs an <I>uninitialized</i> WSCommandMsg.  Application code should not
	 * call this constructor; instead, use {@link WebsocketMessageFactory#createWSMessage(WebsocketMessageType)}.
	 * @deprecated
	 *   Use constructor WSCommandMsg(Command) instead.
	 */
	@Deprecated
	//TODO: make this private to disallow direct construction -- require use of a factory method
	public WSCommandMsg() { }
	
	/**
	 * Constructs a WSCommandMsg containing the specified command.  The resulting
	 * WSCommandMsg is suitable for transmisssion to client machines for processing.
	 * 
	 * @param command the {@link Command} to be placed in the WSCommandMsg and sent to clients.
	 */
	//TODO: make this private to disallow direct construction -- require use of a factory method
	public WSCommandMsg(Command command) {
		this.command = command;
	}
	
	/**
	 * This method is invoked on the CLIENT side to process the {@link Command} contained in the message.
	 */
	@Override
	public void handleMessage(Session session) {
		
		try {

		//add code here to fetch the CLIENT log
		log = RunVCSSClient.getLog();
		
		boolean success ;
		
		//figure out which type of Command we've been sent
		// (This code should be refactored to use some polymorphic approach so that if
		// new command types are added in the future they don't have to also be added here
		// as separate instanceof checks.  However, this should work for V1...)
		if (command instanceof ExecuteCommand) {
			
			System.out.println ("Client " + session.getId() + " received an Execute command");
			log.info("Client " + session.getId() + " received an Execute command");
			
			ExecuteCommand executeCommand = (ExecuteCommand) command;
			
			//execute the command
			RunProgramResults results = doExecute(executeCommand);
			
			//send the results to the server
			success = sendExecuteResultsToServer(executeCommand, results, session);
			
		} else if (command instanceof PutCommand) {

			System.out.println ("Client " + session.getId() + " received a Put command");
			log.info("Client " + session.getId() + " received a Put command");
			
			PutCommand putCommand = (PutCommand) command;
			
			//execute the command
			String status = doPut(putCommand,session);
			
			//send the results to the server
			success = sendPutResultsToServer(putCommand, status, session);
			
		} else if (command instanceof GetCommand) {

			System.out.println ("Client " + session.getId() + " received a Get command");
			log.info("Client " + session.getId() + " received a Get command");
			
			GetCommand getCommand = (GetCommand) command;
			
			//execute the command
			String base64File = doGet(getCommand);
			
			//send the results to the server
			success = sendGetResultsToServer(getCommand, base64File, session);

		} else {
			
			//unknown command type
			System.out.println ("Client " + session.getId() + " received an unknown command type: " + command.getClass());
			log.warning("Client " + session.getId() + " received an unknown command type: " + command.getClass());
			success = false;
		}
		
		if (success) {
			System.out.println ("Client " + session.getId() + ": command results sent to server");
			log.info("Client " + session.getId() + ": command results sent to server");
		} else {
			System.out.println ("Client " + session.getId() + ": error sending command results to server");
			log.info("Client " + session.getId() + ": error sending command results to server");
		}	
		
		} catch (Exception e) {
			log.log(Level.WARNING, "Problem in handleMessage session="+session, e);
		}
		
	}
	
	/**
	 * This method receives a command line to be executed;
	 * it uses the {@link ProgramRunner} class to execute the command on the local host and then,
	 * if the "responseWanted" parameter is true it sends a response to the server (via the client's 
	 * websocket connection to the server).
	 * 
	 * @param commandLine the command to be executed, including any command line parameters.
	 * @param responseWanted a boolean indicating whether the server wants to be notified of the result of executing the specified command.
	 * @param commandID the ID associated with the command; used to identify the command in any response sent.
	 * @param session the {@link javax.websocket.Session} associated with the received message (used for sending responses to the server).
	 * 
	 * @return true if the execution of the command, and sending a response to the server if requested, was successful; false if not.
	 */

	private RunProgramResults doExecute(Command command) {
		
		ExecuteCommand exCommand = (ExecuteCommand) command;
		String commandLine = exCommand.getText();
		
		System.out.println("WSCommandMsg.doExecute(): executing '" + commandLine + "'");
		log.info("Executing '" + commandLine + "'");
		
		RunProgramResults results = ProgramRunner.runProgram(commandLine, log);
		
		//if the ProgramRunner either threw an exception or failed to set the exit code, consider the command to have failed
		if (results.getException()!=null || results.getExitCode()==ExitCode.NOT_INITIALIZED) {
			results.setExitCode(ExitCode.FAILED);
		}
		
		//temporary debug printing...
		printResults(results);
		
		return results;

	}

	/**
	 * This method sends an {@link ExecuteCommandResult} containing the specified {@link RunProgramResults} 
	 * to the server using the websocket associated with the specified {@link javax.websocket.Session}. 
	 * The specified RunProgramResults are presumed to represent the
	 * result of having executed the specified {@link ExecuteCommand}.
	 * 
	 * @param command the {@link ExecuteCommand} which was executed to produce the specified results.
	 * @param results the {@link RunProgramResults} to be sent to the server.
	 * @param session the {@link javax.websocket.Session} to be used to send the results.
	 * 
	 * @return true if the results were successfully sent to the server; false if an error occurred during sending.
	 */
	private boolean sendExecuteResultsToServer(ExecuteCommand command, RunProgramResults results, Session session) {
		
		System.out.println("Client " + session.getId() + " sending execution results for command '" 
							+ command.getId() + "' to the server");
		log.info("Client " + session.getId() + " sending execution results for command '" 
							+ command.getId() + "' to the server");
				
		//build a object containing both the command and the results
		ExecuteCommandResult result = new ExecuteCommandResult();
		result.setCommand(command);
		result.setResult(results);
		
		//build a message containing the result
		WSCommandResponseMsg msg = new WSCommandResponseMsg(command, result);
		
		try {
			//send the message to the server
			session.getBasicRemote().sendObject(msg);
			
		} catch (IOException | EncodeException e) {
			
			System.out.println("Client " + session.getId() + ": exception sending execution results to the server: " + e.getMessage());
			log.info("Client " + session.getId() + ": exception sending execution results to the server: " + e.getMessage());
			return false;
		}
		
		return true;
	}

	private void printResults(RunProgramResults results) {
		
		System.out.println("Execution results:");
		System.out.println("        Exit code = " + results.getExitCode());
		System.out.println("  Exit code value = " + results.getExitCodeValue());
		System.out.println("           Stdout = " + results.getStdoutOutput());
		System.out.println("           Stderr = " + results.getStderrOutput());
		
	}

	/**
	 * This method receives a {@link PutCommand} containing a Base64-encoded file and a file name path plus 
	 * optional filemode and owner parameters; 
	 * it decodes the specified file and saves it at the specified path location on the local machine.
	 * 
	 * @param command a {@link PutCommand} containing:
	 * <pre>
	 *   <ul>
	 *     <li>the Base64-encoded contents of the file to be "put" (saved) on the local machine.
	 *     <li>the absolute path where the file is to be saved.
	 *     <li>an optional <I>filemode</i>, the Unix permissions mode as an octal number.
	 *     <li>an optional <I>owner</i>, the owner of the file (requires fileMode to also be provided).
	 *   </ul>
	 * </pre>
	 * @param session the {@link Session} for which the {@link PutCommand} is being executed.
	 * 
	 * @return a String "OK" if the file save operation was successful; "ERROR" if not.
	 */
	private String doPut (PutCommand command, Session session) {
		
		//make sure the command has at least a base64 file and a path where the file is to be put
		String base64FileContents = command.getBase64SourceFileContents();
		String destPath = command.getDestFilePath();
		if (base64FileContents==null || base64FileContents.equals("") || destPath==null || destPath.equals("")) {
			System.out.println("WSCommandMsg.doPut(): Client " + session.getId() + ": null or empty base64contents or destination file in PutCommand");
			log.warning("Client " + session.getId() + ": null or empty base64contents or destination file in PutCommand");
			return "ERROR";
		}
		
		//extract optional parameters if present
		String fileMode = command.getFilemode();		
		String owner = command.getOwner();
		
		//first we'll create a temp file; make sure the temp file can be created
		String tmpFilePath = destPath + ".tmp";
		File tmpDestFile = new File(tmpFilePath);
		boolean tempFileSuccessfullyCreated;
		try {
			tempFileSuccessfullyCreated = tmpDestFile.createNewFile();
		} catch (IOException e) {
			System.out.println("WSCommandMsg.doPut(): Client " + session.getId() + ": exception creating temporary destination file '" 
								+ tmpFilePath + "': " + e.getMessage());
			log.warning("Client " + session.getId() + ": exception creating temporary destination file '" + tmpFilePath + "': " + e.getMessage());
			return "ERROR";
		}
		
		if (!tempFileSuccessfullyCreated) {
			System.out.println("WSCommandMsg.doPut(): Client " + session.getId() + ": unable to create temporary destination file '" + tmpFilePath + "'");
			log.warning("WSCommandMsg.doPut(): Client " + session.getId() + ": unable to create temporary destination file '" + tmpFilePath + "'");
			return "ERROR";
		}
		
		//decode the file contents into binary
		byte [] fileContent;
        try {
			fileContent = Base64.getDecoder().decode(base64FileContents);
		} catch (IllegalArgumentException e) {
			System.out.println("WSCommandMsg.doPut(): Client " + session.getId() + ": invalid Base64 file content");
			log.warning("Client " + session.getId() + ": doPut(): invalid Base64 file content");
			
			//TODO: remove the temp file which was created above
			return "ERROR";
		}

        //write the file to the file system
		PrintWriter writeFile;
		try {
			//get a writer to use in writing the file
			writeFile = new PrintWriter(tmpDestFile);
			
			//check if we were asked to set filemode
			if (fileMode!=null && !fileMode.trim().equals("")) {
				
				setPosixFilePermissions(fileMode, tmpDestFile);
			}
			
			//write the decoded file to the file system
			Files.write(tmpDestFile.toPath(), fileContent);
			
			writeFile.close();
			
			//check if we were asked to specify file ownership
			if (owner!=null && !owner.trim().equals("")) {
				
				//update the file owner in the file system
				FileSystem fileSystem = tmpDestFile.toPath().getFileSystem();
				UserPrincipalLookupService service = fileSystem.getUserPrincipalLookupService();
				UserPrincipal userPrincipal = service.lookupPrincipalByName(owner);
				if (userPrincipal != null) {
					Files.setOwner(tmpDestFile.toPath(), userPrincipal);
				}
			}
			
			//attempt to rename the temporary file to the requested file name
			if (tmpDestFile.renameTo(new File(destPath))) {
				
				System.out.println("WSCommandMsg.doPut(): Client " + session.getId() + ": doPut() succeeded for file '" + destPath + "'");
				log.info("Client " + session.getId() + ": doPut() succeeded for file '" + destPath + "'");
				return "OK";
				
			} else {
				
				System.out.println("WSCommandMsg.doPut(): Client " + session.getId() + ": doPut(): rename of tmp file to " + destPath + " failed");
				log.warning("Client " + session.getId() + ": doPut(): rename of tmp file to " + destPath + " failed");
				
				//TODO: remove the temp file which was created above
				return "ERROR";
			}
			
		} catch (Exception e) {
			
			System.out.println("WSCommandMsg.doPut(): Client " + session.getId() + ": exception in doPut(): " + e.getMessage());
			log.warning("WSCommandMsg.doPut(): Client " + session.getId() + ": exception in doPut(): " + e.getMessage());

			//TODO: remove the temp file which was created above
			return "ERROR";
		}
	}
	
	/**
	 * This method sends a {@link PutCommandResult} containing the specified status to the server 
	 * using the websocket associated with the specified {@link javax.websocket.Session}. 
	 * The specified status is presumed to represent the result of having executed the specified {@link PutCommand}.
	 * 
	 * @param command the {@link PutCommand} which was executed to produce the specified results.
	 * @param results the {@link RunProgramResults} information to be sent to the server.
	 * @param session the {@link javax.websocket.Session} to be used to send the results.
	 * 
	 * @return true if the results were successfully sent to the server; false if an error occurred during sending.
	 */
	private boolean sendPutResultsToServer(PutCommand command, String status, Session session) {

		System.out.println("Client " + session.getId() + " sending Put results for command '" 
							+ command.getId() + "' to the server");
		log.info("Client " + session.getId() + " sending Put results for command '" 
							+ command.getId() + "' to the server");

		//build an object containing both the command and the results
		PutCommandResult result = new PutCommandResult();
		result.setCommand(command);
		result.setReturnStatus(status);
		
		//build a message containing the result
		WSCommandResponseMsg msg = new WSCommandResponseMsg(command, result);

		try {

			session.getBasicRemote().sendObject(msg);

		} catch (IOException | EncodeException e) {

			System.out.println("Client " + session.getId() + ": exception sending Put results to the server: " + e.getMessage());
			log.info("Client " + session.getId() + ": exception sending Put results to the server: " + e.getMessage());
			return false;
		}

		return true;
	}

	/**
	 * This method receives a {@link GetCommand} containing a file name; 
	 * it fetches the specified file from the local file system and returns it in Base64-encoded form,
	 * or returns null if the file could not be fetched.
	 * 
	 * @param command a {@link GetCommand} containing a file name.
	 * 
	 * @return a String containing the base64-encoded file, or null if the file could not be fetched from the file system.
	 */
	private String doGet (GetCommand command) {
			
		throw new UnsupportedOperationException("WSCommandMsg.doGet() was invoked but sadly is not yet implemented...");
		
		//TODO: add code here to get the file name out of the command, read the file, Base64-encode the file, and
		//   return the base64 string (or return null if an error occurred).
		
		//TODO: it would be desireable to find a way to pass back a specific failure reason if a failure occurred.
		// For example, returning null could be because the file was not found, or because permission to read it was denied.
		// Just passing back null for either of those conditions isn't quite as helpful as it could be...

	}

	/**
	 * This method uses the websocket associated with the specified {@link javax.websocket.Session}
	 * to send to the server a {@link GetCommandResult} containing the file (in Base64-encoded form)
	 * requested by the specified {@link GetCommand}.
	 * 
	 * @param command the {@link GetCommand} which was executed to obtain the specified file.
	 * @param base64file the Base64-encoded file to be sent to the server, or null if the file could not be fetched from the file system.
	 * @param session the {@link javax.websocket.Session} to be used to send the result.
	 * 
	 * @return true if the results were successfully sent to the server; false if an error occurred during sending.
	 */
	private boolean sendGetResultsToServer(GetCommand command, String base64file, Session session) {

		System.out.println("Client " + session.getId() + " sending Get results for command '" 
							+ command.getId() + "' to the server");
		log.info("Client " + session.getId() + " sending Get results for command '" 
							+ command.getId() + "' to the server");

		String status = base64file==null ? "ERROR" : "OK";
		
		//build an object containing both the command and the results
		GetCommandResult result = new GetCommandResult();
		result.setCommand(command);
		result.setReturnStatus(status);
		result.setBase64File(base64file);

		//build a message containing the result
		WSCommandResponseMsg msg = new WSCommandResponseMsg(command, result);

		try {

			session.getBasicRemote().sendObject(msg);

		} catch (IOException | EncodeException e) {

			System.out.println("Client " + session.getId() + ": exception sending Get results to the server: " + e.getMessage());
			log.info("Client " + session.getId() + ": exception sending Get results to the server: " + e.getMessage());
			return false;
		}

		return true;
	}

	private void setPosixFilePermissions(String fileMode, File file) throws IOException {
		try {
			if (!fileMode.trim().equals("")) {
				int num = Integer.parseInt(fileMode, 8);
				Set<PosixFilePermission> perms = new HashSet<>();
				if ((num & 4) == 4) {
					perms.add(PosixFilePermission.OTHERS_READ);
				}
				if ((num & 2) == 2) {
					perms.add(PosixFilePermission.OTHERS_WRITE);
				}
				if ((num & 1) == 1) {
					perms.add(PosixFilePermission.OTHERS_EXECUTE);
				}
				// shift it 3 bits to the right
				num = num >> 3;
				if ((num & 4) == 4) {
					perms.add(PosixFilePermission.GROUP_READ);
				}
				if ((num & 2) == 2) {
					perms.add(PosixFilePermission.GROUP_WRITE);
				}
				if ((num & 1) == 1) {
					perms.add(PosixFilePermission.GROUP_EXECUTE);
				}
				// shift it 3 bits to the right
				num = num >> 3;
				if ((num & 4) == 4) {
					perms.add(PosixFilePermission.OWNER_READ);
				}
				if ((num & 2) == 2) {
					perms.add(PosixFilePermission.OWNER_WRITE);
				}
				if ((num & 1) == 1) {
					perms.add(PosixFilePermission.OWNER_EXECUTE);
				}
				if (System.getProperty("os.name").startsWith("Windows")) {
					System.out.println("Skipping setPosixFilePermissions on Windows");
					log.info("Skipping setPosixFilePermissions on Windows");
				} else {
					Files.setPosixFilePermissions(file.toPath(), perms);
				}
			}
		} catch (NumberFormatException nfe) {
			System.err.println("setPosixFilePermissions() failed to under chmod " + fileMode + ", " + nfe.getMessage());
		}
	}
}
