package org.icpc.vcss.websocketMessage.server;

import java.util.logging.Level;

import javax.websocket.Session;

import org.icpc.vcss.client.RunVCSSClient;
import org.icpc.vcss.client.WSClientEndpoint;
import org.icpc.vcss.core.log.Log;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * This class is a concrete subclass of {@link ServerWebsocketMessage} representing
 * "registration response" messages from the server back to clients following
 * receipt by the server of a "registration request" message.
 * 
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
@JsonTypeName("registration_response")
public class WSRegistrationResponseMsg extends ServerWebsocketMessage {
	
	public enum WSRegistrationResponseType {UNDEFINED, OK, ERROR}
	
	@JsonProperty
	private WSRegistrationResponseType status ;

	@JsonIgnore
	private Log log;

	/**
	 * This constructor is provided for JSON/Jackson.
	 */
	@SuppressWarnings("unused")
	private WSRegistrationResponseMsg() {};
	
	public WSRegistrationResponseMsg(WSRegistrationResponseType status) {
		this.status = status;
	}

	/**
	 * This method is executed on the CLIENT side; it is how the client handles "registration success" 
	 * messages from the server.
	 * 
	 * @param session the {@link javax.websocket.Session} associated with the message.
	 */
	@Override
	public void handleMessage(Session session) {
		
		try {
		
		//get the CLIENT log
		log = RunVCSSClient.getLog();
	
		switch (status) {
		
			case OK :
	
				System.out.println ("Client " + session.getId() + " received registration success (OK); decrementing continuation latch. ");
				log.info("Client " + session.getId() + " received registration success (OK); decrementing continuation latch. ");
	
				//signal that the registration handshake has completed (successfully in this case)
				WSClientEndpoint.getRegistrationLatch().countDown();
	
				break;
	
			case ERROR:
	
				System.out.println ("Client " + session.getId() + " received registration failure (ERROR) message");
				log.info("Client " + session.getId() + " received registration failure (ERROR) message");
	
				//signal that the registration handshake has completed (failed in this case)
				WSClientEndpoint.getRegistrationLatch().countDown();
	
				break;
			
			default:
				System.out.println ("Client " + session.getId() + " received unknown status type in message: " + status);
				log.info("Client " + session.getId() + " received unknown status type in message: " + status);
		}
		
		} catch (Exception e) {
			log.log(Level.WARNING, "Problem in handleMessage session="+session+" : "+e.getMessage(), e);
		}

	}
}
