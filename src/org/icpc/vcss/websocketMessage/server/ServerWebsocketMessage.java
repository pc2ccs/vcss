// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.websocketMessage.server;

import java.util.Date;

import javax.websocket.Session;

import org.icpc.vcss.websocketMessage.WebsocketMessage;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class is the abstract superclass of messages which flow from the server to a client
 * through the websocket which connects client machines with the VCSS server.
 * 
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
abstract public class ServerWebsocketMessage extends WebsocketMessage {
	
	@JsonProperty
    private long timeStamp;
	
	public ServerWebsocketMessage () {
		this.timeStamp = new Date().getTime();
	}
	
	/**
	 * This method is invoked on the CLIENT process this message for the specified {@link Session}.
	 * Concrete subclasses of {@link ServerWebsocketMessage} implement this method each in their
	 * own message-specific way.
	 * 
	 * @param session the {@link Session} with which this message is associated.
	 */
	abstract public void handleMessage(Session session);
		
	/**
	 * Returns the timestamp giving the time (in the form returned by {@link Date#getTime()})
	 * that this message was created.
	 * 
	 * @return a long giving the time at which this message was created.
	 */
	public long getTimeStamp() {
		return timeStamp;
	}
    
}
