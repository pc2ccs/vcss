/**
 * Classes related to websocket messages received by the VCSS server from client machines.
 */
package org.icpc.vcss.websocketMessage.server;