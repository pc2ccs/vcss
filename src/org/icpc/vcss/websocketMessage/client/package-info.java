/**
 * Classes related to websocket messages handled by (received by) VCSS client machines -- 
 * that is, machines such as teams, judges, and so forth which
 * are participating in a virtual contest being managed by VCSS.
 */
package org.icpc.vcss.websocketMessage.client;