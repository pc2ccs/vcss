package org.icpc.vcss.websocketMessage.client;

import java.util.UUID;
import java.util.logging.Level;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.websocket.Session;

import org.icpc.vcss.core.commands.CommandStatusType;
import org.icpc.vcss.core.commands.MachineCommandResultStatusType;
import org.icpc.vcss.core.log.Log;
import org.icpc.vcss.entity.Command;
import org.icpc.vcss.entity.CommandDAO;
import org.icpc.vcss.entity.ExecuteCommand;
import org.icpc.vcss.entity.ExecuteCommandResult;
import org.icpc.vcss.entity.GetCommand;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineCommandId;
import org.icpc.vcss.entity.MachineCommandResult;
import org.icpc.vcss.entity.MachineCommandResultDAO;
import org.icpc.vcss.entity.MachineDAO;
import org.icpc.vcss.entity.PutCommand;
import org.icpc.vcss.entity.PutCommandResult;
import org.icpc.vcss.util.ExitCode;
import org.icpc.vcss.util.RunProgramResults;
import org.icpc.vcss.webserver.WSServerEndpoint;
import org.icpc.vcss.webserver.websocket.User;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * This class is a concrete subclass of {@link ClientWebsocketMessage}
 * representing messages containing a response from a client machine to a
 * commmand which was sent to the client from the server.
 *
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
@JsonTypeName("command_response")
public class WSCommandResponseMsg extends ClientWebsocketMessage {

	@JsonIgnore
	private Log log;

	@JsonIgnore
	private EntityManagerFactory emf;
	@JsonIgnore
	private EntityManager entityManager;
	@JsonIgnore
	private MachineDAO machineDAO;
	@JsonIgnore
	private CommandDAO commandDAO;
	@JsonIgnore
	private MachineCommandResultDAO machineCommandResultDAO;

	/**
	 * The command to which the client is responding.
	 */
	@JsonProperty
	private Command command;

	/**
	 * The response of executing the command on the client.
	 */
	@JsonProperty
	private MachineCommandResult response;

	/**
	 * @deprecated Use constructor WSCommandResponseMsg(CommandResult) instead.
	 */
	// disallow direct construction -- require use of factory method
	public WSCommandResponseMsg() {
		this.emf = WSServerEndpoint.getCurrentDatabase();
		this.entityManager = emf.createEntityManager();
		this.machineDAO = new MachineDAO(entityManager);
		this.commandDAO = new CommandDAO(entityManager);
		this.machineCommandResultDAO = new MachineCommandResultDAO(entityManager);
	}

	public WSCommandResponseMsg(Command command, MachineCommandResult result) {
		this.command = command;
		this.response = result;
	}

	/**
	 * This method is invoked on the SERVER when it receives a
	 * {@link WSCommandResponseMsg}; it defines how the SERVER processes such
	 * messages.
	 *
	 * @param session the {@link Session} through which the message was sent to the
	 *                server.
	 */
	@Override
	public void handleMessage(Session session) {
		// get the SERVER log for use by the handler
		log = WSServerEndpoint.getLog();
		emf = WSServerEndpoint.getCurrentDatabase();

		// Load the command object from the database(based on the ID provided)
		command = commandDAO.get(command.getId()).orElse(null);

		// figure out what kind of Command this response is for
		if (command != null) {

			// this code block needs to use polymorphism to invoke the handlers, so that
			// addition of future
			// command classes won't require updating this code. However, this should work
			// for V1...
			if (command instanceof ExecuteCommand) {
				handleExecuteCommandResponse((ExecuteCommand) command, (ExecuteCommandResult) response, session);
			} else if (command instanceof PutCommand) {
				handlePutCommandResponse((PutCommand) command, (PutCommandResult) response, session);
			} else if (command instanceof GetCommand) {
				handleGetCommandResponse((GetCommand) command, session);
			} else {
				System.out.println("WSCommandResponse.handleCommand(): unknown command in CommandResponse message: "
						+ command.getClass());
				log.warning("Unknown command in CommandResponse message: " + command.getClass());
			}

		} else {
			System.out.println(
					"WSCommandResponseMsg.handleMessage(): got a null command in a Command Response message!?!");
			log.warning("WSCommandResponseMsg.handleMessage(): got a null command in a Command Response message!?!");
		}

		// Determine how many pending results are left for this command, and then update the overall status in the database
		long pendingResults = machineCommandResultDAO.countByStatus(command, MachineCommandResultStatusType.PENDING);
		if (pendingResults > 0) {
			command.setStatus(CommandStatusType.PARTIALlY_COMPLETED);
		} else {
			command.setStatus(CommandStatusType.COMPLETED);
		}
		commandDAO.save(command);
	}

	private void handleExecuteCommandResponse(ExecuteCommand command, ExecuteCommandResult result, Session session) {

		System.out.println("WSCommandResponseMsg.handleExecuteCommandResponse(): processing response from client "
				+ session.getId() + " to command " + command.getId());
		log.info("Processing response from client " + session.getId() + " to command " + command.getId());

		// get the User associated with the client websocket session
		User user = WSServerEndpoint.getConnections().get(session.getId());
		if (user == null) {
			System.out.println(
					"handleExecuteCommandResponse(): unable to find User associated with client websocket session "
							+ session.getId());
			log.warning("Unable to find User associated with client websocket session " + session.getId());
			// TODO: should we do something else here, like throw an exception? What CAN we
			// do?
			return;
		}

		// get the machine uuid for the client
		String machineUUIDString = user.getMachineUUIDAsString();
		UUID machineUUID = UUID.fromString(machineUUIDString);

		Machine machine = machineDAO.findByUuid(machineUUID);
//		Command command = commandDAO.get(command.getId()).orElse(null);

		// get the current "response" stored in the database for this command for this
		// machine
		MachineCommandResult curResult = machineCommandResultDAO.get(new MachineCommandId(machine.getId(), (long) command.getId())).orElse(null);

		if (curResult == null) {
			System.out.println("WSCommandResponseMsg.handleExecuteCommandResponse(): "
					+ "unable to find MachineCommandResult for execution_result message from " + "machine "
					+ machineUUIDString + " for command " + command.getId());
			log.warning("Unable to find MachineCommandResult for execution_result message from " + "machine "
					+ machineUUIDString + " for command " + command.getId());
			// TODO: should we do something else here, like throw an exception? What CAN we
			// do?
			return;
		}

		// update the response with the command response
		RunProgramResults runResults = result.getResults();
		// build a single string as the "response". We really need to be able to store a
		// response OBJECT in the database...
		String exitCode = runResults.getExitCode().toString();
		String stdout = String.join("\\n", runResults.getStdoutOutput());
		String stderr = String.join("\\n", runResults.getStderrOutput());
		String response = "ExitCode:" + exitCode + "  Stdout: " + stdout + "  Stderr: " + stderr;

    curResult.setResponse(response);

    if (curResult instanceof ExecuteCommandResult) {
      ExecuteCommandResult ecr = (ExecuteCommandResult) curResult;
      ecr.setResult(runResults);
    }

		// update the response with the appropriate status
		MachineCommandResultStatusType status;
		Exception ex = runResults.getException();
		if (ex != null) {
			status = MachineCommandResultStatusType.FAILED;
		} else {
			if (runResults.getExitCode() == ExitCode.FAILED) {
				status = MachineCommandResultStatusType.FAILED;
			} else {
				status = MachineCommandResultStatusType.SUCCESS;
			}
		}
		curResult.setStatus(status);

		// put the updated response back in the database
		machineCommandResultDAO.save(curResult);
	}

	private void handleGetCommandResponse(GetCommand command, Session session) {
		// TODO Auto-generated method stub
		throw new UnsupportedOperationException("WSCommandResponse.handleGetCommandResponse(): not implemented!");
	}

	private void handlePutCommandResponse(PutCommand command, PutCommandResult result, Session session) {

		System.out.println("WSCommandResponseMsg.handlePutCommandResponse(): processing response from client "
				+ session.getId() + " to command " + command.getId());
		log.info("Processing response from client " + session.getId() + " to command " + command.getId());

		// get the User associated with the client websocket session
		User user = WSServerEndpoint.getConnections().get(session.getId());
		if (user == null) {
			System.out
					.println("handlePutCommandResponse(): unable to find User associated with client websocket session "
							+ session.getId());
			log.warning("Unable to find User associated with client websocket session " + session.getId());
			// TODO: should we do something else here, like throw an exception? What CAN we
			// do?
			return;
		}

		// get the machine uuid for the client
		String machineUUIDString = user.getMachineUUIDAsString();
		UUID machineUUID = UUID.fromString(machineUUIDString);

		Machine machine = machineDAO.findByUuid(machineUUID);
//		Command command = commandDAO.get(command.getId()).orElse(null);

		// get the current "response" stored in the database for this command for this
		// machine
		MachineCommandResult curResult = machineCommandResultDAO.get(new MachineCommandId(machine.getId(), (long) command.getId())).orElse(null);

		if (curResult == null) {
			System.out.println("WSCommandResponseMsg.handlePutCommandResponse(): "
					+ "unable to find MachineCommandResult for execution_result message from " + "machine "
					+ machineUUIDString + " for command " + command.getId());
			log.warning("Unable to find MachineCommandResult for execution_result message from " + "machine "
					+ machineUUIDString + " for command " + command.getId());
			// TODO: should we do something else here, like throw an exception? What CAN we
			// do?
			return;
		}

		// update the response with the command result
		String putResultStatus = result.getReturnStatus();

		curResult.setResponse(putResultStatus);

		// update the response with the appropriate status
		MachineCommandResultStatusType status;
		if (putResultStatus.equals("OK")) {
			status = MachineCommandResultStatusType.SUCCESS;
		} else {
			status = MachineCommandResultStatusType.FAILED;
		}

		curResult.setStatus(status);

		// put the updated response back in the database
		machineCommandResultDAO.save(curResult);
	}
}
