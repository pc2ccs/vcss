package org.icpc.vcss.websocketMessage.client;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import javax.json.Json;

import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Test;


/**
 * Tests for updating icpc-vcss.org DNS entries at GoDaddy.
 *
 * @author John Clevenger, ICPC VCSS Development Team
 *
 */
public class GoDaddyDNSUpdateTest {

	/**
	 * This method attempts to add a new "A-record" for a fake IP address into the icpc-vcss.org domain at GoDaddy.
	 * It checks the return code from the GoDaddy API and assumes that the addition worked if the return code is "OK".
	 * To actually verify that it worked one would have to login to GoDaddy using icpc-vcss.org credentials and look
	 * at the table of Records.
	 */
	@Test
	public void testAddDNSEntry() {
			
			String machName = "john";
			String contestName = "testContest";
		
			String json = Json.createObjectBuilder()
		            .add("data", "1.2.3.5")	//change this address slightly if you want to see a change at GoDaddy...
		            .add("port", 1)
		            .add("priority", 0)
		            .add("protocol", "string")
		            .add("service", "string")
		            .add("ttl", 600)
		            .add("weight", 1)
		            .build()
		            .toString();
			
			json = "[" + json + "]";
			
			CloseableHttpClient httpClient = HttpClientBuilder.create().build();
			
			try {
			    HttpPut request = new HttpPut("https://api.godaddy.com/v1/domains/icpc-vcss.org/records/A/" + machName + "." + contestName);
			    StringEntity params = new StringEntity(json.toString());
			    request.addHeader("accept", "application/json");
			    request.addHeader("content-type", "application/json");
			    request.addHeader("Authorization", "sso-key eoMJEu9vgZu2_Jbbk4qLJvBam9NKL8kSTLw:KyGXoKu2RUZHsmaRAEkbEF");
			    request.setEntity(params);
			    CloseableHttpResponse response = httpClient.execute(request);

			    StatusLine statusLine = response.getStatusLine();
			    int statusCode = statusLine.getStatusCode();
			    
			    assertEquals(HttpStatus.SC_OK, statusCode);
			    
			    
			} catch (Exception ex) {
			    System.err.println("Exception during DNS update: " + ex.toString());
			    ex.printStackTrace();
			} finally {
			    try {
					httpClient.close();
				} catch (IOException e) {
				    System.err.println("Exception closing httpClient: " + e.toString());
					e.printStackTrace();
				}
			}
			
			
	}



	public static void main(String[] args) {

	}
}
