// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.websocketMessage.client;

import java.util.Date;

import javax.websocket.Session;

import org.icpc.vcss.websocketMessage.WebsocketMessage;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * This class is the abstract superclass of messages which flow from client machines to the VCSS server
 * through the websocket which connects client machines with the VCSS server.
 * 
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
abstract public class ClientWebsocketMessage extends WebsocketMessage {
	
	@JsonProperty
    private long timeStamp;

	public ClientWebsocketMessage () {
		this.timeStamp = new Date().getTime();
	}
	

	/**
	 * This method is invoked ON THE SERVER to process this message for the specified {@link Session}.
	 * Concrete subclasses of {@link WebsocketMessage} implement this method each in their
	 * own message-specific way.
	 * 
	 * @param session the {@link Session} with which this message is associated.
	 */
	abstract public void handleMessage(Session session);
    
}
