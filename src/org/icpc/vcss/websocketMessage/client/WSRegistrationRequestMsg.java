package org.icpc.vcss.websocketMessage.client;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.logging.Level;

import javax.json.Json;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.websocket.EncodeException;
import javax.websocket.Session;

import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.icpc.vcss.core.Constants;
import org.icpc.vcss.core.commands.CommandStatusType;
import org.icpc.vcss.core.commands.MachineCommandResultStatusType;
import org.icpc.vcss.core.log.Log;
import org.icpc.vcss.entity.CommandDAO;
import org.icpc.vcss.entity.ExecuteCommand;
import org.icpc.vcss.entity.ExecuteCommandResult;
import org.icpc.vcss.entity.Login;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineCommandResultDAO;
import org.icpc.vcss.entity.MachineDAO;
import org.icpc.vcss.entity.MachineLogin;
import org.icpc.vcss.util.FileUtilities;
import org.icpc.vcss.util.UUIDValidator;
import org.icpc.vcss.util.WebsocketSessionUtilities;
import org.icpc.vcss.webserver.WSServerEndpoint;
import org.icpc.vcss.webserver.websocket.User;
import org.icpc.vcss.websocketMessage.server.ServerWebsocketMessage;
import org.icpc.vcss.websocketMessage.server.WSCommandMsg;
import org.icpc.vcss.websocketMessage.server.WSRegistrationResponseMsg;
import org.icpc.vcss.websocketMessage.server.WSRegistrationResponseMsg.WSRegistrationResponseType;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.AmazonEC2Exception;
import com.amazonaws.services.ec2.model.CreateTagsRequest;
import com.amazonaws.services.ec2.model.DescribeNetworkInterfacesRequest;
import com.amazonaws.services.ec2.model.DescribeNetworkInterfacesResult;
import com.amazonaws.services.ec2.model.Filter;
import com.amazonaws.services.ec2.model.NetworkInterface;
import com.amazonaws.services.ec2.model.Tag;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * This class is a concrete subclass of {@link ClientWebsocketMessage} representing
 * "registration request" messages from clients to the VCSS server.
 *
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
@JsonTypeName("registration_request")
public class WSRegistrationRequestMsg extends ClientWebsocketMessage {

	@JsonProperty
	private String machineUUIDString;

	@JsonProperty
	private String hostname;

	@JsonIgnore
	private Log log;

	private EntityManagerFactory emf;
	@JsonIgnore
	private EntityManager entityManager;
	@JsonIgnore
	private MachineDAO machineDAO;
	@JsonIgnore
	private CommandDAO commandDAO;
	@JsonIgnore
	private MachineCommandResultDAO machineCommandResultDAO;

	/**
	 * The Amazon Client Builder used to relay AWS requests
	 * NOTE: do not instantiate this here; it needs to be instantiated within method registerClient()
	 *  (which is executed as part of {@link WSRegistrationRequestMsg#handleMessage(Session)} ON THE SERVER SIDE)
	 *  because instantiation requires having AWS credentials, which are not available on the client side.
	 *
	 */
    private AmazonEC2 ec2 = null ;  //do not instantiate this here; it needs to be instantiated in registerClient() -- see above

	/**
	 * This constructor is provided for JSON/Jackson.
	 */
	@SuppressWarnings("unused")
	private WSRegistrationRequestMsg() {
		this.emf = WSServerEndpoint.getCurrentDatabase();
		this.entityManager = emf.createEntityManager();
		this.machineDAO = new MachineDAO(entityManager);
		this.commandDAO = new CommandDAO(entityManager);
		this.machineCommandResultDAO = new MachineCommandResultDAO(entityManager);
	};

	/**
	 * Constructs a websocket registration request for the specified machine to be sent from a client to the server.
	 *
	 * @param machineUUIDString a String containing the UUID of the client machine.
	 * @param machineName a String containing the name of the client machine (e.g. "team1" or "judge3").
	 */
	public WSRegistrationRequestMsg(String machineUUIDString, String machineName) {
		this.machineUUIDString = machineUUIDString;
		this.hostname = machineName;
	}

  // We don't want to run inside a transaction(we'll handle it ourselves)
  @Override
  public boolean requireTransaction() {
    return false;
  }

	/**
	 * This method is invoked ON THE SERVER to handle the {@link WSRegistrationRequestMsg} from a client.
	 */
	@Override
	public void handleMessage(Session session) {

		try {

		//get the SERVER log for use by the handler
		log = WSServerEndpoint.getLog();

		//get the database being used by the server
		emf = WSServerEndpoint.getCurrentDatabase();

        log.info("Processing registration request from client " + session.getId());

		//make sure we have sane input
		if (session==null || machineUUIDString==null || machineUUIDString.equals("") || hostname==null || hostname.equals("")) {
			log.log(Log.WARNING, "Ill-formed  message (Session, uuid, or hostname is null or empty)");
    		sendRegistrationFailureMessageToClient(session);
		}

		//check for valid uuid and hostname
		if (!UUIDValidator.isValidUUID(machineUUIDString) || !WebsocketSessionUtilities.isValidHostname(hostname)) {
			log.warning("Illegal message data (invalid uuid or hostname)");
    		sendRegistrationFailureMessageToClient(session);
		}

    	//force bogus UUIDStrings (like from Ubuntu) to have leading zeros; convert UUIDString to lower-case.
        machineUUIDString = UUIDValidator.massageUUIDString(machineUUIDString);

		log.info("Attempting to register client " + session.getId() + " into VCSS with uuid "
								+ machineUUIDString + " and hostname " + hostname);

		boolean success = registerClient(session, machineUUIDString, hostname);

		if (success) {
			sendRegistrationSuccessMessageToClient(session);
		} else {
			sendRegistrationFailureMessageToClient(session);
		}

		} catch (Exception e) {
			log.log(Level.SEVERE, "Error handling message", e);
		}

	}

	/**
	 * This method is executed on the server to register the specified client machine with the server.
	 *
	 * @param session the Websocket {@link Session} through which the client machine is connected.
	 * @param machineUuidAsString a String containing the UUID of the client machine.
	 * @param machineName a String containing the name of the client machine (e.g. "team1" or "judge3").
	 *
	 * @return true if the client machine was successfully registered with the server; false if not.
	 */
	private boolean registerClient(Session session, String machineUuidAsString, String machineName) {
		
		try {

			//get the properties from the properties file
			Properties props = null;
			String propsFileName = Constants.VCSS_PROPERTIES_FILENAME;
			try {
				props = FileUtilities.loadPropertiesFile(propsFileName);
			} catch (IOException e) {
				log.severe("IOException reading contest properties file '" + propsFileName + "': " + e.getMessage());
				return false;
			}

			//get the name of the contest
			String contestName = props.getProperty(Constants.AWS_CONTEST_NAME_KEY);
			if (contestName==null || contestName.contentEquals("")) {
				contestName = "UnknownContest";
			}
			
			// add client to clientUUIDtoSessionIDMap map
			synchronized (WSServerEndpoint.getClientUUIDtoSessionIDMap()) {
				WSServerEndpoint.getClientUUIDtoSessionIDMap().put(machineUuidAsString,session.getId());
			}

			//add the client's info to the websocket sessions tables
			synchronized (WSServerEndpoint.getConnections()) {
				User user = WSServerEndpoint.getConnections().get(session.getId());
				user.setMachineUUIDAsString(machineUuidAsString);
				user.setUserName(machineName);
			}

			//get a real UUID object matching the client's uuid string (needed as a database key)
			UUID uuid = UUID.fromString(machineUuidAsString);
			
			//get the calling machine's IP address
			String machineIPAddr = WebsocketSessionUtilities.getRemoteIPFromSession(session);

			//strip off any "port" indicator from the IP address
			if (machineIPAddr.contains(":")) {
				machineIPAddr = machineIPAddr.substring(0, machineIPAddr.indexOf(":"));
			}

			// check if a machine definition with the specified UUID is already in the database
			Machine m = machineDAO.findByUuid(uuid);
			if (m != null) {
				
				//this is a machine that has called us before; update its db entry
				log.info("Updating previously-registered machine in database: " + m.getName());
				
				//TODO:  on initial registration connection, if AWS call "getInstanceId" fails, 
				//TODO:  set a flag in the database so that on a re-registration call we can try again to get the instanceId.
				//TODO:  (or maybe set a timer task and just try it again on our own without waiting for a new registration request?)

				m.setIpAddr(machineIPAddr);
//				m.setHostname(machineName);  //don't change hostname for previously-registered machines
				m.setRegistered(true);
				m.setConnected(true);
				machineDAO.save(m);
				
				//update the DNS with the (possibly changed) IP address of this machine (AWS machines can change IPs when restarted)
				String name = m.getName();
				boolean dnsUpdatedOK = updateDNS(name, contestName, machineIPAddr);
						
				if (dnsUpdatedOK) {
					return true;
				} else {
					return false;
				}

			} else {

				//this is a machine that has not called us before; determine if it's local or cloud
				// by looking at its Fully-Qualified Domain Name:

				//separate the IP address octets into individual strings
				String [] ipOctets = machineIPAddr.split("\\.");

				if (ipOctets.length!=4) {
					log.severe("IP address from getRemoteIPFromSession() is not length 4; cannot continue" );
					return false;
				}

				//convert the IP octet strings to bytes
				byte [] ipAddrBytes = new byte[ipOctets.length];
				for (int i=0; i<ipOctets.length; i++) {
					int val = Integer.parseInt(ipOctets[i]);
					ipAddrBytes[i] = (byte) (val & 0xFF) ;
				}

				//get the Fully-Qualified Domain Name for the specified IP Address
			    InetAddress address = InetAddress.getByAddress(ipAddrBytes);
			    String fqdn = address.getCanonicalHostName();

				Machine curMachine = null;
				if (fqdn.trim().endsWith(Constants.CLOUD_DOMAIN_NAME)) {  //TODO: the cloud domain name should be read from vcss.properties

					//this is a cloud machine calling us for the first time
					log.info("Adding new cloud machine to database: " + fqdn + " (" + machineIPAddr + ")");

					//see if there is an available MachineDef in the database (i.e., one which has no UUID)
					//   -- this happens when a cloud machine was defined ahead of time by the Contest Administrator
					Machine availableMD = getAvailableMachine(uuid);

					if (availableMD != null) {
						//there was an available (unassigned) Machine Definition; use it
						curMachine = availableMD;
					} else {

						//there was no available Machine Def; for some reason there have been more registration calls from
						// cloud machines than were originally created in the database
						log.warning("All initially-created cloud machine definitions have been assigned; creating new machine definition");

						//add a new machine definition to the database
						curMachine = new Machine("unknownCloudVM_" + machineName);
					}
					
					log.info("Machine name from selected row in database = " + curMachine.getName());

					//update the Instance Name in AWS so we can see from the name in the AWS console 
					// which "team" is using the instance:

					//determine the new name to be assigned to the AWS instance
					String newAWSInstanceName = contestName + "-" +curMachine.getName()  ;

					//make sure we have an AWS access object.  This needs to be instantiated here
					// because this code is executed on the Server side; client side code does not
					// have AWS credentials.
					if (ec2==null) {
						ec2 = AmazonEC2ClientBuilder.defaultClient();
					}

					//call AWS "DescribeInstances" to get the InstanceId for this machine
					String instanceId = getAWSInstanceId(machineIPAddr);

					if (instanceId!=null) {

						log.info("Updating AWS InstanceId Name for machine " + fqdn + " (" + machineIPAddr + ")");

						//build a request for changing an attribute of the instance
						Tag t = new Tag().withKey("Name").withValue(newAWSInstanceName);
						CreateTagsRequest ctr = new CreateTagsRequest().withTags(t).withResources(instanceId);

						//update the Instance Name in AWS
						ec2.createTags(ctr);

						// update the database to store the AWS InstanceId
						curMachine.setEc2Id(instanceId);

					} else {
						log.severe("Cannot find AWS InstanceId for machine " + fqdn + " (" + machineIPAddr + ")");
						
						//don't automatically fail registration just because we couldn't get the InstanceId
						// (it might be that AWS rejected our request, e.g. for "too many requests")
						
						//TODO: see the notes in getAWSInstanceId() about
						//TODO: consider starting a Timer task to attempt to fetch the InstanceId later (because for example the current
						//TODO: attempt might have been rejected for "too many requests")

						// return false;
					}

				} //end "if this is a cloud machine" 
				
				else {

					//this is a local machine calling us for the first time; create a machine def for it in the database
					log.info("Adding new local machine to database: " + fqdn + " (" + machineIPAddr + ")");

					curMachine = new Machine("localVM_" + machineName);
				}

				//if this is a cloud machine, update the GoDaddy DNS so it can properly resolve this new machine
				if (fqdn.trim().endsWith(Constants.CLOUD_DOMAIN_NAME)) {  
					
					String name = curMachine.getName();
					boolean dnsUpdatedOK = updateDNS(name, contestName, machineIPAddr);
					
					if (!dnsUpdatedOK) {
						return false;
					}
				}
				
				//update database
				log.info("Updating newly-registered machine in database");

				curMachine.setUuid(uuid);
				curMachine.setIpAddr(WebsocketSessionUtilities.getRemoteIPFromSession(session));
				curMachine.setHostname(hostname);
				Date now = new Date();
				curMachine.setRegistered(true);
				curMachine.setRegisteredTime(now);
				curMachine.setConnected(true);
				curMachine.setConnectedTime(now);
				curMachine = machineDAO.save(curMachine);

				
				//When we're here, we're inside the "else" indicating that curMachine is a machine that has not contacted us before.
				// If curMachine is actually a cloud machine it will have account/password information in the database;
				// if that information is present (it will only be present for cloud machines), we need to tell the machine
				// to create those accounts.

				//get a list of accounts to be created (if there are any)
				Set<MachineLogin> logins = curMachine.getLogins();
				Map<String,String> accounts = new HashMap<>();
				for (MachineLogin machineLogin : logins) {
					Login l = machineLogin.getLogin();
					accounts.put(l.getName(), l.getPassword());
				}

				//check if there are any accounts to be created (if the client is a local VM there won't be...)
				if (accounts==null || accounts.isEmpty()) {
					log.info("No accounts listed in database for client; skipping account creation step");
				} else {
					//send messages to the client instructing it to create local accounts
					//NOTE that this code only SENDS account-creation messages to the client; it has no way of detecting
					// whether account creation was successful -- that comes in the form of "Execution_Results" messages
					// sent back by the client...
					sendCreateAccountsMessagesToClient(session, accounts);
				}

				//if this is a cloud machine that has contacted us for the first time, we MAY need to add code here to make
				// a call to the AWS API to obtain the FQDN of the machine and update "hostname" in the database
				// (the code above theoretically got the correct FQDN from a reverse DNS lookup based on the IP of the client
				// and put it in the database, but it might be necessary to update it directly from AWS...).
				// This would require reading the INSTANCE_ID for the machine from the database, then making an API call
				// to AWS with a request for the instance_description of that instance.
				// See https://docs.aws.amazon.com/sdk-for-java/latest/developer-guide/examples-ec2-instances.html and
				// https://github.com/aws/aws-sdk-java-v2/ for additional details.

			}

		} catch (UnknownHostException e) {
			log.log(Level.WARNING, "Problem registering client: UnknownHostException: " +e.getMessage(), e);
			return false;
		} catch (Exception e) {
			log.log(Level.WARNING, "Problem registering client: Exception: "+e.getMessage(), e);
			return false;
		}

		return true;
	}

	/**
	 * Updates the GoDaddy DNS domain "icpc-vcss.org" to contain a new/updated DNS entry (A-record) for the
	 * machine with the specified IP address.
	 * 
	 * @param machineName the name of the machine (the first component of the FQDN).
	 * @param contestName the current contest name (the second component of the FQDN).
	 * @param machineIPAddr the IP address of the machine whose DNS entry is to be updated.
	 * 
	 * @return true if the call to the GoDaddy API was successful (returned "OK"); false if not.
	 */
    private boolean updateDNS(String machineName, String contestName, String machineIPAddr) {
    	
		log.info ("Updating DNS server with new machine's IP address");
		
		//define the "entity string" that will comprise the body of the http request
		String json = Json.createObjectBuilder()
	            .add("data", "" + machineIPAddr.toString() + "")
	            .add("port", 1)
	            .add("priority", 0)
	            .add("protocol", "string")
	            .add("service", "string")
	            .add("ttl", 600)
	            .add("weight", 1)
	            .build()
	            .toString();
		
		//the GoDaddy API requires an array, even if there's just one element
		json = "[" + json + "]";
		
		//construct a client with which to make the GoDaddy API call
		CloseableHttpClient httpClient = HttpClientBuilder.create().build();
		
		try {
			//construct the basic GoDaddy API http request
		    HttpPut request = new HttpPut("https://api.godaddy.com/v1/domains/icpc-vcss.org/records/A/" 
		    								+ machineName + "." + contestName);

		    //add headers and body to the request
		    request.addHeader("accept", "application/json");
		    request.addHeader("content-type", "application/json");
		    request.addHeader("Authorization", "sso-key eoMJEu9vgZu2_Jbbk4qLJvBam9NKL8kSTLw:KyGXoKu2RUZHsmaRAEkbEF");
		    StringEntity params = new StringEntity(json.toString());
		    request.setEntity(params);

		    //execute the http request (send it to the GoDaddy API)
		    CloseableHttpResponse response = httpClient.execute(request);

		    //get the http response code
		    StatusLine statusLine = response.getStatusLine();
		    int statusCode = statusLine.getStatusCode();
		    
		    if (statusCode != HttpStatus.SC_OK) {
		    	log.severe("\n*******************\nDNS update failed for machine " + machineName + "; HTTP status code = " + statusCode );
		    	log.severe("DNS update failed; HTTP Reason Phrase= " + statusLine.getReasonPhrase()
		    				+ "\n******************");
			//TODO: when DNS update fails, create a Timer task to "try again later".
//		    	return false;
		    }
		    
		} catch (Exception ex) {
			
			//TODO: when DNS update fails, create a Timer task to "try again later".
		    log.severe("Exception during DNS update: " + ex.toString());
		    //return false;
		} finally {
		    try {
				httpClient.close();
			} catch (IOException e) {
				//TODO: when DNS update fails, create a Timer task to "try again later".
			    log.severe("Exception closing DNS update httpClient: " + e.toString());
				//return false;
			}
		}
		
		return true;
	}

  private Machine getAvailableMachine(UUID uuid) {
    entityManager.getTransaction().begin();
    Machine machine = machineDAO.getAvailableMachine();
    if (machine == null) {
      // rollback/unlock everything, no machine found
      entityManager.getTransaction().rollback();
      return null;
    }

    // Assign it our uuid, then commit it
    machine.setUuid(uuid);
    machine = machineDAO.save(machine);
    entityManager.getTransaction().commit();
    entityManager.refresh(machine);

    return machine;
  }

	/**
     * Sends a {@link WSRegistrationResponseMsg} message containing "success" status ("OK")
     * to the client represented by the specified {@link Session}.
     *
     * @param session the {@link Session} for client to whom the message should be sent.
     */
	private void sendRegistrationSuccessMessageToClient(Session session) {

    	log.info("Sending registration success message to client " + session.getId());

    	ServerWebsocketMessage successMsg = new WSRegistrationResponseMsg(WSRegistrationResponseType.OK);
		sendMessageToClient(successMsg, session);
	}

    /**
     * Sends a {@link WSRegistrationResponseMsg} message containing "failure" status to the client
     * represented by the specified {@link Session}.
     *
     * @param session the {@link Session} for client to whom the message should be sent.
     */
	private void sendRegistrationFailureMessageToClient(Session session) {

    	log.info("\nSending registration failure message to client " + session.getId());

    	WSRegistrationResponseMsg failMsg = new WSRegistrationResponseMsg(WSRegistrationResponseType.ERROR);
		sendMessageToClient(failMsg, session);
	}

    /**
     * This method sends the specified {@link ServerWebsocketMessage} to the client represented by the
     * specified Websocket {@link Session}.
     * @param message the {@link ServerWebsocketMessage} to be sent to the client.
     * @param session the session representing the client.
     */
    private void sendMessageToClient(ServerWebsocketMessage message, Session session) {

		try {

			//TODO: this should be spawned off on a separate thread
			session.getBasicRemote().sendObject(message);

		} catch (IOException | EncodeException e) {
//			log.throwing("WSRegistrationRequestMsg", "sendMessageToClient", e);
			log.log(Level.WARNING, "Problem sending message to client "+e.getMessage(), e);

		}
	}

	/**
	 * Sends a series of messages through the websocket to the client instructing the client to create
	 * a user login for each account specified in the accounts parameter.
	 *
	 * Note that if an account in the specified set of accounts has no password, the method declines
	 * to send a create-account message to the client for that account.
	 *
	 * Note also that this method simply SENDS account-creation messages to the specified client; it has no
	 * way of determining whether account creation was successful (that information comes in the form
	 * of "execution_response" messages) .
	 *
	 * @param session the Websocket session for the client
	 * @param accounts a Map<String,String> mapping login names to be created to the corresponding password to be assigned.
	 */
	private void sendCreateAccountsMessagesToClient(Session session, Map<String, String> accounts) {

		//get the account login names, which are the keys in the received map
		Set<String> logins = accounts.keySet();

		for (String loginName : logins) {
			String password = accounts.get(loginName);
			if (password!=null && !password.equals("")) {
				//TODO: check the boolean return value from this method; probably, accumulate the booleans from ALL calls
				//  in the loop and then return a boolean from the enclosing method...
				sendCreateAccountMessage(session, loginName, password);
			} else {
				log.warning("Got an account with no password, declining to create account '" + loginName + "'");
			}
		}
	}

	/**
	 * Sends a "create account" message to the specified client.
	 *
	 * @param session the websocket session for the client.
	 * @param loginName the account name (login name) for the account to be created.
	 * @param password the initial password to be assigned to the new account.
	 *
	 * @return true if a "Create Account" message was successfully constructed, sent to the client, and stored in the database;
	 * 			false if an error occurred.
	 */
	private boolean sendCreateAccountMessage(Session session, String loginName, String password) {

		String createAccountCommand = Constants.CLOUD_CREATE_USER_COMMAND;

		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String cryptedPasswordHash = encoder.encode(password);

		//substitute user name and password into command
		createAccountCommand = createAccountCommand.replace("{username}",loginName).replace("{password}", cryptedPasswordHash);

		//create an ExecuteCommand for the create-account operation
		ExecuteCommand commandObj = new ExecuteCommand();
		commandObj.setText(createAccountCommand);
		commandObj.setStatus(CommandStatusType.NEW);

		//TODO:  the above produces a Command object which has no id -- i.e., an object in an invalid state.
		// It should not be possible to do this... we need a Factory which produces Commands which
		// always have id's in them, and the Factory is the ONLY way to get a new Command object.

		//insert into DB and get back Command object with commandID assigned
		commandObj =  commandDAO.save(commandObj);

		if (commandObj==null) {
			log.severe("Unable to save 'createAccount' command in database (db returned null from put() operation); can't continue");
			return false;
		}

		//make sure we got a Command with a valid (i.e., positive) commandID
		long commandID = commandObj.getId();

		if (commandID<1) {
			log.severe("Initial 'createAccount' command object has ID<1; cannot continue");
			return false;
		}

		//put the command in a message
		WSCommandMsg executeMsg = new WSCommandMsg(commandObj);

		log.info("Sending account creation message for account '"
							+ loginName + "' to machine " + WebsocketSessionUtilities.getRemoteIPFromSession(session));

		//send the message to the client
		sendMessageToClient(executeMsg, session);

		//create a new result object for this command/machine combination

		User user = WSServerEndpoint.getConnections().get(session.getId());
		if (user==null) {
			log.severe("Unable to obtain user for session " + session.getId());
			return false;
		}

		String machineUUID = user.getMachineUUIDAsString();
		Machine machine = machineDAO.findByUuid(UUID.fromString(machineUUID));

		//TODO:  see the comments in class MachineCommandResult regarding this constructor...
		ExecuteCommandResult resultObj = new ExecuteCommandResult();
		resultObj.setMachine(machine);
		resultObj.setCommand(commandObj);
		resultObj.setResponse("No response yet");
		resultObj.setStatus(MachineCommandResultStatusType.PENDING);

		//put the result object into the database
		machineCommandResultDAO.save(resultObj);

		//command has been sent to target machine; mark it "pending" in the database.
		commandObj.setStatus(CommandStatusType.PENDING);
		commandObj = commandDAO.save(commandObj);

		return true;
	}

	/**
	 * This method uses the specified IP Address to find the AWS InstanceId of the AWS machine currently associated with that IP.
	 *
	 * @param targetIpAddress a String giving an IP Address for which we want the AWS InstanceId
	 *
	 * @return a String containing the AWS InstanceId of the machine with the specified IP address, or null if no such AWS machine could be found.
	 */
	private String getAWSInstanceId(String targetIpAddress) {

		// the value we are going to return
		String instanceId = null;

		try {
			DescribeNetworkInterfacesRequest newRequest = new DescribeNetworkInterfacesRequest();

			Filter filter = new Filter();

			filter.withName("association.public-ip").withValues(targetIpAddress);
			newRequest.withFilters(filter);

			DescribeNetworkInterfacesResult result = ec2.describeNetworkInterfaces(newRequest);

			if (result != null) {
				List<NetworkInterface> ni = result.getNetworkInterfaces();
				if (ni.size() == 1) {
					instanceId = ni.get(0).getAttachment().getInstanceId();
					log.info("Found InstanceId " + instanceId + " for machine " + targetIpAddress);
					return instanceId;
				} else {
					if (ni.size() == 0) {
						log.severe("PROBLEM: Couldnt find InstanceId for machine " + targetIpAddress);
					} else {
						String ids = "";
						for (int i = 0; i < ni.size(); i++) {
							ids = ni.get(i).getAttachment().getInstanceId() + ", ";
						}
						log.severe("PROBLEM: Found multiple InstanceIds " + ids + " for machine " + targetIpAddress);
					}
					return null;
				}
			} else {
				log.severe("PROBLEM: AWS API returned null while trying to find InstanceId for machine " + targetIpAddress);
				return null;
			}

		} catch (AmazonEC2Exception e) {
			log.severe("\n\n##############\nException getting AWS InstanceId: " + e.getErrorMessage() + "\n##############\n");
			//TODO: consider starting a Timer task to attempt to fetch the InstanceId later (because for example 
			//TODO: the current attempt might have been rejected for "too many requests")
			return null;
		}
	}
		
}
