package org.icpc.vcss.websocketMessage;

import javax.websocket.Session;

import org.icpc.vcss.websocketMessage.client.WSCommandResponseMsg;
import org.icpc.vcss.websocketMessage.client.WSRegistrationRequestMsg;
import org.icpc.vcss.websocketMessage.server.WSCommandMsg;
import org.icpc.vcss.websocketMessage.server.WSRegistrationResponseMsg;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

/**
 * This class was created to act as a common super-class for client and server
 * websocket messages, primarily to allow creation of a single JSON encoder and
 * decoder for websocket messages.
 */

@JsonTypeInfo(use = Id.NAME, include = As.PROPERTY, property = "type")
@JsonSubTypes({
  @JsonSubTypes.Type(value = WSRegistrationRequestMsg.class, name = "registration_request"),
  @JsonSubTypes.Type(value = WSRegistrationResponseMsg.class, name = "registration_response"),
  @JsonSubTypes.Type(value = WSCommandMsg.class, name = "command"),
  @JsonSubTypes.Type(value = WSCommandResponseMsg.class, name = "command_response"),
  // TODO: add more message types here
})
abstract public class WebsocketMessage {
  abstract public void handleMessage(Session session);
  public boolean requireTransaction() {
    return true;
  }
}
