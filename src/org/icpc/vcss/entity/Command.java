package org.icpc.vcss.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.icpc.vcss.core.commands.CommandStatusType;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

/**
 * This class encapsulates a "command" which an Admin has specified should be sent to
 * a collection of client machines.
 */
@Entity
@Table(name = "command")
@Inheritance(
		strategy = InheritanceType.SINGLE_TABLE
)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "cmd_type")
@JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "cmd_type")
@JsonSubTypes({
  @JsonSubTypes.Type(value = ExecuteCommand.class, name = "execute"),
  @JsonSubTypes.Type(value = PutCommand.class, name = "put"),
  @JsonSubTypes.Type(value = GetCommand.class, name = "get"),
})
public class Command {
	/**
	 * The id of this command.
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	/**
	 * The status of this command; an element of {@link CommandStatusType}.
	 */
	@Column(name = "status")
	private CommandStatusType status;

	/**
	 * The list of all {@link MachineCommandResult}s associated with this command.
	 */
	@OneToMany(mappedBy = "command", cascade = CascadeType.ALL)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	private List<MachineCommandResult> results = new ArrayList<>();

	/**
	 * The {@link Date} at which the command was received by the server from an Administrator.
	 */
	@Column(name = "created_at")
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;
	/**
	 * The {@link Date} at which the command was updated.
	 */
	@Column(name = "updated_at")
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdateDate;

	public Command() {
	}

	/**
	 * The id of this command.
	 */
	public long getId() {
		return this.id;
	}

	/**
	 * The status of this command; an element of {@link CommandStatusType}.
	 */
	public CommandStatusType getStatus() {
		return this.status;
	}
	
	/**
	 * The list of all {@link MachineCommandResult}s associated with this command.
	 */
	public List<MachineCommandResult> getResults() {
		return results;
	}

	/**
	 * The {@link Date} at which the command was received by the server from an Administrator.
	 */
	public Date getCreationDate() {
		return this.creationDate;
	}

	/**
	 * The {@link Date} at which the command was updated.
	 */
	public Date getLastUpdateDate() {
		return this.lastUpdateDate;
	}

	/**
	 * The id of this command.
	 */
	public void setId(final long id) {
		this.id = id;
	}

	/**
	 * The status of this command; an element of {@link CommandStatusType}.
	 */
	public void setStatus(final CommandStatusType status) {
		this.status = status;
	}

	/**
	 * The {@link Date} at which the command was received by the server from an Administrator.
	 */
	public void setCreationDate(final Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * The {@link Date} at which the command was updated.
	 */
	public void setLastUpdateDate(final Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}
}
