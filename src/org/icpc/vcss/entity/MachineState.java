package org.icpc.vcss.entity;

/**
 * Machine States
 *
 * @author Douglas A. Lane <pc2@ecs.csus.edu>
 *
 */
public enum MachineState {

	/**
	 * Not initialized
	 */

	UNSET,

	/**
	 * Data loaded from file or UI
	 */
	NEW,

	/**
	 * Machine created not started
	 */
	VM_CREATED,

	/**
	 * Machine started
	 */
	STARTED,

	/**
	 * Machine stopped
	 */
	STOPPED,

	/**
	 * Machine terminated or does not exist
	 */
	DELETED,


}
