package org.icpc.vcss.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Machine information
 */
@Entity
@DiscriminatorValue("put_result")
@JsonTypeName("put_result")
public class PutCommandResult extends MachineCommandResult {
	@JsonProperty
	private String returnStatus;

	public String getReturnStatus() {
		return returnStatus;
	}

	public void setReturnStatus(String returnStatus) {
		this.returnStatus = returnStatus;
	}
}
