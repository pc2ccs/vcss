package org.icpc.vcss.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

@Entity
@Table(name = "logins")
public class Login {
    public Login() {
    }

    public Login(String name, String password) {
        this.name = name;
        this.password = password;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", length = 20)
    private String name;

    @Column(name = "password", length = 64)
    private String password;

    @Column(name = "state", length = 4)
    private String state;

    @Column(name = "added")
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date added;


    @Column(name = "updated")
    @UpdateTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    private Date updated;

    @OneToMany(mappedBy = "login", cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Set<MachineLogin> machineLogins = new HashSet<>();

    public long getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public String getPassword() {
        return this.password;
    }

    public String getState() {
        return this.state;
    }

    public Date getAdded() {
        return this.added;
    }

    public Date getUpdated() {
        return this.updated;
    }

    public Set<MachineLogin> getMachineLogins() {
        return this.machineLogins;
    }

    public void setId(final long id) {
        this.id = id;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public void setAdded(final Date added) {
        this.added = added;
    }

    public void setUpdated(final Date updated) {
        this.updated = updated;
    }

    public void setMachineLogins(final Set<MachineLogin> machineLogins) {
        this.machineLogins = machineLogins;
    }
}
