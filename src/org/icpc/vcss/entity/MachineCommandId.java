package org.icpc.vcss.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class MachineCommandId implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "machine_id")
    private Long machineId;
    @Column(name = "command_id")
    private Long commandId;

    public MachineCommandId() {
    }

    public MachineCommandId(Long machineId, Long commandId) {
        super();
        this.machineId = machineId;
        this.commandId = commandId;
    }

    public Long getMachineId() {
        return this.machineId;
    }

    public Long getCommandId() {
        return this.commandId;
    }

    public void setMachineId(final Long machineId) {
        this.machineId = machineId;
    }

    public void setCommandId(final Long commandId) {
        this.commandId = commandId;
    }

    @java.lang.Override
    public boolean equals(final java.lang.Object o) {
        if (o == this) return true;
        if (!(o instanceof MachineCommandId)) return false;
        final MachineCommandId other = (MachineCommandId) o;
        if (!other.canEqual((java.lang.Object) this)) return false;
        final java.lang.Object this$machineId = this.getMachineId();
        final java.lang.Object other$machineId = other.getMachineId();
        if (this$machineId == null ? other$machineId != null : !this$machineId.equals(other$machineId)) return false;
        final java.lang.Object this$commandId = this.getCommandId();
        final java.lang.Object other$commandId = other.getCommandId();
        if (this$commandId == null ? other$commandId != null : !this$commandId.equals(other$commandId)) return false;
        return true;
    }

    protected boolean canEqual(final java.lang.Object other) {
        return other instanceof MachineCommandId;
    }

    @java.lang.Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final java.lang.Object $machineId = this.getMachineId();
        result = result * PRIME + ($machineId == null ? 43 : $machineId.hashCode());
        final java.lang.Object $commandId = this.getCommandId();
        result = result * PRIME + ($commandId == null ? 43 : $commandId.hashCode());
        return result;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "MachineCommandId(machineId=" + this.getMachineId() + ", commandId=" + this.getCommandId() + ")";
    }
}
