package org.icpc.vcss.entity;

import java.util.List;
import java.util.Optional;
import java.util.function.Function;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.LockModeType;

public abstract class Dao<T> {
    EntityManager entityManager;

    public Dao() {
    }

    public Dao(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    abstract Optional<T> get(long id);

    abstract List<T> getAll();

    public void ensureTransaction() {
      EntityTransaction t = entityManager.getTransaction();
      if (! t.isActive()) {
        t.begin();
      }
    }

    public void commitTransaction() {
      EntityTransaction t = entityManager.getTransaction();
      if (t.isActive()) {
        t.commit();
      }
    }

    public T lock(T entity) {
      entityManager.refresh(entity, LockModeType.PESSIMISTIC_WRITE);
      return entity;
    }

    public <Q> Q save(Q entity) {
        return executeInsideTransaction(entityManager -> {
            return entityManager.merge(entity);
        });
    }

    public void delete(T entity) {
        executeInsideTransaction(entityManager -> {
            entityManager.remove(entity);
            return null;
        });
    }

    private <Q> Q executeInsideTransaction(Function<EntityManager, Q> action) {
        Q ret;
        EntityTransaction tx = entityManager.getTransaction();
        Boolean existingTransaction = tx.isActive();
        try {
            if (!existingTransaction) tx.begin();
            ret = action.apply(entityManager);
            if (!existingTransaction) tx.commit();
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        }
        return ret;
    }

    public EntityManager getEntityManager() {
        return this.entityManager;
    }
}
