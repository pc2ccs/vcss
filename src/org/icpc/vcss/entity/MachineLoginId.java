package org.icpc.vcss.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class MachineLoginId implements Serializable {
    private static final long serialVersionUID = 1L;
    @Column(name = "machine_id")
    private Long machineId;
    @Column(name = "login_id")
    private Long loginId;

    public MachineLoginId() {
    }

    public MachineLoginId(Long machineId, Long loginId) {
        super();
        this.machineId = machineId;
        this.loginId = loginId;
    }

    public Long getMachineId() {
        return this.machineId;
    }

    public Long getLoginId() {
        return this.loginId;
    }

    public void setMachineId(final Long machineId) {
        this.machineId = machineId;
    }

    public void setLoginId(final Long loginId) {
        this.loginId = loginId;
    }

    @java.lang.Override
    public boolean equals(final java.lang.Object o) {
        if (o == this) return true;
        if (!(o instanceof MachineLoginId)) return false;
        final MachineLoginId other = (MachineLoginId) o;
        if (!other.canEqual((java.lang.Object) this)) return false;
        final java.lang.Object this$machineId = this.getMachineId();
        final java.lang.Object other$machineId = other.getMachineId();
        if (this$machineId == null ? other$machineId != null : !this$machineId.equals(other$machineId)) return false;
        final java.lang.Object this$loginId = this.getLoginId();
        final java.lang.Object other$loginId = other.getLoginId();
        if (this$loginId == null ? other$loginId != null : !this$loginId.equals(other$loginId)) return false;
        return true;
    }

    protected boolean canEqual(final java.lang.Object other) {
        return other instanceof MachineLoginId;
    }

    @java.lang.Override
    public int hashCode() {
        final int PRIME = 59;
        int result = 1;
        final java.lang.Object $machineId = this.getMachineId();
        result = result * PRIME + ($machineId == null ? 43 : $machineId.hashCode());
        final java.lang.Object $loginId = this.getLoginId();
        result = result * PRIME + ($loginId == null ? 43 : $loginId.hashCode());
        return result;
    }

    @java.lang.Override
    public java.lang.String toString() {
        return "MachineLoginId(machineId=" + this.getMachineId() + ", loginId=" + this.getLoginId() + ")";
    }
}
