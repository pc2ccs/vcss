package org.icpc.vcss.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@Entity
@DiscriminatorValue("put")
@JsonTypeName("put")
public class PutCommand extends Command {
	@JsonProperty
	String sourceFilePath = "";

	@JsonProperty
	String destFilePath = "";

	@JsonProperty
	String filemode = "";

	@JsonProperty
	String owner = "";

	@JsonProperty
	String base64SourceFileContents;

	public String getSourceFilePath() {
		return sourceFilePath;
	}

	public String getDestFilePath() {
		return destFilePath;
	}

	public String getFilemode() {
		return filemode;
	}

	public String getOwner() {
		return owner;
	}

	public String getBase64SourceFileContents() {
		return base64SourceFileContents;
	}

	public void setBase64SourceFileContents(String base64SourceFileContents) {
		this.base64SourceFileContents = base64SourceFileContents;
	}
}
