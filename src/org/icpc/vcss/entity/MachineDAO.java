package org.icpc.vcss.entity;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.persistence.EntityManager;
import javax.persistence.LockModeType;
import javax.persistence.TypedQuery;

public class MachineDAO extends Dao<Machine> {

    public MachineDAO(EntityManager em) {
        super(em);
    }

    public Machine findByHostname(String hostname) {
    	  TypedQuery<Machine> query = entityManager.createQuery("select m from Machine m where m.hostname=:hostname", Machine.class);
        query.setParameter("hostname", hostname);
        return query.getResultStream().findFirst().orElse(null);
    }

    public List<Machine> findAllByHostname(String hostname) {
        TypedQuery<Machine> query = entityManager.createQuery( "SELECT m FROM Machine m WHERE m.hostname=:hostname", Machine.class);
        query.setParameter("hostname", hostname);
        return query.getResultList();
    }

    public Machine findByUuid(UUID uuid) {
        TypedQuery<Machine> query;
        if (uuid == null) {
          query = entityManager.createQuery("select m from Machine m where m.uuid is null", Machine.class);
        } else {
          query = entityManager.createQuery("select m from Machine m where m.uuid=:uuid", Machine.class);
          query.setParameter("uuid", uuid);
        }
        return query.getResultStream().findFirst().orElse(null);
    }

    /**
     * Returns a @Machine from the database that has no Uuid assigned, and locks it for update.
     * Execute this inside a transaction.
     *
     * @return @Machine a machine with no uuid set
     */
    public Machine getAvailableMachine() {
      TypedQuery<Machine> query = entityManager.createQuery("SELECT m FROM Machine m WHERE m.uuid is null", Machine.class);
      query.setMaxResults(1);
      query.setHint("javax.persistence.lock.timeout", 120000);
      query.setLockMode(LockModeType.PESSIMISTIC_WRITE);
      return query.getResultStream().findFirst().orElse(null);
    }

    public List<Machine> getConnectedMachines() {
        TypedQuery<Machine> query = entityManager.createQuery("SELECT m FROM Machine m WHERE m.connected=:connected", Machine.class);
        query.setParameter("connected", true);
        return query.getResultList();
    }

    public List<Machine> getAllMachinesByUUID(List<UUID> uuids) {
      TypedQuery<Machine> query = entityManager.createQuery("SELECT m FROM Machine m WHERE m.uuid IN :uuids", Machine.class);
      query.setParameter("uuids", uuids);
      return query.getResultList();
    }

    @Override
    public Optional<Machine> get(long id) {
        return Optional.ofNullable(entityManager.find(Machine.class, id));
    }

    @Override
    public List<Machine> getAll() {
        TypedQuery<Machine> query = entityManager.createQuery("SELECT m FROM Machine m", Machine.class);
        return query.getResultList();
    }

    public List<Machine> getCloudMachines() {
        TypedQuery<Machine> query = entityManager.createQuery("SELECT m FROM Machine m where not name like 'localVM%'", Machine.class);
        return query.getResultList();
    }

	public List<Machine> FindAllByName(String name) {
		 TypedQuery<Machine> query = entityManager.createQuery("SELECT m FROM Machine m where m.name=:name", Machine.class);
	        query.setParameter("name", name);
	        return query.getResultList();
	}

	public Machine findByInstanceId(String awsInstanceId) {
		 TypedQuery<Machine> query = entityManager.createQuery("SELECT m FROM Machine m where m.ec2Id=:awsInstanceId", Machine.class);
	        query.setParameter("awsInstanceId", awsInstanceId);
	        return query.getResultStream().findFirst().orElse(null);
	}
}
