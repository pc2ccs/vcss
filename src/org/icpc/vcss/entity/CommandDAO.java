package org.icpc.vcss.entity;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class CommandDAO extends Dao<Command> {

    public CommandDAO(EntityManager em) {
        super(em);
    }
    
    @Override
    public Optional<Command> get(long id) {
      return Optional.ofNullable(entityManager.find(Command.class, id));
    }

    @Override
    public List<Command> getAll() {
        TypedQuery<Command> query = entityManager.createQuery("SELECT c FROM Command c", Command.class);
        return query.getResultList();
    }
}
