package org.icpc.vcss.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Table(name = "machlogin")
public class MachineLogin implements Comparable<MachineLogin> {
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private long id;
    @EmbeddedId
    private MachineLoginId pk_id = new MachineLoginId();

    @ManyToOne(cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @MapsId("machineId")
    public Machine machine;

    @ManyToOne(cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @MapsId("loginId")
    public Login login;

    @Column(name = "state", length = 4)
    private String state;

    @Column(name = "added")
    @Temporal(TemporalType.TIMESTAMP)
    private Date added;

    public MachineLoginId getPk_id() {
        return this.pk_id;
    }

    public Machine getMachine() {
        return this.machine;
    }

    public Login getLogin() {
        return this.login;
    }

    public String getState() {
        return this.state;
    }

    public Date getAdded() {
        return this.added;
    }

    public void setPk_id(final MachineLoginId pk_id) {
        this.pk_id = pk_id;
    }

    public void setMachine(final Machine machine) {
        this.machine = machine;
    }

    public void setLogin(final Login login) {
        this.login = login;
    }

    public void setState(final String state) {
        this.state = state;
    }

    public void setAdded(final Date added) {
        this.added = added;
    }

    @Override
    public int compareTo(MachineLogin o) {
      if (getLogin() == null || o.getLogin() == null) {
        return getAdded().compareTo(o.getAdded());
      }
      String data1 = getLogin().getName();
      String data2 = o.getLogin().getName();
      return data1.compareTo(data2);
    }
}
