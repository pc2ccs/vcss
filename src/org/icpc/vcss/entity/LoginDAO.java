package org.icpc.vcss.entity;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

public class LoginDAO extends Dao<Command> {

    public LoginDAO(EntityManager em) {
        super(em);
    }

    public Login findByPassword(String password) {
      TypedQuery<Login> query = entityManager.createQuery("select l from Login l where l.password=:password", Login.class);
      query.setParameter("password", password);
      return query.getResultStream().findFirst().orElse(null);
  }

    @Override
    public Optional<Command> get(long id) {
      return Optional.ofNullable(entityManager.find(Command.class, id));
    }

    @Override
    public List<Command> getAll() {
        TypedQuery<Command> query = entityManager.createQuery("SELECT c FROM Command c", Command.class);
        return query.getResultList();
    }
}
