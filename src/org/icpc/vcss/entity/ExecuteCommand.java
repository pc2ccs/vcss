package org.icpc.vcss.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@Entity
@DiscriminatorValue("execute")
@JsonTypeName("execute")
public class ExecuteCommand extends Command {
	/**
	 * The text of the command, including any command-line arguments.
	 * 
	 * Developer's note: the text of the command probably shouldn't be represented
	 * with a single field. At a minimum it seems like there should be a "command"
	 * (verb) field and a "command arguments" field, which also implies separate
	 * accessors for the fields.
	 */
	@JsonProperty
	String text;

	public ExecuteCommand() {
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
