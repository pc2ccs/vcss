package org.icpc.vcss.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

@Entity
@DiscriminatorValue("get")
@JsonTypeName("get")
public class GetCommand extends Command {
	/**
	 * The text of the command, including any command-line arguments.
	 * 
	 * Developer's note:  this command probably shouldn't have a "text" field; it would make more sense
	 * to have a file "filename", the name of the file to be "got" from the client.
	 */
	@JsonProperty
	String text;

	public GetCommand() {
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}
}
