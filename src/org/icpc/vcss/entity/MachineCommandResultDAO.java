package org.icpc.vcss.entity;

import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.icpc.vcss.core.commands.MachineCommandResultStatusType;

public class MachineCommandResultDAO extends Dao<MachineCommandResult> {

    public MachineCommandResultDAO(EntityManager em) {
        super(em);
    }

    public List<MachineCommandResult> findByCommand(Command command) {
      TypedQuery<MachineCommandResult> query = entityManager.createQuery("SELECT mcr FROM MachineCommandResult mcr WHERE command=:command", MachineCommandResult.class);
      query.setParameter("command", command);
      return query.getResultList();
    }

    public long countByStatus(Command command, MachineCommandResultStatusType status) {
        Query query = entityManager.createQuery( "SELECT count(mcr) FROM MachineCommandResult mcr WHERE command=:command AND status=:status");
        query.setParameter("command", command);
        query.setParameter("status", status);
        return (long)query.getSingleResult();
    }

    public Optional<MachineCommandResult> get(MachineCommandId id) {
        return Optional.ofNullable(entityManager.find(MachineCommandResult.class, id));
    }

    @Override
    public Optional<MachineCommandResult> get(long id) {
      throw new RuntimeException("Unsupported");
    }

    @Override
    public List<MachineCommandResult> getAll() {
        TypedQuery<MachineCommandResult> query = entityManager.createQuery("SELECT mcr FROM MachineCommandResult mcr", MachineCommandResult.class);
        return query.getResultList();
    }
}
