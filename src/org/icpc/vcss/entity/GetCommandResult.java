package org.icpc.vcss.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeName;

/**
 * Machine information
 */
@Entity
@DiscriminatorValue("get_result")
@JsonTypeName("get_result")
public class GetCommandResult extends MachineCommandResult {
	
	@JsonProperty
	private String returnStatus;
	
	@JsonProperty
	private String base64File ;
	
	/**
	 * Returns the base64-encoded file which was requested by the command indicated
	 * by the value in {@link GetCommandResult#getCommandId()}.
	 * Note: callers should first check the value returned by {@link GetCommandResult#getStatus()}
	 * to insure that the command was successful in fetching the requested file.
	 * 
	 * @return a base64-encoded file if {@link GetCommandResult#getStatus()} equals "OK"; null otherwise
	 */
	public String getBase64File() {
		return base64File;
	}
	
	public void setBase64File(String base64File) {
		this.base64File = base64File;
	}
	
	public String getReturnStatus() {
		return returnStatus;
	}

	public void setReturnStatus(String returnStatus) {
		this.returnStatus = returnStatus;
	}
}
