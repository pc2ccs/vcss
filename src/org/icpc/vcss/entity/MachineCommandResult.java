package org.icpc.vcss.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;
import org.icpc.vcss.core.commands.MachineCommandResultStatusType;

/**
 * Machine information
 */
@Entity
@Table(name = "machine_command_result")
@Inheritance(
		strategy = InheritanceType.SINGLE_TABLE
)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "result_type")
@JsonTypeInfo(use = com.fasterxml.jackson.annotation.JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "result_type")
@JsonSubTypes({
  @JsonSubTypes.Type(value = ExecuteCommandResult.class, name = "execute_result"),
  @JsonSubTypes.Type(value = PutCommandResult.class, name = "put_result"),
  @JsonSubTypes.Type(value = GetCommandResult.class, name = "get_result"),
})
public class MachineCommandResult implements Comparable<MachineCommandResult> {
	@EmbeddedId
	@JsonIgnore
	private MachineCommandId pk_id = new MachineCommandId();

	@ManyToOne(cascade = CascadeType.ALL)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	@MapsId("machineId")
	public Machine machine;

	@ManyToOne(cascade = CascadeType.ALL)
	@OnDelete(action = OnDeleteAction.CASCADE)
	@JsonIgnore
	@MapsId("commandId")
	public Command command;

  @Lob
	@Column(name = "response", columnDefinition = "longtext")
	private String response;

	/**
	 * The current status of this MachineCommandResult.
	 */
	@Column(name = "status")
	private MachineCommandResultStatusType status;

	/**
	 * The date/time this MachineCommandResult was first created.
	 * Note that this will be (closely) related to the date when the corresponding
	 * command was received by the server (because receipt of a command by the server
	 * is what initially creates a MachineCommandResult), but this Date is more-or-less
	 * unrelated to when the corresponding command was actually sent to the target machine,
	 * and is UNrelated to the time when the response to the command (if any yet) is
	 * received back from the target machine (except that this creationDate must be
	 * BEFORE any value contained in the lastUpdateDate field).
	 */
	@Column(name = "created_at")
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	/**
	 * The date/time when the most recent response to the associated command was
	 * received from the target machine by the server.
	 */
	@UpdateTimestamp
	@Column(name = "updated_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdateDate;

	public MachineCommandResult() {
	}

	public MachineCommandResult(String uuid, String result) {
		throw new RuntimeException("Don\'t use this!");
	}

	/**
	 * Returns a string version of the composite ID(e.g. c123-m123)
	 */
	@JsonIgnore
	public String getIdString() {
		return String.format("c%d-m%d", getPk_id().getCommandId(), getPk_id().getMachineId());
	}

	public MachineCommandId getPk_id() {
		return this.pk_id;
	}

	public Machine getMachine() {
		return this.machine;
	}

	public Command getCommand() {
		return this.command;
	}

	public String getResponse() {
		return this.response;
	}

	/**
	 * The current status of this MachineCommandResult.
	 */
	public MachineCommandResultStatusType getStatus() {
		return this.status;
	}

	/**
	 * The date/time this MachineCommandResult was first created.
	 * Note that this will be (closely) related to the date when the corresponding
	 * command was received by the server (because receipt of a command by the server
	 * is what initially creates a MachineCommandResult), but this Date is more-or-less
	 * unrelated to when the corresponding command was actually sent to the target machine,
	 * and is UNrelated to the time when the response to the command (if any yet) is
	 * received back from the target machine (except that this creationDate must be
	 * BEFORE any value contained in the lastUpdateDate field).
	 */
	public Date getCreationDate() {
		return this.creationDate;
	}

	/**
	 * The date/time when the most recent response to the associated command was
	 * received from the target machine by the server.
	 */
	public Date getLastUpdateDate() {
		return this.lastUpdateDate;
	}

	@JsonIgnore
	public void setPk_id(final MachineCommandId pk_id) {
		this.pk_id = pk_id;
	}

	@JsonIgnore
	public void setMachine(final Machine machine) {
		this.machine = machine;
	}

	@JsonIgnore
	public void setCommand(final Command command) {
		this.command = command;
	}

	public void setResponse(final String response) {
		this.response = response;
	}

	/**
	 * The current status of this MachineCommandResult.
	 */
	public void setStatus(final MachineCommandResultStatusType status) {
		this.status = status;
	}

	/**
	 * The date/time this MachineCommandResult was first created.
	 * Note that this will be (closely) related to the date when the corresponding
	 * command was received by the server (because receipt of a command by the server
	 * is what initially creates a MachineCommandResult), but this Date is more-or-less
	 * unrelated to when the corresponding command was actually sent to the target machine,
	 * and is UNrelated to the time when the response to the command (if any yet) is
	 * received back from the target machine (except that this creationDate must be
	 * BEFORE any value contained in the lastUpdateDate field).
	 */
	public void setCreationDate(final Date creationDate) {
		this.creationDate = creationDate;
	}

	/**
	 * The date/time when the most recent response to the associated command was
	 * received from the target machine by the server.
	 */
	public void setLastUpdateDate(final Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

  @Override
  public int compareTo(MachineCommandResult o) {
    if (getMachine() == null || o.getMachine() == null) {
      return getCreationDate().compareTo(o.getCreationDate());
    }
    String data1 = getMachine().getName();
    String data2 = o.getMachine().getName();
    return data1.compareTo(data2);
  }
}
