package org.icpc.vcss.entity;

import java.util.Date;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * Machine information
 */
@Entity
@Table(name = "machines")
public class Machine {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@Column(name = "name", length = 60)
	private String name;

	@Column(name = "hostname", length = 80)
	private String hostname;

	@Column(name = "ip_addr", length = 80)
	private String ipAddr;

	@Column(name = "contest_region", length = 80)
	private String contestRegion = "";

	@Column(name = "uuid", length = 16, unique = true)
	private UUID uuid;

	@Column(name = "state", length = 4)
	private MachineState state;

	@Column(name = "connected")
	private Boolean connected = false;

	@Column(name = "connectedTime")
	private Date connectedTime = null;

	@Column(name = "registered")
	private Boolean registered = false;

	@Column(name = "registeredTime")
	private Date registeredTime = null;

	@Column(name = "ec2id")
	private String ec2Id = "";

	@Column(name = "autoShutoff")
	private Boolean autoShutoff = false;

	@Column(name = "selfStartCount")
	private int selfStartCount = 0;

	@Column(name = "selfStart_StartTime")
	private Date selfStart_StartTime = null;

	@Column(name = "selfStart_StopTime")
	private Date selfStart_StopTime = null;

	@Column(name = "runtime")
	private Integer runtime = 30;  //self-start run time, in minutes

	@Column(name = "running")
	private Boolean running = false;

	/**
	 * Note that this is only intended to be used for initial creation of cloud
	 * machine accounts; there is currently no tracking of any user changes to
	 * passwords or independent user additions/deletions of accounts.
	 */
	@OneToMany(mappedBy = "machine", cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
	private Set<MachineLogin> logins = new TreeSet<>();

	@OneToMany(mappedBy = "machine", cascade = CascadeType.ALL)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private Set<MachineCommandResult> results = new TreeSet<>();

	@Column(name = "added")
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date added;

	@Column(name = "updated")
	@UpdateTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	private Date updated;

	public Machine() {
		super();
	}

	public Machine(String name) {
		this.name = name;
		state = MachineState.NEW;
  }

  public boolean checkPassword(String password) {
    // TODO: we can probably issue a single query that asks the database to find whether a login matches or not
    // TOOD: but this should work fine for now
    for (MachineLogin ml: getLogins()) {
      Login l = ml.getLogin();
      if (l.getPassword().equals(password)) {
        return true;
      }
    }
    return false;
  }

	// extra getters/setters
	public void setUuid(String uuid) {
		this.uuid = UUID.fromString(uuid);
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	public boolean isConnected() {
		return this.getConnected();
	}

	public boolean isRegistered() {
		return this.getRegistered();
	}

	// Getters/Setters for specific properties
	// DO NOT TOUCH OR BAD THINGS HAPPEN:
	// https://struberg.wordpress.com/2016/10/15/tostring-equals-and-hashcode-in-jpa-entities/
	@Override
	public String toString() {
		return this.getClass().getSimpleName() + "-" + getId();
	}

	public void addLogin(Login login) {
		MachineLogin ml = new MachineLogin();
		ml.setMachine(this);
		ml.setLogin(login);
		getLogins().add(ml);
	}

	public long getId() {
		return this.id;
	}

	public String getName() {
		return this.name;
	}

	public String getHostname() {
		return this.hostname;
	}

	public String getIpAddr() {
		return this.ipAddr;
	}

	public String getContestRegion() {
		return this.contestRegion;
	}

	public String getEc2Id() {
		return this.ec2Id;
	}

	public UUID getUuid() {
		return this.uuid;
	}

	public MachineState getState() {
		return this.state;
	}

	public Boolean getConnected() {
		return this.connected;
	}

	public Date getConnectedTime() {
		return connectedTime;
	}

	public Boolean getRegistered() {
		return this.registered;
	}

	public Date getRegisteredTime() {
		return registeredTime;
	}

	/**
	 * Note that this is only intended to be used for initial creation of cloud
	 * machine accounts; there is currently no tracking of any user changes to
	 * passwords or independent user additions/deletions of accounts.
	 */
	public Set<MachineLogin> getLogins() {
		return this.logins;
	}

	public Date getAdded() {
		return this.added;
	}

	public Date getUpdated() {
		return this.updated;
	}

	public void setId(final long id) {
		this.id = id;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public void setHostname(final String hostname) {
		this.hostname = hostname;
	}

	public void setIpAddr(final String ipAddr) {
		this.ipAddr = ipAddr;
	}

	public void setContestRegion(final String contestRegion) {
		this.contestRegion = contestRegion;
	}

	public void setEc2Id(final String ec2id) {
		this.ec2Id = ec2id;
	}

	public void setState(final MachineState state) {
		this.state = state;
	}

	public void setConnected(final Boolean connected) {
		this.connected = connected;
	}

	public void setConnectedTime(Date connectedTime) {
		this.connectedTime = connectedTime;
	}

	public void setRegistered(final Boolean registered) {
		this.registered = registered;
	}

	public void setRegisteredTime(Date registeredTime) {
		this.registeredTime = registeredTime;
	}

	/**
	 * Note that this is only intended to be used for initial creation of cloud
	 * machine accounts; there is currently no tracking of any user changes to
	 * passwords or independent user additions/deletions of accounts.
	 */
	public void setLogins(final Set<MachineLogin> logins) {
		this.logins = logins;
	}

	public void setAdded(final Date added) {
		this.added = added;
	}

	public void setUpdated(final Date updated) {
		this.updated = updated;
	}

	public boolean isAutoShutoff() {
		return autoShutoff;
	}

	public void setAutoShutoff(boolean autoShutoff) {
		this.autoShutoff = autoShutoff;
	}

	public Date getSelfStartStartTime() {
		return selfStart_StartTime;
	}

	public void setSelfStartStartTime(Date startTime) {
		this.selfStart_StartTime = startTime;
	}

	public Date getSelfStartStopTime() {
		return selfStart_StopTime;
	}

	public void setSelfStartStopTime(Date stopTime) {
		this.selfStart_StopTime = stopTime;
	}

	/**
	 * Returns the allowed runtime of this machine, in minutes.
	 * 
	 * @return the minutes this machine is allowed to run.
	 */
	public int getRuntime() {
		return runtime;
	}

	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}
	
	
	/**
	 * Returns the boolean flag indicating whether this machine is currently running.
	 * @return true if the machine is running; false if not.
	 */
	public boolean isRunning() {
		return running;
	}
	
	/**
	 * Sets the boolean flag indicating whether or not this machine is currently running.
	 * @param isRunning the value to which the "is running" flag should be set.
	 */
	public void setIsRunning(boolean isRunning) {
		this.running = isRunning;
	}

	/**
	 * Increments the count of the number of times this Machine has been "selfStarted".
	 */
	public void incrementSelfStartCounter() {
		selfStartCount++;
	}
	
	/**
	 * Returns the number of times this Machine has been self-started.
	 * @return the self-start count.
	 */
	public int getSelfStartCounter() {
		return selfStartCount;
	}

}
