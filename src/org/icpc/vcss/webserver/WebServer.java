// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.webserver;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Date;
import java.util.EnumSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;

import javax.persistence.EntityManagerFactory;
import javax.servlet.DispatcherType;

import org.eclipse.jetty.http.HttpVersion;
import org.eclipse.jetty.rewrite.handler.RedirectRegexRule;
import org.eclipse.jetty.rewrite.handler.RewriteHandler;
import org.eclipse.jetty.security.ConstraintMapping;
import org.eclipse.jetty.security.ConstraintSecurityHandler;
import org.eclipse.jetty.security.HashLoginService;
import org.eclipse.jetty.security.SecurityHandler;
import org.eclipse.jetty.security.authentication.BasicAuthenticator;
import org.eclipse.jetty.server.Connector;
import org.eclipse.jetty.server.HttpConfiguration;
import org.eclipse.jetty.server.HttpConnectionFactory;
import org.eclipse.jetty.server.SecureRequestCustomizer;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.SslConnectionFactory;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.eclipse.jetty.util.resource.Resource;
import org.eclipse.jetty.util.security.Constraint;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.WebAppClassLoader;
import org.eclipse.jetty.webapp.WebAppContext;
import org.glassfish.jersey.servlet.ServletContainer;
import org.icpc.vcss.core.Constants;
import org.icpc.vcss.core.Utilities;
import org.icpc.vcss.core.log.Log;
import org.icpc.vcss.core.log.LogFormatter;
import org.icpc.vcss.database.DatabaseUtilities;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineDAO;

import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.InstanceState;
import com.amazonaws.services.ec2.model.InstanceStateChange;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesResult;

/**
 * This class runs the VCSS Server.
 *
 * The server listens on the configured VCSS input port and when a websocket connection is made it
 * creates a websocket for processing messages from the connecting client.
 * The server also responds to HTTPS REST API endpoint requests, including /admin/commandUI, /admin/commands,
 * /admin/commands/{commandID}, etc.
 *
 * @author John Clevenger, ICPC Development Team (pc2@ecs.csus.edu)
 */
public class WebServer {

  private static final String WEBROOT_INDEX = "/webroot/";

	public static final int DEFAULT_WEB_SERVER_PORT_NUMBER = 50443;

	public static final String VCSS_KEYSTORE_FILE = "cacerts.vcss";

	public static final String WEB_SERVER_PROPERTIES_FILENAME = "vcss.properties";

	public static final String PORT_NUMBER_KEY = "port";

	private static Properties wsProperties = new Properties();

	private Server jettyServer = null;

	private String keystorePassword = "i don't care";

	private static Log log = null;

	private ConsoleHandler consoleHandler = new ConsoleHandler();

	public WebServer() {
		super();
		log = new Log("vcss.log");
		consoleHandler.setLevel(Level.ALL);
		consoleHandler.setFormatter(new LogFormatter(true)); // set console log line format, thread, date, etc.
		log.addHandler(consoleHandler);  // by default shows log entries to console during startup
	}

	/**
	 * Remove log console handler
	 */
	private void removeConsoleHandler() {
		if (consoleHandler != null) {
			log.removeHandler(consoleHandler);
		}
		consoleHandler = null;
	}

	private void createKeyStoreAndKey(File keystoreFile) throws KeyStoreException, IOException,
			NoSuchAlgorithmException, CertificateException, FileNotFoundException {
		try {
			KeyStore ks = KeyStore.getInstance(KeyStore.getDefaultType());
			char[] password = keystorePassword.toCharArray();
			ks.load(null, password);

			// taken from
			// https://svn.forgerock.org/opendj/trunk/opends/src/server/org/opends/server/util/Platform.java
			String certAndKeyGen;
			// and this is why you are not suppose to use sun classes
			if (System.getProperty("java.version").matches("^1\\.[67]\\..*")) {
				certAndKeyGen = "sun.security.x509" + ".CertAndKeyGen";
			} else {
				// Java 8 moved the CertAndKeyGen class to sun.security.tools.keytool
				certAndKeyGen = "sun.security.tools.keytool" + ".CertAndKeyGen";
			}
			String x500Name = "sun.security.x509" + ".X500Name";
			Class<?> certKeyGenClass = Class.forName(certAndKeyGen);
			Class<?> x500NameClass = Class.forName(x500Name);
			Constructor<?> certKeyGenCons = certKeyGenClass.getConstructor(String.class, String.class);
			Constructor<?> x500NameCons = x500NameClass.getConstructor(String.class);
			Object keypair = certKeyGenCons.newInstance("RSA", "SHA256WithRSA");
			Object subject = x500NameCons.newInstance(Constants.SUBJECT_DN);
			Method certAndKeyGenGenerate = certKeyGenClass.getMethod("generate", int.class);
			certAndKeyGenGenerate.invoke(keypair, 2048);
			Method certAndKeyGenPrivateKey = certKeyGenClass.getMethod("getPrivateKey");
			PrivateKey rootPrivateKey = (PrivateKey) certAndKeyGenPrivateKey.invoke(keypair);
			Method getSelfCertificate = certKeyGenClass.getMethod("getSelfCertificate", x500NameClass, long.class);

			X509Certificate[] chain = new X509Certificate[1];
			// create with a length of 1 (non-leap) year
			long days = (long) 365 * 24 * 3600;
			// Generate self signed certificate
			chain[0] = (X509Certificate) getSelfCertificate.invoke(keypair, subject, days);
			X509Certificate outCert = chain[0];
			ks.setCertificateEntry("jetty", outCert);
			ks.setKeyEntry("jetty", rootPrivateKey, keystorePassword.toCharArray(), chain);

			// Store away the keystore
			FileOutputStream fos = new FileOutputStream(keystoreFile);
			ks.store(fos, password);
			fos.close();
		} catch (Exception ex) {
			log.log(Level.WARNING, "Failed to create/store key", ex);
		}
	}

	/**
	 * Start Web Server.
	 * <P>
	 * Starts a Jetty webserver running on the port specified in the
	 * WEB_SERVER_PROPERTIES_FILENAME properties file (or on the
	 * DEFAULT_WEB_SERVER_PORT_NUMBER if no port is found in the properties file),
	 * and registers a set of default REST (Jersey/JAX-RS) services with Jetty, in
	 * addition to registering a WebSocket service.
	 */
	public void startWebServer() {

		System.out.println ("Starting webserver...");
		log.info("Starting VCSS webserver");

		if (Utilities.fileExists(Constants.VCSS_PROPERTIES_FILENAME)) {

			showMessage("Found web server properties file " + Constants.VCSS_PROPERTIES_FILENAME + "; loading...");
			wsProperties = loadPropertiesFile(Constants.VCSS_PROPERTIES_FILENAME);

			String propertiesWithoutPasswords = filterPasswords(wsProperties);
			showMessage("Loaded " + propertiesWithoutPasswords);

		}

		try {
			boolean logToConsole = getBooleanProperty(Constants.LOGGER_CONSOLE_KEY, false);
			if (! logToConsole) {
				// by default console logging is turned ON to show startup log messages
				// at this point the console logging property says turn logging off.

				log.info("Console logging turned off");
				removeConsoleHandler();
			}

		} catch (Exception e) {
			log.log(Level.WARNING, "Problem turning off console logging", e);
		}

		try {
			//get the proper port number
			int port = getIntegerProperty(PORT_NUMBER_KEY, DEFAULT_WEB_SERVER_PORT_NUMBER);

			//set up security
			File keystoreFile = new File(VCSS_KEYSTORE_FILE);
			if (!keystoreFile.exists()) {
				createKeyStoreAndKey(keystoreFile);
			}

      			//create a basic http config as a starting point
			HttpConfiguration httpConfig = new HttpConfiguration();
			httpConfig.setSecureScheme("https");
			httpConfig.setSecurePort(port);
			httpConfig.setOutputBufferSize(32768);

			// set to trustAll
			SslContextFactory.Server sslContextFactoryServer = new SslContextFactory.Server();
			sslContextFactoryServer.setTrustAll(true);
			sslContextFactoryServer.setKeyStorePath(keystoreFile.getAbsolutePath());
			sslContextFactoryServer.setKeyStorePassword(keystorePassword);
			sslContextFactoryServer.setKeyManagerPassword(keystorePassword);
			// suggestions from http://www.eclipse.org/jetty/documentation/current/configuring-ssl.html:
			sslContextFactoryServer.setIncludeCipherSuites("TLS_DHE_RSA.*", "TLS_ECDHE.*");
			sslContextFactoryServer.setExcludeProtocols("SSL", "SSLv2", "SSLv2Hello", "SSLv3");
			sslContextFactoryServer.setRenegotiationAllowed(false);

			//create an HTTPS config from the basic http config
			HttpConfiguration httpsConfig = new HttpConfiguration(httpConfig);
			httpsConfig.addCustomizer(new SecureRequestCustomizer());


			//create the Jetty webserver
			jettyServer = new Server();

			//create a ServerConnector -- the object which will replace the default Jetty handler for HTTPS requests
			ServerConnector httpsConnector = new ServerConnector(jettyServer,
					new SslConnectionFactory(sslContextFactoryServer, HttpVersion.HTTP_1_1.asString()),
					new HttpConnectionFactory(httpsConfig));
			httpsConnector.setPort(port);
			// do not timeout
			httpsConnector.setIdleTimeout(0);

			Configuration.ClassList classlist = Configuration.ClassList.setServerDefault( jettyServer );
			// Enable annotation processsing
			classlist.addBefore( "org.eclipse.jetty.webapp.JettyWebXmlConfiguration", "org.eclipse.jetty.annotations.AnnotationConfiguration" );

			// Enable parsing of jndi-related parts of web.xml and jetty-env.xml
			classlist.addAfter("org.eclipse.jetty.webapp.FragmentConfiguration", "org.eclipse.jetty.plus.webapp.EnvConfiguration", "org.eclipse.jetty.plus.webapp.PlusConfiguration");

			// Base URI for servlet context
			URI baseUri = getWebRootResourceUri();

			WebAppContext context = new WebAppContext();

			// This is required for the jstl standard taglib to work
			context.setAttribute("org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern",
					".*/.*jsp-api-[^/]*\\.jar$|.*/.*jsp-[^/]*\\.jar$|.*/.*taglibs[^/]*\\.jar$");

			// Set a non-default class loader(required)
			context.setClassLoader(new WebAppClassLoader(getClass().getClassLoader(), context));
			// Set the base location to our webroot(where WEB-INF and our static resources live)
			context.setResourceBase(baseUri.toASCIIString());
			// Disable directory index to prevent browsing files
			context.setInitParameter("org.eclipse.jetty.servlet.Default.dirAllowed", "false");
			context.setParentLoaderPriority(true);
			context.setContextPath("/");

			context.setSecurityHandler(basicAuth());

			// Wrap the context in a rewrite handler so we can do some nice things(like redirect / to our admin pages)
			RewriteHandler rewrite = new RewriteHandler();
			rewrite.setHandler(context);

			// Redirect root to /public/
			RedirectRegexRule rootRule = new RedirectRegexRule();
			rootRule.setRegex("^/$");
			rootRule.setLocation("/public/");
			rootRule.setTerminating(true);
			rewrite.addRule(rootRule);

			jettyServer.setHandler(rewrite);

			// Use the location of this class as the location to scan for annotated servlets and things
			URL classes = WebServer.class.getProtectionDomain().getCodeSource().getLocation();
			context.getMetaData().setWebInfClassesDirs(Arrays.asList(Resource.newResource(classes)));

			ServletHolder jerseyAdminServletHolder = context.addServlet(ServletContainer.class, "/admin/*");
			jerseyAdminServletHolder.setInitParameter("javax.ws.rs.Application", "org.icpc.vcss.webserver.VCSSAdminApplication");

			ServletHolder jerseyTeamServletHolder = context.addServlet(ServletContainer.class, "/public/*");
			jerseyTeamServletHolder.setInitParameter("javax.ws.rs.Application", "org.icpc.vcss.webserver.VCSSPublicApplication");

			//add a filter to allow Cross-Origin Requests
			//TODO:  this was added to support testing (invocation of /execute from a browser); does this represent a significant security risk?
			FilterHolder holder = new FilterHolder(new CrossOriginFilter());
			holder.setInitParameter(CrossOriginFilter.ALLOWED_METHODS_PARAM, "GET,PUT, POST,HEAD,OPTIONS");
			context.addFilter(holder, "/*", EnumSet.of(DispatcherType.REQUEST));

			//add the HTTPS connector to Jetty
			jettyServer.setConnectors(new Connector[] { httpsConnector });

			//make sure no clients are shown as "connected" in the database
			clearDatabaseClientConnections();
			
			//set a new "auto-stop" task for any clients marked as needing to be auto-stopped
			scheduleAutostopTasks();

			jettyServer.start();

			String startMsg = "Started server";
			startMsg += " on port " + port;
			startMsg += " using database " + wsProperties.getProperty("javax.persistence.jdbc.url");
			showMessage(startMsg);


		} catch (NumberFormatException e) {
			showMessage("Unable to start web services: invalid port number: " + e.getMessage(), e);
		} catch (IOException e1) {
			showMessage("Unable to start web services: " + e1.getMessage(), e1);
		} catch (Exception e2) {
			showMessage("Unable to start web services: " + e2.getMessage(), e2);
		}
	}

	/**
	 * This method returns a JSON String containing all the key/value pairs in the specified Properties object,
	 * but replaces the value of any property whose key contains "password" with asterisks.
	 *
	 * The specified Properties object is assumed to map Strings to Strings.
	 *
	 * @param props a Properties object mapping String keys to String values which is to have passwords filtered out of values.
	 *
	 * @return a String in JSON form containing the key/value pairs in the specified Properties, but with no passwords visible.
	 */
	private String filterPasswords(Properties props) {

		String retStr = "{" ;

		Set<Object> keys = props.keySet();
		boolean keysHaveBeenAdded = false;

		for (Object key : keys) {

			if (key instanceof String) {

				//if we've already added some properties (and we're about to add another one), add a comma
				if (keysHaveBeenAdded) {
					retStr += ", ";
				}

				//put the key into the return string, followed by "="
				String keyString = (String)key;
				retStr += keyString + "=";

				//get the value associated with the key
				String value = props.getProperty(keyString);

				if (value == null) {
					retStr += "null";
				} else if (keyString.contains("password")) {
						retStr += "******";
					} else {
						retStr += value;
					}

				keysHaveBeenAdded = true;
			}
		}

		retStr += "}";

		return retStr;
	}

	/**
	 * This method marks all machines currently defined in the database as "not connected".  This method should
	 * only be called when the server is first starting (actually, the method exists to handle the condition of the
	 * server RE-starting after a crash, at which time there may be machines in the database marked as "connected"
	 * when they clearly shouldn't be because if the server is just (re-)starting then NO machines are yet connected.).
	 */
	private void clearDatabaseClientConnections() {
		EntityManagerFactory emf = DatabaseUtilities.getCurrentDatabase();
		MachineDAO machineDAO = new MachineDAO(emf.createEntityManager());
		List<Machine> machines = machineDAO.getAll();
		for (Machine machine : machines) {
			machine.setConnected(false);
			machineDAO.save(machine);
		}
	}

	/**
	 * This method checks for machines marked for "autoShutoff" in the database, and if necessary schedules a new
	 * TimerTask to stop them.  This method should only be called when the server is first starting (actually, the 
	 * method exists to handle the condition of the server RE-starting after a crash, at which time there may be 
	 * machines in the database marked as needing "autoshutoff" but for which the TimerTask which was going to stop them
	 * disappeared when the server crashed.)
	 */
	private void scheduleAutostopTasks() {
		EntityManagerFactory emf = DatabaseUtilities.getCurrentDatabase();
		MachineDAO machineDAO = new MachineDAO(emf.createEntityManager());
		
		//check every cloud machine
		List<Machine> cloudMachines = machineDAO.getCloudMachines();
		for (Machine machine : cloudMachines) {
			
			//only consider scheduling an autostop if the machine is running
			if (machine.isRunning()) {
				
				//only schedule an autostop if the machine was supposed to be "auto-shutoff"
				if (machine.isAutoShutoff()) {

					//find how many minutes the machine is still supposed to be allowed to run
					int remainingRuntimeMins = getRemainingRuntimeMins(machine);

					//either shut the machine down (if its runtime has already elapsed), or schedule a future auto-stop
					if (remainingRuntimeMins <= 0) {
						stopMachine(machineDAO, machine);
					} else {
						scheduleStopInstanceTask(machineDAO, machine, remainingRuntimeMins);
					}
				} 
			} else {
				//machine is not running; clear its autoshutoff flag
				machine.setAutoShutoff(false);
				machineDAO.save(machine);
			}
		}
		
	}


	/**
	 * Returns the number of minutes which the specified machine has left to run.
	 * The remaining minutes is determined by comparing the mostRecentStartTime, plus the runtime for the machine
	 * (which is in minutes), with "now".
	 * If the mostRecentStartTime is null (meaning the machine has never been started), zero is returned.
	 * 
	 * @param machine the Machine whose remaining runtime minutes is to be found; expected to be non-null.
	 * @return the number of minutes the specified machine has left to run; a negative number means the
	 * 			machine has already exceeded its alloted runtime minutes.
	 */
	private int getRemainingRuntimeMins(Machine machine) {
		
		Date mostRecentStartTime = machine.getSelfStartStartTime();
		
		if (mostRecentStartTime != null) {
			
			//get machine's mostRecentStartTime in Unix epoch milliseconds
			long startTimeMillisecs = mostRecentStartTime.getTime();
			
			//convert machine's allowed runtime (which is in minutes) to long millisecs
			long allowedRuntimeMillisecs = machine.getRuntime() * 60 * 1000; //minutes * 60 secs/minute * 1000 msec/sec = msec
			
			//get current time, in Unix Epoch millisecs
			long now = new Date().getTime();
			
			//find number of remaining millisecs (will be negative if start+run is in the past)
			long remainingMillisecs = (startTimeMillisecs + allowedRuntimeMillisecs) - now;
			
			//return remaining millis converted to minutes
			return (int) (remainingMillisecs / (1000 * 60)); //millisecs / (1000msec/sec * 60sec/min) = minutes
			
		} else {
			//the machine has never been started
			return 0;
		}
	}

	/**
	 * Schedules a {@link TimerTask} which will execute after the specified runtime has elapsed
	 * and at that time will stop the specified machine, which is presumed to be an AWS Instance.
	 * 
	 * Note that runtime is expected to be in MINUTES.
	 * 
	 * @param machineDAO the {@link MachineDAO} object for accessing machines in the database.
	 * @param machine the machine for which a stop task is to be scheduled.
	 * @param runtime the number of minutes to delay before stopping the specified machine.
	 */
	private void scheduleStopInstanceTask(MachineDAO machineDAO, Machine machine, int runtimeMins) {

		String instanceId = machine.getEc2Id();
		
		//define the task we want performed
	    TimerTask task = new TimerTask() {
	        public void run() {
	        	stopMachine(machineDAO, machine);
	        }
	    };
	    
	    //create a Timer with which to schedule the task
	    Timer timer = new Timer("Timer-" + instanceId);
	    
	    //compute the number of milliseconds to delay before executing the task
	    long delay = runtimeMins*60*1000L;  //minutes X 60secs/min X 1000 msec/sec
	    
	    //schedule the task for execution after "delay" msec
	    timer.schedule(task, delay);
	}
	
	/**
	 * Invokes the AWS API to stop the specified machine, which is presumed to represent an AWS Instance.
	 * If AWS indicates that stopping the machine was successful, updates the machine state in the database
	 * 
	 * @param machineDAO the {@link MachineDAO} object for accessing machines in the database.
	 * @param machine the machine which is to be stopped, presumed to be an AWS Instance.
	 */
	private void stopMachine(MachineDAO machineDAO, Machine machine) {

		String instanceId = machine.getEc2Id();

		log.info("Stopping AWS instance " + instanceId);
    	
    	final AmazonEC2 ec2 = AmazonEC2ClientBuilder.defaultClient();

    	StopInstancesRequest stopRequest = new StopInstancesRequest().withInstanceIds(instanceId);

    	StopInstancesResult stopResult = ec2.stopInstances(stopRequest);
    	
		List<InstanceStateChange> instanceStates = stopResult.getStoppingInstances();

		if (instanceStates==null || instanceStates.size()==0) {
			log.severe("Null or empty AWS InstanceState list returned while attempting to stop machine instance " + instanceId);
			return;
		}

		InstanceState state = instanceStates.get(0).getCurrentState();

		if (state==null) {
			log.severe("Null AWS Instance State returned while attempting to stop machine instance " + instanceId);
			return;
		}

		String curState = state.getName();

		if (curState!=null && (curState.contentEquals("shutting-down") ||
								curState.contentEquals("stopping") ||
								curState.contentEquals("stopped")
							  ) ) {
			
			log.info("Stopped AWS Instance " + instanceId);	
			
			//update the machine self-start parameters
	    	Machine targetMachine = machineDAO.findByInstanceId(instanceId);
	    	
			if (targetMachine!=null) {
				String name = targetMachine.getName();
				log.info("Setting autoshutoff=false, connected=false, stoptime=now, running=false in database for InstanceId " 
							+ instanceId + " (" + name + ")"); 
				targetMachine.setAutoShutoff(false);
				targetMachine.setConnected(false);
				targetMachine.setSelfStartStopTime(new Date());
				targetMachine.setIsRunning(false);
				machineDAO.save(targetMachine);

			} else {
				log.severe("Unable to find InstanceId " + instanceId + " in database; cannot clear settings");
			}
			
		} else {
			log.severe("AWS returned Instance State other than 'shutting-down' or 'stopping' "
					+ " or 'stopped' while stopping machine instance " + instanceId);
		}
    	
		return;
    }

	
	private URI getWebRootResourceUri() throws FileNotFoundException, URISyntaxException {
		URL indexUri = this.getClass().getResource(WEBROOT_INDEX);
		if (indexUri == null) {
			throw new FileNotFoundException("Unable to find resource " + WEBROOT_INDEX);
		}
		// Points to wherever /webroot/ (the resource) is
		return indexUri.toURI();
	}

	private void showMessage(final String message, Exception ex) {
		getLog().log(Log.INFO, message, ex);
		System.out.println(new Date() + " " + message);
		ex.printStackTrace();
	}

	private void showMessage(String message) {
		// TODO REFACTOR remove showMessage replace with log.info(message);
//		System.out.println(new Date() + " " + message);
		log.info(message);
	}

	public static Log getLog() {
		// TODO REFACTOR remove getLog use log   Comment on this comment: why?  Use of accessors is normally QUITE preferable  (jlc)
		return log;
	}

  private SecurityHandler basicAuth() {
    try {

      HashLoginService l = new HashLoginService();
      File f = new File("realm.properties");
      if (f.exists() && f.isFile() && f.canRead()) {
        showMessage("Loading " + f.getAbsolutePath());
        l.setHotReload(true); // hot reload of the Property file
        l.setConfig(f.getAbsolutePath());
        try {
          l.start();
        } catch (Exception e) {
          showMessage(e.getMessage(), e);
        }
      } else if (!f.exists()) {
        showMessage("WARNING: " + f.getAbsolutePath() + " does not exist");
      } else if (!f.isFile()) {
        showMessage("WARNING: " + f.getAbsolutePath() + " is not a file");
      } else {
        showMessage("WARNING: Cannot read " + f.getAbsolutePath());
      }

      Constraint constraintPublic = new Constraint();
      constraintPublic.setAuthenticate(false);

      Constraint constraintAdmin = new Constraint();
      constraintAdmin.setName(Constraint.__BASIC_AUTH);
      constraintAdmin.setRoles(new String[] { "admin" });
      constraintAdmin.setAuthenticate(true);

      // Root is unathenticated/public access
      ConstraintMapping cmUnauthenticated = new ConstraintMapping();
      cmUnauthenticated.setConstraint(constraintPublic);
      cmUnauthenticated.setPathSpec("/*");

      // admin requires admin access
      ConstraintMapping cmAdmin = new ConstraintMapping();
      cmAdmin.setConstraint(constraintAdmin);
      cmAdmin.setPathSpec("/admin/*");

      ConstraintSecurityHandler csh = new ConstraintSecurityHandler();
      csh.setAuthenticator(new BasicAuthenticator());
      csh.setRealmName("vcss");
      csh.addConstraintMapping(cmAdmin);
      csh.addConstraintMapping(cmUnauthenticated);
      csh.setLoginService(l);

      return csh;

    } catch (Exception e) {
      log.log(Level.WARNING, "Problem in basic Auth", e);

      throw e;
    }

  }

	/**
	 * Returns the value of the specified property in the global wsProperties table,
	 * or the specified boolean value if the specified property is not found in the
	 * wsProperties table. Property values "true", "yes", "on", and "enabled" are
	 * treated as true; any other string is considered false.
	 *
	 * @param key - a wsProperties table property key
	 * @param b   - the value to be returned if key is not found in wsProperties
	 *
	 * @return true if key is found in wsProperties and has a value which is any of
	 *         "true", "yes", "on", or "enabled"; false if key is found in
	 *         wsProperties but has any other value; b if key is not found in
	 *         wsProperties
	 */
	protected boolean getBooleanProperty(String key, boolean b) {

		String value = wsProperties.getProperty(key);

		if (value == null) {
			return b;
		} else {
			return "true".equalsIgnoreCase(value.trim()) || //
					"yes".equalsIgnoreCase(value.trim()) || //
					"on".equalsIgnoreCase(value.trim()) || //
					"enabled".equalsIgnoreCase(value.trim());
		}

	}

	protected int getIntegerProperty(String key, int defaultValue) {

		String value = wsProperties.getProperty(key);

		if (value == null) {
			return defaultValue;
		} else {
			try {
				return Integer.parseInt(value);
			} catch (Exception e) {
				return defaultValue;
			}
		}
	}

	public static Properties createSampleProperties() {

		Properties props = new Properties();

		props.put(PORT_NUMBER_KEY, DEFAULT_WEB_SERVER_PORT_NUMBER + "");

		return props;
	}

	public void stop() {
		try {
			jettyServer.stop();
		} catch (Exception e1) {
			getLog().log(Log.INFO, "Unable to stop Jetty webserver: " + e1.getMessage(), e1);
		}
		jettyServer.destroy();
	}

	public boolean isServerRunning() {

		boolean serverRunning = false;

		try {

		if (jettyServer == null) {
			serverRunning = false;
		} else {
			serverRunning = jettyServer.isRunning();
		}

		return serverRunning;

		} catch (Exception e) {
			// TODO: handle exception
			log.log(Level.WARNING, "Problem in isServerRunning", e);
			return false;
		}
	}

	/**
	 * Loads properties from the specified file and returns them in a Properties
	 * object.
	 *
	 * @param filename - the file to load from
	 * @return a Properties object containing the Properties given in the file
	 */

	protected Properties loadPropertiesFile(String filename) {

		Properties properties = new Properties();
		try {
			FileReader reader = new FileReader(new File(filename));
			properties.load(reader);

			// debug:
			String choice ;
			if (properties.size()==1) {
				choice = "property";
			} else {
				choice = "properties";
			}
			System.out.println("Loaded " + properties.size() + " " + choice + " from '" + filename + "'");

		} catch (Exception e) {
			log.log(Log.SEVERE,"Exception loading properties file '" + filename + "':  " + e, e);
		}

		return properties;
	}
	
	public static String getProperty(String propName, String defaultValue) {
		if (wsProperties==null) {
			return defaultValue;
		} else {
			return wsProperties.getProperty(propName,defaultValue);
		}
	}

	/**
	 * This method starts the VCSS web server. On startup the web server reads the
	 * WEB_SERVER_PROPERTIES_FILENAME to obtain its configuration, then starts a Jetty webserver
	 * which listens for websocket connnections from clients.
	 *
	 * @param args not used
	 */
	public static void main(String[] args) {
	    // make hibernate quiet
	    // java.util.logging.Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
	    // java.util.logging.Logger.getLogger("com.mchange.v2.c3p0").setLevel(Level.SEVERE);

		new WebServer().startWebServer();

	}

}
