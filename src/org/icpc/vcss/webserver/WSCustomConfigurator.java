package org.icpc.vcss.webserver;

import javax.websocket.server.ServerEndpointConfig.Configurator;

import org.glassfish.hk2.api.ServiceLocator;
import org.glassfish.hk2.utilities.ServiceLocatorUtilities;

/**
 * This is used by the WSServerEndpoint class to enable hk2 dependency injection
 * The injectable things can be found in the VCSSBinder class
 */
public class WSCustomConfigurator extends Configurator {
    private ServiceLocator serviceLocator;

    public WSCustomConfigurator() {
        serviceLocator = ServiceLocatorUtilities.createAndPopulateServiceLocator();
        ServiceLocatorUtilities.enableLookupExceptions(serviceLocator);
        ServiceLocatorUtilities.bind(serviceLocator, new VCSSBinder(){
          @Override
            protected void configure() {
                super.configure();
                bind(WSServerEndpoint.class).to(WSServerEndpoint.class);
            }
        });
    }

    @Override
    public <T> T getEndpointInstance(Class<T> endpointClass) throws InstantiationException
    {
        return serviceLocator.getService(endpointClass);
    }
}
