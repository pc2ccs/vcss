package org.icpc.vcss.webserver.services.admin;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.util.Base64;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.websocket.Session;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.eclipse.jetty.client.HttpResponse;
import org.icpc.vcss.client.ClientCommandType;
import org.icpc.vcss.core.commands.CommandStatusType;
import org.icpc.vcss.core.commands.MachineCommandResultStatusType;
import org.icpc.vcss.core.log.Log;
import org.icpc.vcss.entity.Command;
import org.icpc.vcss.entity.CommandDAO;
import org.icpc.vcss.entity.ExecuteCommand;
import org.icpc.vcss.entity.ExecuteCommandResult;
import org.icpc.vcss.entity.GetCommand;
import org.icpc.vcss.entity.GetCommandResult;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineCommandResult;
import org.icpc.vcss.entity.MachineCommandResultDAO;
import org.icpc.vcss.entity.MachineDAO;
import org.icpc.vcss.entity.PutCommand;
import org.icpc.vcss.entity.PutCommandResult;
import org.icpc.vcss.util.UUIDValidator;
import org.icpc.vcss.webserver.WSServerEndpoint;
import org.icpc.vcss.webserver.websocket.User;
import org.icpc.vcss.websocketMessage.server.WSCommandMsg;

/**
 * This class is a webservice to dispatch commands received from the Contest Admin's front-end web page.
 * It provides a REST API endpoint "/dispatchCommand" which receives "execute", "put", and "get" commands,
 * along with a list of target machines; marshalls the appropriate arguments for each command and
 * then invokes the specific service to perform the command.
 *
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
@Path("/dispatchCommand")
//@Produces(MediaType.APPLICATION_JSON)
//@Consumes(MediaType.APPLICATION_JSON)
//@Provider
@Singleton
public class CommandDispatcherService {

	private Log log;

	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;
	private MachineDAO machineDAO;
	private CommandDAO commandDAO;
	private MachineCommandResultDAO machineCommandResultDAO;

  @Inject
	public CommandDispatcherService(Log log, EntityManagerFactory emf) {
		super();
		this.log = log;
		this.entityManagerFactory = emf;
	    this.entityManager = entityManagerFactory.createEntityManager();
	    this.machineDAO = new MachineDAO(entityManager);
	    this.commandDAO = new CommandDAO(entityManager);
	    this.machineCommandResultDAO = new MachineCommandResultDAO(entityManager);
	}

	/**
	 * This method is invoked when the Contest Admin's "Command" page submits a request to have a command applied to a
	 * list of target machines.  It expects a set of HMTL "form data" containing a list of machine IDs and a command,
	 * in URL-encoded format, in the Request Body of the received HTTP request;
	 * it extracts the request data and performs the operation specified therein.
	 *
	 * Each time the dispatchCommand() method is invoked it assigns a new "commandID" to the command and
	 * saves the command for subsequent access by the {@link CommandResultsService} service.
	 *
	 * @param sc the {@link SecurityContext} in which the method runs.
	 * @param request the {@link HttpServletRequest} whose Request Body contains the operation to be performed.
	 * @param response an {@link HttpServletResponse} to be returned to the invoker.
	 * @return an {@link HttpResponse}
	 * @throws IOException
	 */
	@POST
	@Consumes("application/x-www-form-urlencoded")
    @Produces("text/plain")
    public Response dispatchCommand(@Context SecurityContext sc, @Context HttpServletRequest request,  @Context HttpServletResponse response
    		,@FormParam("machines[]") List<String> machines
    		,@FormParam("command") String command
    		) throws IOException {

		String msg = "An error occurred while sending command "+command;

		try {

        // DEBUG:
        System.out.println ("\nDispatchCommand Service invoked");
        log.log(Log.DEBUG, "DispatchCommand Service invoked");

      //TODO: authenticate user (or can this be done at the HTTP request level before we ever get here?)
//    // check authorization (verify requester is allowed to make this request)
//    if (!sc.isUserInRole("admin")) {
//    	//debug:
//    	System.out.println("ExecuteService.execute(): unauthorized execute request (user not in admin role)");
//      log.log(Log.WARNING, "ExecuteService.execute(): unauthorized execute request (user not in admin role)");
//      // return HTTP 401 response code
//      return Response.status(Status.UNAUTHORIZED).entity("You are not authorized to access this page").build();
//    }

        //make sure we got some machines
		System.out.println("Machines array:");
		if (machines==null || machines.size()==0) {
			System.out.println("  null or empty");
			System.out.println("CommandDispatcherService.dispatchCommand(): null or empty machine list; cannot continue "
								+ "(did the Admin forget to select target machine(s)? )");
			log.severe("Null or empty machine list; cannot continue (did the Admin forget to select target machine(s)? )");
			return Response.status(Status.BAD_REQUEST)
					.entity("Null or empty machine list; cannot continue (did you forget to select target machine(s)? )")
					.build();
		} else {
			for (int i = 0; i < machines.size(); i++) {
				System.out.println("  " + machines.get(i));
			}
		}

		//verify all machines are represented by valid UUIDs
		for (String machineUUID : machines) {
			if (!UUIDValidator.isValidUUID(machineUUID)) {
				System.out.println("CommandDispatcherService.dispatchCommand(): invalid machine UUID in request: " + machineUUID);
				log.severe("Invalid machine UUID in request: " + machineUUID);
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity("The server found an invalid machine UUID in the HTTP request: " + machineUUID + "."
								+ " This could be due to an internal server error or a bad request")
						.build();
			}
		}

		//make sure we got a non-empty command
		if (command==null || command.equals("")) {

			System.out.println("CommandDispatcherService.dispatchCommand(): null or empty command found in HTTP request; cannot continue");
			log.severe("Null or empty command found in HTTP request; cannot continue");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("The server was unable to find a command in the HTTP request; this could be due to "
							+ "an internal server error or a bad request")
					.build();
		} else {
			System.out.println ("CommandDispatcherService.dispatchCommand(): dispatching command: " + command);
			log.info("Dispatching command: " + command);
		}

		//validate the command
		if (!validCommand(command)) {

			System.out.println("CommandDispatcherService.dispatchCommand(): invalid command: " + command);
			log.severe("Invalid command received from admin: " + command);
			return Response.status(Status.BAD_REQUEST)
					.entity("Invalid command: " + command )
					.build();
		}

		//we have a valid command (that is, we know that it must be EXECUTE, PUT, or GET), and list of machines;
		// get a Command object to encapsulate it

		String [] tokens = command.split(" ");
		ClientCommandType commandType = ClientCommandType.fromString(tokens[0]);
		//get all the arguments of the command except the command verb (i.e., everything past the first space)
		String commandArgs = command.substring(command.indexOf(" ")+1);
		Command commandObj ;
		Class<?> resultType;
		boolean success = true;
		switch(commandType) {
			case EXECUTE:
				resultType = ExecuteCommandResult.class;
				commandObj = new ExecuteCommand();
				((ExecuteCommand)commandObj).setText(commandArgs);
				break;
			case GET:
				resultType = GetCommandResult.class;
				commandObj = new GetCommand();
				((GetCommand)commandObj).setText(commandArgs);
				break;
			case PUT:
				resultType = PutCommandResult.class;
				commandObj = new PutCommand();
				// TODO: FIXME: HACK: XXX: RESOLVE THIS
//				((PutCommand)commandObj).setText(commandArgs);
				success = addBase64EncodedFileToCommand((PutCommand)commandObj);
				break;
			default:
				System.out.println("CommandDispatcherService.dispatchCommand(): server error: "
										+ "unkown command type accepted by method validCommand()");
				log.severe("Server error: unkown command type accepted by method validCommand()");
				return Response.status(Status.INTERNAL_SERVER_ERROR)
						.entity("Server error: unkown command type accepted by method validCommand()")
						.build();
		}

		if (!success) {
			System.out.println("CommandDispatcherService.dispatchCommand(): unable to read/convert file in Put command");
			log.warning("Unable to read/convert file in Put command");
			return Response.status(Status.BAD_REQUEST)
				.entity("CommandDispatcherService.dispatchCommand(): unable to read/convert file in Put command")
				.build();
		}

		//TODO:  the above produces a Command object which has no id -- i.e., an object in an invalid state.
		// It should not be possible to do this... we need a Factory which produces Commands which
		// always have id's in them, and the Factory is the ONLY way to get a new Command object.

		//insert into DB and get back Command object with commandID assigned
		commandObj = commandDAO.save(commandObj);

		//make sure we got a Command with a valid (i.e., positive) commandID
		long commandID = commandObj.getId();

		if (commandID<1) {
			System.out.println("CommandDispatcherService.dispatchCommand(): initial command object has ID<1; cannot continue");
			log.severe("initial command object has ID<1");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("The server was unable to allocate a Command object for the command.")
					.build();
		}

		commandObj.setStatus(CommandStatusType.NEW);

		//save the command
		commandObj = commandDAO.save(commandObj);

		if (commandObj==null) {
			System.out.println("CommandDispatcherService.dispatchCommand(): unable to save command");
			log.severe("Unable to save command");
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("The server was unable to save the command")
					.build();
		}

		//send the command to each specified machine and create a "results" object for responses
		boolean sentToAllMachinesOK = true;
		boolean createdAllMachineResultsOK = true;
		for (String machineUUID : machines) {
			Machine machine = machineDAO.findByUuid(UUID.fromString(machineUUID));

			// TODO: put this on a separate thread so we can return from the HTTP call quickly
			// (See comment in sendCommandMessageToClient(); maybe the separate threading
			// could/should be done there...
			sentToAllMachinesOK = sentToAllMachinesOK & sendCommandToMachine(commandObj, machineUUID);

			// get a new result object for this command/machine combination
			// TODO: see the comments in class MachineCommandResult regarding this
			// constructor...
			MachineCommandResult resultObj = null;
			try {
				resultObj = (MachineCommandResult) resultType.getDeclaredConstructor().newInstance();
				resultObj.setCommand(commandObj);
				resultObj.setMachine(machine);
				resultObj.setResponse("No response yet");
				resultObj.setStatus(MachineCommandResultStatusType.PENDING);

				// update the object in the database
				resultObj = machineCommandResultDAO.save(resultObj);
			} catch (InstantiationException | IllegalAccessException | IllegalArgumentException | InvocationTargetException | NoSuchMethodException | SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			createdAllMachineResultsOK = createdAllMachineResultsOK & resultObj != null;
		}

		//command has been sent to all machines; mark it "pending" in the database.
		//TODO: possible timing issue here: what if some machine responds quickly, before other machines have received the
		// command in the above loop, and the response from that machine updates the command status to "Partially_Completed"?
		// Then the following will revert it back to "Pending"...  It's not clear how to synchronize this issue...
		commandObj.setStatus(CommandStatusType.PENDING);
		commandObj = commandDAO.save(commandObj);


		if (sentToAllMachinesOK && createdAllMachineResultsOK) {
			msg = "You successfully sent your command ";
		} else {
			msg = "An error occurred while sending command ";
		}

		msg += "'" + command + "'";
		msg += machines.size()>1 ? " to machines " : " to machine ";
		boolean first = true;
		for (int i=0; i<machines.size(); i++) {
			if (!first) {
				msg += ", ";
				if (i==machines.size()-1) {
					msg += "and ";
				}
			}
			msg += machines.get(i);
			first = false;
		}

		System.out.println(msg);
        if (sentToAllMachinesOK && createdAllMachineResultsOK) {
			log.log(Log.INFO, msg);
			return Response.status(Status.OK).entity(msg).build();
		} else {
			log.log(Log.WARNING, msg);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		}



		} catch (Exception e) {

			log.log(Log.WARNING, msg, e);
			return Response.status(Status.INTERNAL_SERVER_ERROR).entity(msg).build();
		}

	}

	/**
	 * Reads the value of the "sourceFilePath" field of the specified {@link PutCommand},
	 * which is assumed to contain the full path name of a file which is to be "Put" to a client machine,
	 * and uses that to load the base64SourceFileContents field of the {@link PutCommand} with
	 * the Base64-encoded version of that file.
	 *
	 * NOTE: the destination directory must already exist on the client machine; use the "Execute" command to
     * send a mkdir -p full_dir_path,  possibly with additional commands to set owner/group, prior to sending
     * a "Put" command to a client.
     *
	 * @param command a {@link PutCommand} whose fields (including specifically the sourceFilePath field) are
	 * 					assumed to have been loaded with the command tokens.
	 * @return true if the file indicated by the sourceFilePath field of the specified {@link PutCommand}
	 * 			was successfully used to load the base64SourceFileContents field with the Base64-encoded
	 * 			version of the specified file; false if an error occurred attempting to read and convert the file.
	 */
	private boolean addBase64EncodedFileToCommand(PutCommand command) {


		String sourceFile = command.getSourceFilePath();

		if (sourceFile==null || sourceFile.equals("")){
			System.out.println("CommandDispatcherService.addBase64EncodedFileToCommand(): null or empty source file name in Put command");
			log.severe("Null or empty source file name in Put command");
			return false;
		}

		File file = new File(sourceFile);

		if (file.canRead()) {

		    try {

		    	//read the file bytes
		        byte[] fileContent = Files.readAllBytes(file.toPath());
		        //encode to Base64
		        String encodedFile = Base64.getEncoder().encodeToString(fileContent);
		        command.setBase64SourceFileContents(encodedFile);

		    } catch (IOException e) {
				System.out.println("CommandDispatcherService.addBase64EncodedFileToCommand(): IOException reading file '" + sourceFile + "'");
				log.severe("IOException reading file '" + sourceFile + "'");
				return false;
		    }

		} else {
			System.out.println("CommandDispatcherService.addBase64EncodedFileToCommand(): cannot read file named in Put command: " + sourceFile);
			log.severe("Cannot read file named in Put command: " + sourceFile);
			return false;
		}

		return true;
	}

	/**
	 * Validates that the specified command string is valid.  In particular, this means that the
	 * command must start with one of the recognized command verbs (e.g. "execute", "put", or "get")
	 * and that it must have the appropriate number of arguments following the verb.
	 *
	 * @param command a String containing the command to be validated.
	 * @return true if the command passes all verification tests; otherwise false.
	 */
	private boolean validCommand(String command) {
		String [] tokens = command.split(" ");

		//make sure we have at least the minimum number of tokens (every command has a verb (token 0) and at least one argument)
		if (tokens==null || tokens.length<2) {
			return false;
		}

		//make sure we have a proper verb
		ClientCommandType commandType = ClientCommandType.fromString(tokens[0]);
		if(!(commandType instanceof ClientCommandType)) {
			return false;
		}

		//check that each type of command has the proper number of arguments
		switch (commandType) {

			case EXECUTE:
				//every "execute" command needs at least two tokens (the "execute" and the OS command to be executed)
				if (tokens.length<2) {
					return false;
				}
				break;

			case GET:
				//every "get" needs at least two tokens (the "get" and the name of the file to be "got")
				if (tokens.length<2) {
					return false;
				}
				break;

			case PUT:
				//every "put" needs at least three tokens (the "put", the name of the local file to be "put", and the path where the file should be "put")
				if (tokens.length<3) {
					return false;
				}
				break;

			case UNDEFINED:
			default:
				return false;
		}

		return true;
	}

	/**
	 * Sends the specified command to the machine with the specified UUID.
	 *
	 * @param command a {@link Command} representing the command to be sent; the "text" field
	 * 			of the Command contains the actual command including any command line arguments.
	 * @param machineUUID the machine to which the command should be sent.
	 * @return true if the command was sent to the specified machine (no guarantees regarding receipt);
	 * 			false if an error occurred during the sending of the command.
	 */
	private boolean sendCommandToMachine(Command command, String machineUUID) {

		//get the websocket session associated with the specified machineUUID
		Session session = getWebsocketSession(machineUUID);

		//make sure we got a non-null Session
		if (session==null) {
			System.out.println ("CommandDispatcherService.sendCommandToMachine(): cannot find websocket Session for machine " + machineUUID
									+ "; cannot send command");
			log.warning("Cannot find websocket Session for machine " + machineUUID + "; cannot send command");
			return false;

		}

		//send the command to the websocket
		boolean success = sendCommandMessageToClient(command, session);

		return success;

	}

	/**
	 * Returns the Websocket {@link Session} for the client with the specified machine UUID, or null if
	 * no websocket session for that machine was found.
	 *
	 * @param machineUUID a String representing the UUID of a client machine.
	 * @return the WebSocket Session through which the client machine is currently connected, or null if not found.
	 */
	private Session getWebsocketSession(String machineUUID) {

		//get the websocket sessionID associated with the specified machine UUID
		String websocketSessionID = WSServerEndpoint.getClientUUIDtoSessionIDMap().get(machineUUID);

		//make sure we got a non-null websocket session ID
		if (websocketSessionID==null) {

			System.out.println ("CommandDispatcherService.getWebsocketSession(): cannot find websocket session ID for machine " + machineUUID);
			log.warning("Cannot find websocket session ID for machine " + machineUUID);
			return null;
		}

		System.out.println("CommandDispatcherService.getWebsocketSession(): found websocket Session ID " + websocketSessionID + " for machine " + machineUUID);
		log.info("Found websocket Session ID " + websocketSessionID + " for machine " + machineUUID);

		//get the user associated with the websocket sessionID
		User user = WSServerEndpoint.getConnections().get(websocketSessionID);

		//make sure we got a non-null user
		if (user==null) {

			System.out.println ("CommandDispatcherService.getWebsocketSession(): cannot find User for machine " + machineUUID);
			log.warning("Cannot find User for machine " + machineUUID);
			return null;
		}

		System.out.println("CommandDispatcherService.getWebsocketSession(): found user " + user.getUserName() + " for machine " + machineUUID);
		log.info("Found user " + user.getUserName() + " for machine " + machineUUID);

		//get the user's websocket session
		Session userSession = user.getUserSession();

		//make sure we got a non-null Session
		if (userSession==null) {

			System.out.println ("CommandDispatcherService.getWebsocketSession(): cannot find websocket Session for machine " + machineUUID);
			log.warning("Cannot find websocket Session for machine " + machineUUID);
			return null;
		}

		System.out.println("CommandDispatcherService.getWebsocketSession(): found Session " + userSession.getId() + " for machine " + machineUUID);
		log.info("Found Session " + userSession.getId() + " for machine " + machineUUID);

		return userSession ;
	}


//	/**
//	 * This is a callback method for javax.ws.rs {@link Feature}s.  It is called when the JAX-RS runtime
//	 * initializes the registered services; it allows each registered Feature service to configure itself
//	 * if desired.
//	 *
//	 * @param context the configuration context for this Feature.
//	 * @return true if the Feature is considered successfully configured and enabled; false if not (thus, a Feature
//	 * 				can disable itself at startup by returning false).
//	 */
//	@Override
//	public boolean configure(FeatureContext context) {
//		// no special configuration needed (as far as is known right now...)
//		return true;
//	}

    /**
     * This method sends the specified message to the client represented by the
     * specified Websocket {@link Session}.
     *
     * @param message the message to be sent to the client.
     * @param session the Websocket {@link Session}through which the client is connected.
     *
     * @return true if the message was sent (no guarantees on receipt however); false if an error occurred during sending.
     */
    private boolean sendCommandMessageToClient(Command command, Session session) {
    	System.out.println ("\nCommandDispatcherService: sending command " + command.getId() + " to client " + session.getId());
    	log.info("Sending command " + command.getId() + " to client " + session.getId());
		try {

			//build a websocket message containing the command
			WSCommandMsg cmdMsg = new WSCommandMsg(command);

			//send the message to the client
			//TODO: this should be spawned off on a separate thread
			session.getBasicRemote().sendObject(cmdMsg);

		} catch (IOException e) {
			System.out.println ("CommandDispatcherService.sendCommandMessageToClient(): IOException sending message to client: " + e.getMessage());
			log.warning("IOException sending message to client: " + e.getMessage());
			return false;
		} catch (Exception e) {
			System.out.println ("CommandDispatcherService.sendCommandMessageToClient(): Exception sending message to client: " + e.getMessage());
			log.warning("IOException sending message to client: " + e.getMessage());
			return false;
		}

		return true;
	}

	//this method exists for JUnit testing
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
