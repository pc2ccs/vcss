package org.icpc.vcss.webserver.services.admin;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import org.glassfish.jersey.server.mvc.Viewable;
import org.icpc.vcss.core.log.Log;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineDAO;

/**
 * Webservice to serve to Contest Admins the current list of connected machines.
 *
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
@Path("/machines")
@Produces("text/html")
public class MachineListService {
  private Log log;
	private MachineDAO machineDAO;

  @Inject
	public MachineListService(EntityManagerFactory emf, Log log) {
		super();
    this.log = log;
		this.machineDAO = new MachineDAO(emf.createEntityManager());
	}

	/**
	 * Sends back to the invoker an HTML page containing a table listing all known machines.
	 */
	@GET
    @Produces("text/html")
    public Viewable getMachines() {
        // DEBUG:
        System.out.println ("\nGet All Machines service invoked");
        log.info("Get All Machines service invoked");

        // TODO: clear entitymanager?
    		List<Machine> machines = machineDAO.getAll();

        Map<String, Object> model = new HashMap<>();
				model.put("machines", machines);

				return new Viewable("/machines.jsp", model);
	}
}
