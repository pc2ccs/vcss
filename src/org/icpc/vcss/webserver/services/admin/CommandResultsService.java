package org.icpc.vcss.webserver.services.admin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.BadRequestException;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import org.glassfish.jersey.server.mvc.Viewable;
import org.icpc.vcss.core.log.Log;
import org.icpc.vcss.util.UUIDValidator;

import org.icpc.vcss.core.log.Log;
import org.icpc.vcss.entity.Command;
import org.icpc.vcss.entity.CommandDAO;
import org.icpc.vcss.entity.ExecuteCommand;
import org.icpc.vcss.entity.GetCommand;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineCommandId;
import org.icpc.vcss.entity.MachineCommandResult;
import org.icpc.vcss.entity.MachineCommandResultDAO;
import org.icpc.vcss.entity.MachineDAO;
import org.icpc.vcss.entity.PutCommand;


/**
 * Webservice to serve to Contest Admins the current status/results of previously-executed commands.
 *
 * Provides the following endpoints related to handling commands:
 *
 * <pre>
 * 	<ul>
 *   	<li> <b>/admin/commands</b>: returns a list of all commands which have been submitted.
 *   	<li> <b>/admin/commands/{commandID}</b>: returns a description of command {commandID}.
 *   	<li> <b>/admin/commands/{commandID}/results</b>: returns a description of the results (responses)
 *   			which have been received from all of the machines to which {commandID} was sent.
 *   	<li> <b>/admin/commands/{commandID}/{machineID}</b>: returns a description of the most recent response
 *   			(if any) to {commandID} from {machineID}.
 * 	</ul>
 * </pre>
 *
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
@Path("/commands")
public class CommandResultsService {

	private Log log;

	private EntityManagerFactory entityManagerFactory;
	private MachineDAO machineDAO;
	private CommandDAO commandDAO;
	private MachineCommandResultDAO machineCommandResultDAO;

	@Inject
	public CommandResultsService(Log log, EntityManagerFactory emf) {
		super();
		this.log = log;

    this.entityManagerFactory = emf;
		EntityManager em = entityManagerFactory.createEntityManager();
		this.machineDAO = new MachineDAO(em);
		this.commandDAO = new CommandDAO(em);
		this.machineCommandResultDAO = new MachineCommandResultDAO(em);
	}

	/**
	 * Sends back to the invoker an HTML page containing a table listing all
	 * {@link Command}s which have been received.
	 *
	 * @param sc       the SecurityContext for the request.
	 * @param request  the HTTP servlet request object.
	 * @param response the HTTP servlet response object to which the HTML web page
	 *                 will be sent.
	 *
	 * @return a {@link Response} object containing the requested data.
	 */

	@GET
	public Viewable getAllCommands() {
    // DEBUG:
    System.out.println ("\nGet All Commands service invoked");
    log.info("Get All Commands service invoked");

		List<Command> commands = commandDAO.getAll();
		List<Machine> connectedMachines = machineDAO.getConnectedMachines();

		Map<String, Object> model = new HashMap<>();
		model.put("commands", commands);
		model.put("connectedMachines", connectedMachines);

		return new Viewable("/commands.jsp", model);
	}

	/**
	 * Returns an HTML page containing the contents of a specific command, or BAD_REQUEST if the
	 * specified commandID does not exist.
	 *
	 * @param commandID the ID of the specific command whose information is being requested.
	 */
	@GET
	@Path("{commandId}")
    public Viewable getSpecificCommand(@PathParam("commandId") int commandId) {
        Command command = commandDAO.get(commandId).orElse(null);
        if (command == null) {
          throw new NotFoundException();
        }

				Map<String, Object> model = new HashMap<>();
				model.put("command", command);
				model.put("results", null);

				return new Viewable("/command_results.jsp", model);
	}

	/**
	 * Returns an HTML page containing all results for a specific command, or BAD_REQUEST if the
	 * specified commandID does not exist.
	 *
	 * @param commandID the ID of the specific command whose information is being requested.
	 */
	@GET
	@Path("{commandId}/results/")
    @Produces("text/html")
    public Viewable getSpecificCommandResults(@PathParam("commandId") int commandId) {
        Command command = commandDAO.get(commandId).orElse(null);
        if (command == null) {
          throw new NotFoundException();
        }

        List<MachineCommandResult> results = command.getResults();
        Map<String, Object> model = new HashMap<>();
				model.put("command", command);
				model.put("results", results);

				return new Viewable("/command_results.jsp", model);
	}


	/**
	 * Returns an HTML page containing the results for a specific command sent to a specific machine,
	 * or BAD_REQUEST if the specified commandID or machine UUID does not exist.
	 *
	 * @param commandId an int giving the ID of the specific command whose information is being requested.
	 * @param machineId a String giving the UUID of the specific machine whose information is being requested.
	 */
	@GET
	@Path("{commandId}/results/{machineId}")
    @Produces("text/html")
    public Viewable getSpecificMachineResults(@PathParam("commandId") int commandId, @PathParam("machineId") String machineId) {
        //validate the incoming machineUUID
        if (!UUIDValidator.isValidUUID(machineId)) {
        	String msg = "Invalid machine ID: " + machineId;
			System.out.println("CommandResultsService.getSpecificMachineResults(): " + msg);
			log.warning(msg);
			throw new BadRequestException(msg);
        }

        Command command = commandDAO.get(commandId).orElse(null);
        if (command == null) {
          throw new NotFoundException("Command not found");
        }

        //get the current result for the specified command from the specified machine
        Machine machine = machineDAO.findByUuid(UUID.fromString(machineId));
        MachineCommandResult result = machineCommandResultDAO.get(new MachineCommandId(machine.getId(), command.getId())).orElse(null);
        if (result == null) {
          throw new NotFoundException("No result for that machine and command combination");
        }

				// turn it into a list, since that's what our template wants
				List<MachineCommandResult> results = new ArrayList<MachineCommandResult>();
				results.add(result);

				Map<String, Object> model = new HashMap<>();
				model.put("command", command);
				model.put("results", results);

				return new Viewable("/command_results.jsp", model);
	}
}
