package org.icpc.vcss.webserver.services.admin;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

import org.glassfish.jersey.server.mvc.Viewable;

@Path("/")
public class IndexService {
  @GET
  public Viewable index(@PathParam("index") String _unused) {
    return new Viewable("/index.jsp");
  }
}
