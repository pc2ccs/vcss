package org.icpc.vcss.webserver.services.pub;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;

import org.eclipse.jetty.client.HttpResponse;
import org.icpc.vcss.core.log.Log;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineDAO;
import org.icpc.vcss.webserver.WebServer;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.ec2.AmazonEC2;
import com.amazonaws.services.ec2.AmazonEC2ClientBuilder;
import com.amazonaws.services.ec2.model.InstanceState;
import com.amazonaws.services.ec2.model.InstanceStateChange;
import com.amazonaws.services.ec2.model.StartInstancesRequest;
import com.amazonaws.services.ec2.model.StartInstancesResult;
import com.amazonaws.services.ec2.model.StopInstancesRequest;
import com.amazonaws.services.ec2.model.StopInstancesResult;

/**
 * This class is a webservice for starting AWS team machine instances.
 * It provides a REST API endpoint "/startmachine" which receives a team number,
 * a password, and a "runtime" value; it verifies that the specified password is
 * correct for the specified team and if so it sends commands to AWS to start the
 * machine corresponding to the specified team.
 *
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
@Path("/startmachine")
@Singleton
public class StartMachineService {

	private static final int MAX_ALLOWED_RUNTIME_MINS = 180; //3 hours max

	private Log log;

	private EntityManagerFactory entityManagerFactory;
	private EntityManager entityManager;
	private MachineDAO machineDAO;

  @Inject
	public StartMachineService(Log log, EntityManagerFactory emf) {
		super();
		this.log = log;
		this.entityManagerFactory = emf;
	    this.entityManager = entityManagerFactory.createEntityManager();
	    this.machineDAO = new MachineDAO(entityManager);
	}

	/**
	 * This method is invoked when a team wishes to self-start their cloud VM.
	 *
	 * @param sc the {@link SecurityContext} in which the method runs.
	 * @param request the {@link HttpServletRequest} whose Request Body contains the operation to be performed.
	 * @param response an {@link HttpServletResponse} to be returned to the invoker.
	 * @param machineId a long identifying which machine is to be started; the id must match one of the machineIds
	 * 			in the database.
	 * @param password a String giving the password which the user will ultimately use to login to the machine.
	 * @param runtime an int giving the desired amount of time which the machine should remain running, in minutes.
	 * @return an {@link HttpResponse}
	 * @throws IOException
	 */
	@POST
    @Produces("text/plain")
	public Response startMachine(@Context SecurityContext sc, @Context HttpServletRequest request,
			@Context HttpServletResponse response, @FormParam("machineId") long machineId,
			@FormParam("password") String password, @FormParam("runtime") int runtime) throws IOException {

		boolean success = false;
		Machine targetMachine = null ;
		String awsInstanceId ;
		
		boolean enforceAutoShutoff = WebServer.getProperty("enforceAutoShutoff", "true").equalsIgnoreCase("true");
		
		try {

			log.info("StartMachine Service invoked for machineId " + machineId + ", requested runtime = " + runtime);

			//get the description of the machine we're asked to start
			targetMachine = machineDAO.get(machineId).orElse(null);

			// make sure the specified machine exists
			if (targetMachine == null) {
				log.warning("Unable to find machine " + machineId);
				return Response.status(Status.BAD_REQUEST).entity("No such machine").build();
			}

			// make sure we got a non-empty password
			if (password == null || password.equals("")) {
				log.warning("Null or empty password; cannot continue");
				return Response.status(Status.BAD_REQUEST).entity("Null or empty password received").build();
			}

			// Verify the password is valid for some login on this machine
			if (!targetMachine.checkPassword(password)) {
				log.warning("Invalid password");
				return Response.status(Status.UNAUTHORIZED).entity("Invalid password").build();
			}

			// make sure the runtime is legit
			if (enforceAutoShutoff && (runtime < 1 || runtime > MAX_ALLOWED_RUNTIME_MINS) ) {

				log.warning("Requested runtime not in valid range (1..180)");
				return Response.status(Status.BAD_REQUEST).entity("Requested runtime not in valid range (1..180)")
						.build();
			}

			// get the AWS instance id for the machine
			awsInstanceId = targetMachine.getEc2Id();

			if (awsInstanceId==null || awsInstanceId.contentEquals("")) {
				log.warning("Requested machine has no AWS InstanceId; cannot start it");
				return Response.status(Status.BAD_REQUEST)
						.entity("Requested machine has no AWS InstanceId; cannot start it")
						.build();
			}

			success = startAWSInstance(awsInstanceId);

		} catch (Exception e) {
			return Response.status(Status.INTERNAL_SERVER_ERROR)
					.entity("Exception processing StartMachine request: " + e.getMessage()).build();

		}

		if (success) {
			
			if (enforceAutoShutoff) {
				log.info("Scheduling StopInstance Task to stop machine " + awsInstanceId + " after " + runtime + " minutes");
				//schedule an autostop task for the machine
				scheduleStopInstanceTask(targetMachine.getEc2Id(), runtime);
			}
			
			String name = targetMachine.getName();
			
			try {
				//update the machine self-start parameters
				log.info("Setting autoshutoff=" + enforceAutoShutoff + ", runtime=" + runtime + ", starttime=now, stoptime=null,"
						+ " running=true in database for InstanceId "+ awsInstanceId + " (" + name + ")"); 
				targetMachine.setAutoShutoff(enforceAutoShutoff);
				targetMachine.setRuntime(runtime);
				targetMachine.setSelfStartStartTime(new Date());
				targetMachine.setSelfStartStopTime(null);
				targetMachine.incrementSelfStartCounter();
				targetMachine.setIsRunning(true);
				machineDAO.save(targetMachine);
			} catch (Exception e) {
				log.severe("Exception trying to update database with self-start params (autoShutoff/runtime/startTime/stopTime) "
							+ "after starting AWS machine " + awsInstanceId + " (" + name + ") : " + e.getMessage()); 
			}
			
			return Response.status(Status.OK).entity("Machine Started").build();
			
		} else {
			return Response.status(Status.SERVICE_UNAVAILABLE).entity("Unable to start cloud machine").build();
		}

	}

	/**
	 * Invokes the AWS API to start the machine instance with the specified instanceId.
	 *
	 * @param awsInstanceId the AWS Instance Id for the machine to be started.
	 * @return true if AWS returned an indication that the state of the instance is either "pending" or "running";
	 * 			false otherwise.
	 */
	private boolean startAWSInstance(String awsInstanceId) {

		try {

			log.info("Invoking AWS to start instanceId " + awsInstanceId);
			
			final AmazonEC2 ec2 = AmazonEC2ClientBuilder.defaultClient();

			//TODO:  check whether the specified instance is ALREADY RUNNING
			
			StartInstancesRequest request = new StartInstancesRequest()
					.withInstanceIds(awsInstanceId);

			StartInstancesResult result = ec2.startInstances(request);

			List<InstanceStateChange> instanceStates = result.getStartingInstances();

			if (instanceStates==null || instanceStates.size()==0) {
				log.severe("Null or empty AWS InstanceState list returned while starting machine instance " + awsInstanceId);
				return false;
			}

			InstanceState state = instanceStates.get(0).getCurrentState();

			if (state==null) {
				log.severe("Null AWS Instance State returned while starting machine instance " + awsInstanceId);
				return false;
			}

			String curState = state.getName();

			if (curState!=null && (curState.contentEquals("pending")||curState.contentEquals("running")) ) {
				log.info("Started AWS Instance " + awsInstanceId);
				return true;
			} else {
				log.severe("AWS returned Instance State other than 'pending' or 'running' while starting machine instance " + awsInstanceId);
				return false;
			}

		} catch (AmazonServiceException e) {
			log.severe("AWS Exception starting machine instance " + awsInstanceId +  ": " + e.getMessage());
			return false;
		} catch (Exception e) {
			log.severe("Exception starting machine instance " + awsInstanceId + ": " + e.getMessage());
			return false;
		}
	}

	/**
	 * Constructs a {@link TimerTask} which will execute after the specified runtime has elapsed
	 * and at that time will stop the specified AWS Instance.
	 * 
	 * Note that runtime is expected to be in MINUTES.
	 * 
	 * @param awsInstanceId the AWS instance to be stopped after "runtime" minutes have elapsed
	 * @param runtime the number of minutes to delay before stopping the AWS instance
	 */
	private void scheduleStopInstanceTask(String awsInstanceId, int runtime) {

		//define the task we want performed
	    TimerTask task = new TimerTask() {
	        public void run() {
	            
	        	log.info("Stopping AWS instance " + awsInstanceId);
	        	
	        	//cancel the timer task so it doesn't execute again (any new self-start will get a NEW timer task)
	        	this.cancel();
	        	
	        	final AmazonEC2 ec2 = AmazonEC2ClientBuilder.defaultClient();

	        	StopInstancesRequest stopRequest = new StopInstancesRequest().withInstanceIds(awsInstanceId);

	        	StopInstancesResult stopResult = ec2.stopInstances(stopRequest);
	        	
				List<InstanceStateChange> instanceStates = stopResult.getStoppingInstances();

				if (instanceStates==null || instanceStates.size()==0) {
					log.severe("Null or empty AWS InstanceState list returned while stopping machine instance");
					return;
				}

				InstanceState state = instanceStates.get(0).getCurrentState();

				if (state==null) {
					log.severe("Null AWS Instance State returned while stopping machine instance " + awsInstanceId);
					return;
				}

				String curState = state.getName();

				if (curState!=null && (curState.contentEquals("shutting-down") ||
										curState.contentEquals("stopping") ||
										curState.contentEquals("stopped")
									  ) ) {
					log.info("Stopped AWS Instance " + awsInstanceId);
					
					//update the machine self-start parameters
		        	Machine targetMachine = machineDAO.findByInstanceId(awsInstanceId);
		        	
					if (targetMachine!=null) {
						String name = targetMachine.getName();
						log.info("Setting autoshutoff=false, connected=false, stoptime=now, running=false in database for InstanceId " 
									+ awsInstanceId + "(" + name + ")"); 
						targetMachine.setAutoShutoff(false);
						targetMachine.setConnected(false);
						targetMachine.setSelfStartStopTime(new Date());
						targetMachine.setIsRunning(false);
						machineDAO.save(targetMachine);
						
					} else {
						log.severe("Unable to find InstanceId " + awsInstanceId + " in database; cannot update settings");
					}
					
				} else {
					log.severe("AWS returned Instance State other than 'shutting-down' or 'stopping' "
							+ " or 'stopped' while stopping machine instance " + awsInstanceId);
				}
	        	
				return;
	        	
	        }

	    };
	    
	    //create a Timer with which to schedule the task
	    Timer timer = new Timer("Timer-" + awsInstanceId);
	    
	    //compute the number of milliseconds to delay before executing the task
	    long delay = runtime*60*1000L;  //minutes X 60secs/min X 1000 msec/sec
	    
	    //schedule the task for execution after "delay" msec
	    timer.schedule(task, delay);
	    
	    log.info("StopInstance Task scheduled to stop instance " + awsInstanceId + " after " + runtime + " minutes");
	}

	//this method exists for JUnit testing
	public static void main(String[] args) {

	}

}
