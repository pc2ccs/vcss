package org.icpc.vcss.webserver.services.pub;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.glassfish.jersey.server.mvc.Viewable;
import org.icpc.vcss.core.log.Log;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineDAO;


	@Path("/")
  public class PublicService {

	private EntityManagerFactory entityManagerFactory;
	private MachineDAO machineDAO;

	@Inject
	public PublicService(Log log, EntityManagerFactory emf) {
		super();

    this.entityManagerFactory = emf;
		EntityManager em = entityManagerFactory.createEntityManager();
		this.machineDAO = new MachineDAO(em);
	}

  @GET
	public Viewable selfStart() {

		return new Viewable("/publicindex.jsp");
	}
}
