package org.icpc.vcss.webserver.services.pub;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

import org.glassfish.jersey.server.mvc.Viewable;
import org.icpc.vcss.core.log.Log;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineDAO;
import org.icpc.vcss.webserver.WebServer;


  @Path("/cloudmachine")
  public class ControlCloudMachineService {

	private EntityManagerFactory entityManagerFactory;
	private MachineDAO machineDAO;

	@Inject
	public ControlCloudMachineService(Log log, EntityManagerFactory emf) {
		super();

    this.entityManagerFactory = emf;
		EntityManager em = entityManagerFactory.createEntityManager();
		this.machineDAO = new MachineDAO(em);
	}

    @GET
	public Viewable selfStart() {

		List<Machine> cloudMachines = machineDAO.getCloudMachines();

		Map<String, Object> model = new HashMap<>();
		model.put("cloudMachines", cloudMachines);
		model.put("enforceAutoShutoff", WebServer.getProperty("enforceAutoShutoff", "true"));

		return new Viewable("/controlcloudmachine.jsp", model);
	}
}
