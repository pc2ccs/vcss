package org.icpc.vcss.webserver.services;

import java.io.PrintWriter;
import java.io.StringWriter;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class UncaughtException extends Throwable implements ExceptionMapper<Throwable>
{
    private static final long serialVersionUID = 1L;

    @Override
    public Response toResponse(Throwable exception) {
      Response response;
      if (exception instanceof WebApplicationException) {
        WebApplicationException webEx = (WebApplicationException)exception;
        response = webEx.getResponse();
      } else {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        exception.printStackTrace(pw);
        String msg = "Internal error:\n" + sw.toString();
        response = Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(msg).type("text/plain").build();
      }
      return response;
    }
}
