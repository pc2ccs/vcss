// Copyright (C) 1989-2019 PC2 Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.webserver;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.websocket.CloseReason;
import javax.websocket.CloseReason.CloseCodes;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.PongMessage;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.icpc.vcss.core.log.Log;
import org.icpc.vcss.entity.Machine;
import org.icpc.vcss.entity.MachineDAO;
import org.icpc.vcss.webserver.websocket.User;
import org.icpc.vcss.websocketMessage.WebsocketMessage;
import org.icpc.vcss.websocketMessage.WebsocketMessageJSONDecoder;
import org.icpc.vcss.websocketMessage.WebsocketMessageJSONEncoder;

/**
 * WebService to handle requests for establishing websockets between the VCSS web server and client machines.
 *
 * @author John Clevenger (pc2@ecs.csus.edu)
 *
 */
@ServerEndpoint(
	value = "/websocket",
  configurator = WSCustomConfigurator.class,
  encoders = WebsocketMessageJSONEncoder.class,
  decoders = WebsocketMessageJSONDecoder.class
	)
@Singleton
public class WSServerEndpoint extends Endpoint {

	//a mapping from UUID (as String) to Websocket Session ID (a String).  Allows others to obtain a websocket's
	// sessionID knowing the client's machine UUID.
    private static Map<String,String> clientUUIDtoSessionIDMap = Collections.synchronizedMap(new HashMap<String, String>());

    //a mapping from Websocket SessionID (a String) to WebsocketUser, a class encapsulating information about a client.
    // Allows others to obtain client (User) information knowing the client's Websocket SessionID.
    private static Map<String, User> connections = Collections.synchronizedMap(new HashMap<String, User>());

    private static WSServerEndpoint self = null;
    public WSServerEndpoint() {
      if (WSServerEndpoint.self == null) {
        WSServerEndpoint.self = this;
      }
    }

    @Inject
    private EntityManagerFactory emf;

    @Inject
    private Log log;

    //Rather than use a single injected instance (which is only used in onClose()), we now create a new
    // instance for every onClose() call in order to avoid multi-threading problems.
//    @Inject
//  	private MachineDAO machineDAO;


    @OnOpen
    public void onOpen(Session session) {

    	try {

	        log.info("WSServerEndpoint onOpen() invoked, new connected Session ID = " + session.getId());
	
	        //save the new client session
	        if (connections == null) {
	        	connections = Collections.synchronizedMap(new HashMap<String, User>());
	        }
	        
	        // from https://stackoverflow.com/questions/56914261/android-and-java-ping-pong-web-socket-functionality
	        User newUserConnection = new User(session);
	        connections.put(session.getId(), newUserConnection);
	        schedulePingMessages(newUserConnection);
	        
		} catch (Exception e) {
			log.log(Level.WARNING, "Problem onOpen session, session="+session, e);
		}
    }

    /**
     * This method is invoked whenever a client sends a message through its websocket.
     * The message is expected to be a {@link WebsocketMessage} in JSON format.
     *
     * @param message a {@link WebsocketMessage} object containing the message from the client (currently, a String is used).
     * @param session the session for the client that sent the message.
     */
    @OnMessage
    public void onMessage(WebsocketMessage message, Session session) {

    	log.info("Processing client message for session ID " + session.getId());

      if (message.requireTransaction()) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
          message.handleMessage(session);
          em.getTransaction().commit();
        } catch (Exception e) {
          e.printStackTrace();
          e.getCause().printStackTrace();
          // Rollback if there were any uncaught/unexpected exceptions
          em.getTransaction().rollback();
        }
      } else {
        // Note, error handling, try/catch done in classes implementing handleMessage
        message.handleMessage(session);
      }
    }

	@OnMessage
    public void onPong(PongMessage pongMessage, Session session) {

		try {

        String sourceSessionId = session.getId();
        User user = connections.get(sourceSessionId);
        user.disconnectTimer.cancel();
        user.disconnectTimer.purge();

		} catch (Exception e) {
			log.log(Level.WARNING, "Problem in onPong",  e);
		}
    }

    private void scheduleDisconnection(User user) {
        user.disconnectTimer = new Timer();
        user.disconnectTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                try {
                    user.getUserSession().close(new CloseReason(CloseCodes.UNEXPECTED_CONDITION," Client does not respond"));
                } catch (IOException e) {
                	getLog().log(Log.WARNING, "Disconnecting user "+user+" " + e.getMessage(), e);
                }
            }
        }, 5000);
    }

    private void schedulePingMessages(User newUserConnection) {
        newUserConnection.pingExecutorService = Executors.newScheduledThreadPool(1);
        newUserConnection.pingExecutorService.scheduleAtFixedRate(() -> {
            scheduleDisconnection(newUserConnection);
            try {
                String data = "Ping";
                ByteBuffer payload = ByteBuffer.wrap(data.getBytes());
                newUserConnection.getUserSession().getBasicRemote().sendPing(payload);
            } catch (IOException e) {
            	getLog().log(Log.WARNING, "Pinging user "+newUserConnection+" " + e.getMessage(), e);
            }
        }, 50, 50, TimeUnit.SECONDS);
    }

	@OnClose
    public void onClose(Session session, CloseReason closeReason) {

        try {
			log.info(String.format("Session %s closed because of %s", session.getId(), closeReason));
			
			log.info("Creating new MachineDAO object");
			MachineDAO machineDAO = new MachineDAO(emf.createEntityManager());

			User user = connections.get(session.getId());
			// Nothing to do if there's no user associated
			if (user == null) {
				log.warning("onClose() called for session " + session + " with CloseReason " + closeReason 
								+ " but no corresponding User connection could be found");
				return;
			}

			Machine m = machineDAO.findByUuid(UUID.fromString(user.getMachineUUIDAsString()));
			// Nothing to do if there's no machine associated
			if (m == null) {
				log.warning("onClose() called for session " + session + " with CloseReason " + closeReason 
						+ " but no corresponding machine could be found");
				return;
			}

			// machineDAO.ensureTransction();
			// m = machineDAO.lock(m);
			// Mark the machine as disconnected in the database
			log.info("Attempting to mark machine " + m.getUuid() + " as disconnected");
			log.info(" (Machine m is currently marked as connected = " + m.getConnected() + ")");
			m.setConnected(false);
			log.info(" (Machine m is now marked as connected = " + m.getConnected() + ")");
			log.info("Updating machine m in database");
			machineDAO.save(m);
			// machineDAO.commitTransaction();
			log.info(String.format("Machine %s marked as disconnected", m.getUuid()));
			
			log.info("Removing session " + session.getId() + " from clientUUIDtoSessionIDMap");
			clientUUIDtoSessionIDMap.remove(user.getMachineUUIDAsString());
			
			log.info("Removing User for session " + session.getId() + " from connections table");
			connections.remove(session.getId());
			
		} catch (Exception e) {
			log.log(Level.WARNING, "Exception in onClose() for session " + session, e);
		}
    }

    @OnError
    public void onError(Session session, Throwable t) {

    	try {
	    	log.warning(String.format("Error in Session %s: Exception message = %s\n", session.getId(), t.getMessage()));
	        log.warning(String.format("Error in Session %s caused by %s\n", session.getId(), t.getCause().getMessage()));
	
	        //attempt to notify the client
	        //log.info("Attempting to notify client of error");
	        //session.getAsyncRemote().sendText("Error: " + t.getMessage());
	        //session.getBasicRemote().sendText("Error: " + t.getMessage());
	        
    	} catch (Exception e) {
	    	log.warning(String.format("Exception in onError() for Session %s while attempting to notify client: %s\n", 
	    								session.getId(), e.getMessage()));
      }
    }

    /**
     * Returns the clientUUIDtoSessionIDMap for currently connected clients.
     * Callers should take care to check {@link Session#isOpen()} to insure that a given client connection (Session) is still active.
     * @return a {@link Set<Session>} of client websocket sessions.
     */
    public static Map<String, String> getClientUUIDtoSessionIDMap() {
    	return clientUUIDtoSessionIDMap;
    }

    /**
     * Returns a map of current connections (specifically, a Map from Websocket SessionID to the WebsocketUser
     * currently associated with that connection).
     *
     * @return a Map<String,WebsocketUser> where "String" is a Websocket SessionID.
     */
    public static Map<String,User> getConnections() {
    	return connections;
    }

    /**
     * Returns the {@link EntityManagerFactory} associated with the VCSS server.
     */
    public static EntityManagerFactory getCurrentDatabase() {
    	return self.emf;
    }

    /**
     * Returns the {@link Log} being used by the server.
     *
     * @return the server's {@link Log}.
     */
    public static Log getLog() {
      if (self == null) return null;
    return self.log;
  }

  @Override
  public void onOpen(Session arg0, EndpointConfig arg1) {
    // TODO Auto-generated method stub

    }

}
