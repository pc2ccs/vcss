package org.icpc.vcss.webserver;

import javax.persistence.EntityManagerFactory;

import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.icpc.vcss.core.log.Log;
import org.icpc.vcss.database.DatabaseUtilities;
import org.icpc.vcss.entity.CommandDAO;
import org.icpc.vcss.entity.MachineCommandResultDAO;
import org.icpc.vcss.entity.MachineDAO;

public class VCSSBinder extends AbstractBinder {
  @Override
  protected void configure() {
      EntityManagerFactory emf = DatabaseUtilities.getCurrentDatabase();
      bind(emf).to(EntityManagerFactory.class);
      bind(new MachineDAO(emf.createEntityManager())).to(MachineDAO.class);
      bind(new CommandDAO(emf.createEntityManager())).to(CommandDAO.class);
      bind(new MachineCommandResultDAO(emf.createEntityManager())).to(MachineCommandResultDAO.class);
      bind(WebServer.getLog()).to(Log.class);
  }
}
