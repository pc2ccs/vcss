package org.icpc.vcss.webserver;

import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;
import org.glassfish.jersey.server.mvc.jsp.JspMvcFeature;

public class VCSSPublicApplication extends ResourceConfig {
  public VCSSPublicApplication() {
      // Configure jackson mvc so it knows where our jsp files are
      register(JspMvcFeature.class);
      property(JspMvcFeature.TEMPLATE_BASE_PATH, "/WEB-INF/jsps");

      // Scan this package(and subpackages) for jax-rs annotations
      packages(true, "org.icpc.vcss.webserver.services.pub");

      // Add our binder, which provides our classes with the dependency injection they need
      register(new VCSSBinder());

      // These were in the old ResourceConfig bit, unsure if we actually need these or not
      register(RolesAllowedDynamicFeature.class);
      register(JacksonFeature.class);

  }
}
