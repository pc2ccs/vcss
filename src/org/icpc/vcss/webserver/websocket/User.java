package org.icpc.vcss.webserver.websocket;

import java.util.Timer;
import java.util.concurrent.ScheduledExecutorService;

import javax.websocket.Session;

/**
 * This class encapsulates a Websocket client -- that is, a client machine which is connected
 * to the VCSS system via a WebSocket.
 * 
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
public class User {
	
	public ScheduledExecutorService pingExecutorService;
	public Timer disconnectTimer;
	private Session userSession;
	private String userName;
	private String machineUUIDAsString;

	public User(Session userSession) {
		this.userSession = userSession;
		//use SessionID as default name
		this.userName = userSession.getId();
	}

	public Session getUserSession() {
		return userSession;
	}

	public void setUserSession(Session userSession) {
		this.userSession = userSession;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getMachineUUIDAsString() {
		return machineUUIDAsString;
	}

	public void setMachineUUIDAsString(String machineUUID) {
		this.machineUUIDAsString = machineUUID;
	}
}
