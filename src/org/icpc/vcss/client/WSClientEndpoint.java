// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.client;

import java.io.IOException;
import java.util.concurrent.CountDownLatch;
import java.util.logging.Level;

import javax.websocket.ClientEndpoint;
import javax.websocket.CloseReason;
import javax.websocket.EncodeException;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;

import org.icpc.vcss.core.log.Log;
import org.icpc.vcss.websocketMessage.WebsocketMessage;
import org.icpc.vcss.websocketMessage.WebsocketMessageJSONDecoder;
import org.icpc.vcss.websocketMessage.WebsocketMessageJSONEncoder;
import org.icpc.vcss.websocketMessage.client.WSRegistrationRequestMsg;

/**
 * This class represents the Client Endpoint of a WebSocket connection. Client
 * endpoints reside on VCSS client machines -- for example, teams and possibly
 * judges, admins, and other machine types.
 * 
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
@ClientEndpoint(encoders = WebsocketMessageJSONEncoder.class, decoders = WebsocketMessageJSONDecoder.class)
public class WSClientEndpoint {

	private static CountDownLatch socketClosedLatch = new CountDownLatch(1);
	
	//a latch used to block threads until successful registration with the VCSS server has been accomplished
	private static CountDownLatch registrationLatch = new CountDownLatch(1);

	private Session session;
	
	private Log log ;

	@OnOpen
	public void onOpen(Session session) {

        this.session = session;
        this.log = RunVCSSClient.getLog();
        
		System.out.println("WebsocketClientEndpoint onOpen() called...");
		log.info("Client connected: " + session.getId());
	}

	@OnMessage
	public void onMessage(WebsocketMessage message, Session session) {

		System.out.println("WSClientEndpoint.onMessage(): processing message for session ID " + session.getId() );
		log.info("Processing message for session ID " + session.getId() );
		
		message.handleMessage(session);
		
	}


	@OnClose
	public void onClose(Session session, CloseReason closeReason) {

		System.out.println("WebsocketClientEndpoint onClose() called for Session " + session.getId()
				+ " with Close Reason " + closeReason);
		log.info("WebsocketClientEndpoint onClose() called for Session " + session.getId()
				+ " with Close Reason " + closeReason);

		socketClosedLatch.countDown();

	}
	
	@OnError
	public void onError(Session session, Throwable t) {
		t.printStackTrace();
		t.getCause().printStackTrace();
		
		String sessionID = "(unknown)";
		if (session!=null) {
			sessionID = session.getId();
		}
		String throwableMsg = "(unknown)";
		if (t!=null) {
			throwableMsg = t.getMessage();
		}
		System.out.println("WebsocketClientEndpoint onError() called for Session " + sessionID + " with Throwable message " + throwableMsg);
		log.info("WebsocketClientEndpoint onError() called for Session " + sessionID + " with Throwable message " + throwableMsg);

	}
	
	/**
	 * Builds a "Registration Request" WebsocketMessage and sends it to the VCSS server.
	 * @param uuid a String containing the UUID for this machine, to be inserted into the registration message.
	 * @param hostname a String containing the fully-qualified hostname for this machine, to be inserted into the registration message.
	 */
	public void sendRegistrationRequestToServer(String uuid, String hostname) {
		
		try {


			String logStr = "WSClientEndpoint sendRegistrationRequestToServer() invoked ";
			if (session==null) {
				logStr += "but Session is null";
			} else {
				logStr += "for Session " + session.getId();
				if (uuid==null || uuid.length()==0) {
					logStr += " with null or empty uuid string";
				} else {
					logStr += " with UUID string '" + uuid + "'";
					if (hostname==null || hostname.length()==0) {
						logStr += " and null or empty hostname string";
					} else {
						logStr += "and hostname string '" + hostname + "'";
					}
				}
			}
			System.out.println(logStr);
			log.info(logStr);

			if (uuid==null || uuid.equals("") || hostname==null || hostname.equals("")) {
				System.out.println("WARNING: sending registration request to server with null or empty uuid or hostname");
				log.warning("Sending registration request to server with null or empty uuid or hostname");
			}

			WSRegistrationRequestMsg regReqMsg = new WSRegistrationRequestMsg(uuid, hostname);

			sendMessageToServer(regReqMsg);


		} catch (Exception e) {
			log.log(Level.WARNING, "Sending registration request to server with null or empty uuid or hostname", e);
		}
	}
	
	/**
	 * This method sends the specified message through the websocket to the server.
	 * 
	 * @param regReqMsg the {@link WSRegistrationRequestMsg} to be sent.
	 * 
	 */
    public void sendMessageToServer(WSRegistrationRequestMsg regReqMsg) {

		System.out.println("WSClientEndpoint.sendMessageToServer() called for session " + session.getId() + " with message '" + regReqMsg + "'");
		log.info("WebsocketClientEndpoint sendMessageToServer() called for Session " + session.getId() + " with message '" + regReqMsg + "'");

		try {
            if (session!=null) {
            	
				session.getBasicRemote().sendObject(regReqMsg);
				
			} else {
				log.warning("WSClientEndpoint.sendMessageToServer() invoked with no open Session");
				//TODO: find a better way to handle this. Throwing a RuntimeException will terminate the JVM
				//  (which may be acceptable since it is the CLIENT JVM being terminated, but it seems like this is
				//  a bit too brute-force...)
				throw new RuntimeException("WSClientEndpoint.sendMessageToServer() called with no open Session");
			}
        } catch (IOException | EncodeException e) {
    		log.log(Level.SEVERE, "WSClientEndpoint.sendMessageToServer(): exception sending message to server: " + e.getMessage(), e);
        }
    }


	/**
	 * Returns a reference to the "socket closed" CountdownLatch, which is initialized to one and
	 * then decremented to zero when the socket is closed.
	 * 
	 * @return the {@link CountDownLatch} used to indicate that the socket has been closed.
	 */
	public CountDownLatch getSocketClosedLatch() {
		return socketClosedLatch;
	}
	
	/**
	 * Returns a reference to the "registration latch", which is initialized to one and
	 * then decremented to zero when the client machine has successfully "registered" with
	 * the VCSS server.
	 * @return the Registration {@link CountDownLatch}.
	 */
	public static CountDownLatch getRegistrationLatch() {
		return registrationLatch;
	}

}