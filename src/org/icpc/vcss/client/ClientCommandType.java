package org.icpc.vcss.client;

/**
 * This enum defines the types of commands which can be sent to client machines.
 * Note that this is different from the types of *Websocket Messages* which can
 * be sent to a client; this enum defines commands which can be executed on client 
 * machines.
 * 
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
public enum ClientCommandType {

    /**
     * Indicates an unknown command type.
     */
    UNDEFINED("undefined"),
    
    /**
     * Command which causes the client to execute a given command line.
     */
    EXECUTE("execute"),
    
    /**
     * Command which tells the client to accept and store a file.
     */
    PUT("put"),
    
    /**
     * Command which requests the client to return a file.
     */
    GET("get");
    

    private String textName = "undefined";
    
    
    /**
     * Set override text name/description for enum element during element construction.
     */
    ClientCommandType(String textName) {
        this.textName = textName;
    }
    
    /**
     * Get an enum element based on its textName.
     */
    public static ClientCommandType fromString(String text) {
        for (ClientCommandType b : ClientCommandType.values()) {
            if (b.textName.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }
    
    @Override
    public String toString() {
        return textName;
    }

}
