// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
/**
 * Classes related to VCSS client machines -- that is, machines such as teams, judges, and so forth which
 * are participating in a virtual contest being managed by VCSS.
 */
package org.icpc.vcss.client;