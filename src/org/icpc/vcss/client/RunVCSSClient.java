// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.client;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.UnknownHostException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Properties;
import java.util.Set;
import java.util.UUID;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.websocket.DeploymentException;
import javax.websocket.Session;

import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.glassfish.tyrus.client.ClientManager;
import org.glassfish.tyrus.client.ClientProperties;
import org.glassfish.tyrus.client.SslEngineConfigurator;
import org.glassfish.tyrus.client.auth.Credentials;
import org.icpc.vcss.core.Constants;
import org.icpc.vcss.core.Utilities;
import org.icpc.vcss.core.log.Log;
import org.icpc.vcss.core.log.LogFormatter;
import org.icpc.vcss.util.UUIDValidator;

/**
 * This class is the main driver for the Client-side operations of the VCSS system.  It establishes a client-side
 * websocket which connects to the VCSS server at the address specified by the input command line argument.  
 * The websocket registers with the VCSS server and then accepts and processes commands from the server.
 * 
 * @author John Clevenger, ICPC Development Team (pc2@ecs.csus.edu)
 *
 */
public class RunVCSSClient {
	
	private static Log log = null;
	
	private WSClientEndpoint clientEndpoint = null;
	
	private Properties vcssProperties = new Properties();
	
	private ConsoleHandler consoleHandler = new ConsoleHandler();
	
	private static final long CONNECTION_IS_OPEN_POLLING_DELAY_MS = 5000;
	private static final int MAX_ALLOWED_RECONNECTION_ATTEMPTS = 5;  //TODO: this value was for testing; change me to something larger
	
	private static final int EXITCODE_MISSING_ARGUMENTS = 1;
	private static final int EXITCODE_INVALID_ARGUMENT = 2;
	private static final int EXITCODE_MAX_ALLOWED_RECONNECTION_ATTEMPTS_REACHED = 3;

	
	private static long delayMsec = 2000L;
	private static int reconnectionCount = 0;

    public RunVCSSClient() {
    	super();
    }
    
	/**
	 * Remove log console handler
	 */
	private void removeConsoleHandler() {
		if (consoleHandler != null) {
			log.removeHandler(consoleHandler);
		}
		consoleHandler = null;
	}
    /**
     * This method starts RunVCSSClient executing on the client machine.  It expects a single argument: 
     * an array whose first element contains the URL of the VCSS server which is to be contacted.
     * The method starts a separate thread which connects to the VCSS server and then
     * sends to the server a "registration request" message. Responses to messages
     * are handled by an instance of {@link WSClientEndpoint} which is connected to the
     * server.
     * 
	 * @param args[0] - the URL to which this class should establish a websocket.
     */
    private void start(String [] args) {
    	
    	//make sure we were passed the correct number of arguments
		if (args==null || args.length<1) {
			usage();
			System.exit(EXITCODE_MISSING_ARGUMENTS);
		}

		//make sure the arguments make sense (arg0 is a URL)
		boolean argsOK = validateArgs(args);
		if (!argsOK) {
			System.err.println ("Invalid arguments");
			usage();
			System.exit(EXITCODE_INVALID_ARGUMENT);
		}
		
		log = new Log("vcss-client.log");
		consoleHandler.setLevel(Level.ALL);
		consoleHandler.setFormatter(new LogFormatter(true)); // set console log line format, thread, date, etc.
		log.addHandler(consoleHandler);  // by default shows log entries to console during startup
		
    	log.info("Starting VCSS client");

		if (Utilities.fileExists(Constants.VCSS_PROPERTIES_FILENAME)) {

			showMessage("Found VCSS properties file " + Constants.VCSS_PROPERTIES_FILENAME + "; loading...");
			vcssProperties = loadPropertiesFile(Constants.VCSS_PROPERTIES_FILENAME);
			String propertiesWithoutPasswords = filterPasswords(vcssProperties);
			showMessage("Loaded " + propertiesWithoutPasswords);


		}
		
		boolean logToConsole = Utilities.getBooleanProperty(vcssProperties, Constants.LOGGER_CONSOLE_KEY, false);
		if (! logToConsole) {
			// by default console logging is turned ON to show startup log messages
			// at this point the console logging property says turn logging off.
			
			log.info("Console logging turned off");
			removeConsoleHandler();
		}

//    	WebSocketContainer container = ContainerProvider.getWebSocketContainer();

        try {

	    	URI uri = new URI("wss://" + args[0] + "/websocket") ;

	    	log.info("Connecting to VCSS server at " + uri);
            
	    	//create a separate thread which will join the contest
	    	Thread clientThread = new Thread("Client connection thread") {
	    		@Override
	    		public void run() {
	    			while (true) {
	    				
	    				//connect to the server and register; this method only returns if the connection to the 
	    				//contest is lost (in which case we wait a short while and try to rejoin...)
	    				joinContest(uri);
	    				
	    				//if we get here then the connection to the server was lost (or failed in the first place)
	    				try {
	    					if (reconnectionCount > MAX_ALLOWED_RECONNECTION_ATTEMPTS) {
	    						
								System.out.println("Maximum allowed connection attempts exceeded; giving up...");
								log.severe("Maximum allowed connection attempts exceeded; giving up...");
								System.exit(EXITCODE_MAX_ALLOWED_RECONNECTION_ATTEMPTS_REACHED);
	    						
	    					} else {
	    						//add a short delay so we don't burn up the CPU
								sleep(getReconnectionDelayMs());
								System.out.println("Connection to server lost; attempting to reconnect...");
								log.warning("Connection to server lost; attempting to reconnect...");
							}
	    				} catch (InterruptedException e) {
	    					// ignore
	    				}
	    			}
	    		}
	    	};
	    	clientThread.start();

        } catch (URISyntaxException  e) {
        	log.log(Level.SEVERE,"Exception in websocket handling: " + e.getMessage(), e);
            throw new RuntimeException(e);
			
//		} finally {
//			// Force lifecycle stop when done with container. This is to free up threads and resources that the
//			// JSR-356 container allocates, since unfortunately the JSR-356 spec does not handle lifecycles (yet)
//			LifeCycle.stop(container);
		} 
	}

    /**
     * This method returns the number of milliseconds which should elapse before a websocket reconnection is tried.
     * The method applies a rudimentary algorithm to increase the delay each time it is called.
     * 
     * @return the reconnection delay in msec.
     */
    protected long getReconnectionDelayMs() {
		
    	reconnectionCount++;
    	
    	float increaseFactor = 0.1f*reconnectionCount;
    	
    	delayMsec += delayMsec*increaseFactor ;

		return delayMsec;
	}

	/**
     * This method establishes a connection to the VCSS server, resulting in a websocket between the client
     * and the server which is handled on the client end by an instance of {@link WSClientEndpoint}.
     * It then sends a RegistrationRequest message to the server through the websocket.  (The server is expected to respond with
     * a "RegistrationResults" message, which will be handled by the onMessage() method of the client endpoint class.)
     * 
     * Once the server has been contacted, the websocket has been established, and the registration request has been sent,
     * the method loops while checking periodically that the websocket session is still open;
     * the method returns only if the websocket session is closed.  (It is expected that the caller will then apply
     * "retry logic" to call this method again to rejoin the contest.)
     * 
     * @param uri the URI for the server which is to be contacted.
     * 
     */
    private void joinContest(URI uri)  {
    	
    	Session session ;
    	
		try {

			SslContextFactory sslContextFactory = new SslContextFactory.Server();
			sslContextFactory.setValidateCerts(false);
			sslContextFactory.setTrustAll(true);

			TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
				public java.security.cert.X509Certificate[] getAcceptedIssuers() {
					return null;
				}

				public void checkClientTrusted(X509Certificate[] certs, String authType) {
				}

				public void checkServerTrusted(X509Certificate[] certs, String authType) throws CertificateException {
					boolean validCert = false;
					for (int i = 0; i < certs.length; i++) {
						X509Certificate cert = certs[i];
						if (cert.getSubjectDN().toString().equals(Constants.SUBJECT_DN)) {
							validCert = true;
							break;
						}
						if (cert.getSubjectDN().toString().equals("CN=*.icpc-vcss.org")) {
							cert.checkValidity(); // checks the notBefore and notAfter dates
							// need to check the other cert too
							if (i < certs.length) {
								cert =  certs[i+1];
								cert.checkValidity(); // checks the notBefore and notAfter dates
								if (cert.getSubjectDN().toString().contains("O=Let's Encrypt,")) {
									validCert=true;
									break;
								}
							}
						}
					}
					if (!validCert) {
						throw new CertificateException("PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target");
					}
				}
			} };

			SSLContext ctx = SSLContext.getInstance("TLS");
			ctx.init(null, trustAllCerts, null);

			sslContextFactory.setSslContext(ctx);

			SslEngineConfigurator sslEngineConfigurator = new SslEngineConfigurator(ctx, true, false, false);
			sslEngineConfigurator.setHostVerificationEnabled(false);
			sslEngineConfigurator.setClientMode(true);
			sslEngineConfigurator.setWantClientAuth(false);
			sslEngineConfigurator.setNeedClientAuth(false);

			ClientManager client = ClientManager.createClient();
			client.getProperties().put(ClientProperties.SSL_ENGINE_CONFIGURATOR, sslEngineConfigurator);
			client.getProperties().put(ClientProperties.CREDENTIALS, new Credentials("admin", "admin"));

			clientEndpoint = new WSClientEndpoint();

			client.setDefaultMaxSessionIdleTimeout(60000L);

			session = client.connectToServer(clientEndpoint, uri);
        	
			System.out.println("Client connected to VCSS server; sending registration request message");
			log.info("Client connected to VCSS server; sending registration request message");
			
			String uuid;
			if (vcssProperties.containsKey(Constants.OVERRIDE_CLIENT_UUID_KEY)
					&& vcssProperties.get(Constants.OVERRIDE_CLIENT_UUID_KEY).equals("true")) {
				System.out.println("Found overrideClientUUID key in vcssProperties file; using random UUID for client");
				log.info("Found overrideClientUUID key in vcssProperties file; using random UUID for client");
				uuid = UUID.randomUUID().toString();
			} else {
				uuid = getMyMachineUUID();
			}
			
			String hostname = getMyHostname();
			
			//send a registration request to the server
			clientEndpoint.sendRegistrationRequestToServer(uuid, hostname);
			
			log.info("Waiting for message from server indicating registration handshake is complete");
			
			//wait for registration handshake to complete (note this could either mean it succeeded or it failed, but either
			// response message from the server will decrement the latch)
			WSClientEndpoint.getRegistrationLatch().await(); 
			
			//block until socket gets closed
			log.info("Client registered; processing messages until socket closure");	

			//loop as long as the connection stays open (messages will be received/processed by WSClientEndpoint.onMessage() )
			while (session!=null && session.isOpen()) {
				Thread.sleep(CONNECTION_IS_OPEN_POLLING_DELAY_MS);
			}

			//the connection/session has been dropped (for whatever reason)
			log.info("Websocket connection dropped");
			return ;

		} catch (IOException | DeploymentException e) {
			String msg = e.getMessage();
			if (e.getCause() != null) {
				msg += " (" + e.getCause().getMessage() + ")";
			}
			System.out.println("Warning: " + msg);
			log.warning(msg);
		} catch (Exception e) {
			System.out.println("Warning: " + e.getMessage());
			log.warning(e.getMessage());
		} catch (Throwable t) {
			System.out.println("Warning: " + t.getMessage());
			log.warning(t.getMessage());
		}

		return;
		
    }
    /**
     * This method returns the hostname for this machine.
     * @return the hostname for this machine, or null if the host name could not be determined.
     */
	private String getMyHostname() {
		
		String hostname = null;
		
		try {
			//String canonicalHostName = InetAddress.getLocalHost().getCanonicalHostName();
			hostname = InetAddress.getLocalHost().getHostName();
		} catch (UnknownHostException e) {
	        log.info("Unable to determine hostname; returning null"); 
		}

		return hostname;
	}

	/**
	 * This method attempts to obtain the current machine's UUID from the underlying OS. 
	 * If successful, it returns the OS-obtained machine UUID; otherwise, it returns
	 * a randomly-generated UUID.
	 * 
	 * @return a String representing a UUID.
	 */
	private String getMyMachineUUID() {
		
		
		String os = System.getProperty("os.name");
		if (os==null) {
			log.warning("Can't get os.name property from system; returning random UUID");
			return UUID.randomUUID().toString() ;			
		}
		
		os = os.toLowerCase();
		
		String uuid = null;
		if (os.indexOf("win") >= 0) {
			uuid = getWindowsUUID();
		} else if (os.indexOf("mac") >= 0) {
			uuid = getMacUUID();
		} else if (os.indexOf("nix") > 0 || os.indexOf("nux") > 0 || os.indexOf("aix") > 0 ) {
			uuid = getUnixUUID();
		}
		
		if (UUIDValidator.isValidUUID(uuid)) {
			return uuid;
		} else {
			log.warning("Can't get UUID from system; choosing random UUID");
			return UUID.randomUUID().toString();
		}

	}

	/**
	 * Returns a string containing the Unix-based (Linux, AIX, etc) machine UUID, 
	 * or null if the UUID could not be determined.
	 * Note that this method is Unix-like OS-SPECIFIC.
	 * 
	 * @return the underlying MacOS machine's UUID, or null if a valid UUID can't be obtained from the underlying OS.
	 */
	private String getUnixUUID() {
		// Based on: https://stackoverflow.com/questions/49488624/how-to-get-a-computer-specific-id-number-using-java
		// NOTE: must run as root to read this file
		// otherwise you get an exception and the client stops.
	    String command = "cat /sys/class/dmi/id/product_uuid";

		String output = executeCommand(command);
		
		log.info("Output from Unix system request for UUID = '" + output + "'" );

		String uuid = "";
		if (output.trim().equals("")) {
			System.err.println("WARNING: RunVCSSClient must be run as root to determine the UUID");
		} else {
			// the first line is the uuid
			uuid = output.substring(0, output.indexOf("\n")).trim();
			log.info("Unix machine UUID string appears to be: '" + uuid + "'");
		}

	    if (UUIDValidator.isValidUUID(uuid)) {
	    	log.info("Found my Unix machine UUID: " + uuid);
	    	return uuid;
	    } else {
	    	log.info("unable to find Unix machine UUID");
	    	return null;
	    }	}

	/**
	 * Returns a string containing the MacOS machine UUID, or null if the UUID could not be determined.
	 * Note that this method is MacOS-SPECIFIC.
	 * 
	 * @return the underlying MacOS machine's UUID, or null if a valid UUID can't be obtained from the underlying OS.
	 */
	private String getMacUUID() {
		// Based on: https://stackoverflow.com/questions/49488624/how-to-get-a-computer-specific-id-number-using-java

		// the pipe does not work
//	    String command = "system_profiler SPHardwareDataType | awk '/UUID/ { print $3; }'";
	    String command = "system_profiler SPHardwareDataType";

		String output = executeCommand(command);
		// output.length causes us to also get the Activation Lock Status: 
//		String uuid = output.substring(output.indexOf("UUID: "), output.length()).replace("UUID: ", "");
		int indexOfUUID = output.indexOf("UUID: ");
		// find the first \n after the UUID:
		String uuid = output.substring(indexOfUUID, output.indexOf("\n", indexOfUUID)).replace("UUID: ", "");
	    if (UUIDValidator.isValidUUID(uuid)) {
	    	log.info("Found my Mac machine UUID: " + uuid);
	    	return uuid;
	    } else {
	    	log.info("unable to find Mac machine UUID");
	    	return null;
	    }
	}

	/**
	 * Returns a string containing the Windows machine UUID, or null if the UUID could not be determined.
	 * Note that this method is WINDOWS-SPECIFIC.
	 * 
	 * @return the underlying Windows machine's UUID, or null if a valid UUID can't be obtained from the underlying OS.
	 */
	private String getWindowsUUID() {
		// Based on: https://stackoverflow.com/questions/49488624/how-to-get-a-computer-specific-id-number-using-java
		
		String command = "wmic csproduct get UUID";
		
		String output = executeCommand(command);
		
		String uuid = output.substring(output.indexOf("\n"), output.length()).trim();

	    if (UUIDValidator.isValidUUID(uuid)) {
	    	log.info("Found my Windows machine UUID: " + uuid);
	    	return uuid;
	    } else {
	    	log.info("unable to find Windows machine UUID");
	    	return null;
	    }
	}
	
	//TODO: consider whether this method should be replaced by an invocation of "ProgramRunner".
	String executeCommand(String command) {
		// Based on: https://stackoverflow.com/questions/49488624/how-to-get-a-computer-specific-id-number-using-java
		
	    StringBuffer output = new StringBuffer();

	    Process proc=null;
		try {
			proc = Runtime.getRuntime().exec(command);
			BufferedReader reader = new BufferedReader(new InputStreamReader(proc.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				output.append(line + "\n");
			}
			
			//wait for the OS process to terminate
			proc.waitFor();
			
			reader.close();
			
		} catch (IOException e) {
			log.log(Level.WARNING, "Error running command "+e.getMessage(), e);
		} catch (InterruptedException e) {
			log.log(Level.WARNING, "Error running command "+e.getMessage(), e);
		}
		
		return output.toString();
		
	}

	private void usage() {
		System.out.println ("Usage:");
		System.out.println ("   java RunVCSSClient <vcssServerURL>");
		System.out.println ("where");
		System.out.println ("   <vcssServerURL> is the URL of the VCSS server which this client machine should contact");
	}
	
	/**
	 * Validate that args[0] contains a valid URL.
	 * @param args a String array of arguments
	 * @return true if arg[0] contains a valid URL; false otherwise.
	 */
	private boolean validateArgs(String [] args) {
		//note that "log" cannot be used here because this method is called before the log has been created...
		System.out.println ("validateArgs() not implemented; assuming arg[0] is a valid URL");
		return true;
	}

	/**
	 * Loads properties from the specified file and returns them in a Properties
	 * object.
	 * 
	 * @param filename - the file to load from
	 * @return a Properties object containing the Properties given in the file
	 */

	protected Properties loadPropertiesFile(String filename) {

		Properties properties = new Properties();
		try {
			FileReader reader = new FileReader(new File(filename));
			properties.load(reader);
			
			// debug:
			String choice ;
			if (properties.size()==1) {
				choice = "property";
			} else {
				choice = "properties";
			}
			System.out.println("Loaded " + properties.size() + " " + choice + " from '" + filename + "'");
			
		} catch (Exception e) {
			log.info("Exception loading properties file '" + filename + "':  " + e);
		}

		return properties;
	}
	private void showMessage(String message) {
		log.info(message);
	}
	
	/**
	 * This method returns a JSON String containing all the key/value pairs in the specified Properties object, 
	 * but replaces the value of any property whose key contains "password" with asterisks.
	 * 
	 * The specified Properties object is assumed to map Strings to Strings.
	 * 
	 * @param props a Properties object mapping String keys to String values which is to have passwords filtered out of values.
	 * 
	 * @return a String in JSON form containing the key/value pairs in the specified Properties, but with no passwords visible.
	 */
	private String filterPasswords(Properties props) {

		String retStr = "{" ;

		Set<Object> keys = props.keySet();
		boolean keysHaveBeenAdded = false;

		for (Object key : keys) {

			if (key instanceof String) {

				//if we've already added some properties (and we're about to add another one), add a comma
				if (keysHaveBeenAdded) {
					retStr += ", ";
				}

				//put the key into the return string, followed by "="
				String keyString = (String)key;
				retStr += keyString + "=";

				//get the value associated with the key
				String value = props.getProperty(keyString);

				if (value == null) {
					retStr += "null";
				} else if (keyString.contains("password")) {
						retStr += "******";
					} else {
						retStr += value;
					}

				keysHaveBeenAdded = true;
			}
		}

		retStr += "}";

		return retStr;
	}


	/**
	 * Returns the static (class-wide) log for this VCSS client.
	 * 
	 * @return the client {@link Log}.
	 */
	public static Log getLog() {
		return log;
	}
	
	/**
	 * This is the main program for starting the client-side VCSS system. 
	 * @param args[0] - the URL to which this class should establish a websocket.
	 */
    public static void main(String[] args) {
    	
    	RunVCSSClient runner = new RunVCSSClient();
    	runner.start(args);
    }

}
