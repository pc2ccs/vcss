package org.icpc.vcss.core.commands;

/**
 * This enum defines the status values which a {@link MachineCommandResult} can have.
 * 
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
public enum MachineCommandResultStatusType {

    /**
     * Indicates an unknown status type.
     */
    UNDEFINED("undefined"),
    
    /**
     * Indicates that a command was sent to a machine but no response has yet been received.
     */
    PENDING("pending"),
    
    /**
     * Indicates that a command was reported as successfully completed by a machine.
     */
    SUCCESS("success"),
    
    /**
     * Indicates that a command which was sent to a machine was reported to have failed.
     */
    FAILED("failed");
    

    private String textName = "undefined";
    
    
    /**
     * Set override text name/description for enum element during element construction.
     */
    MachineCommandResultStatusType(String textName) {
        this.textName = textName;
    }
    
    /**
     * Get an enum element based on its textName.
     * 
     * @return an element of this MachineCommandResultStatusType if the specified text String
     * 			matches (ignoring case) the text name of one of the enum elements, or null if
     * 			no enum text name matches.
     */
    public static MachineCommandResultStatusType fromString(String text) {
        for (MachineCommandResultStatusType b : MachineCommandResultStatusType.values()) {
            if (b.textName.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }
    
    @Override
    public String toString() {
        return textName;
    }

}
