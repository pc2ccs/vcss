// Copyright (C) 2021 ICPC VCSS Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.core.commands;

/**
 * This enum defines the types of commands which can be sent from the VCSS server to client machines.
 * 
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 */
public enum CommandType {

    /**
     * Indicates an unknown command type.
     */
    UNKNOWN("unknown"),

    /**
     * A command to be executed by the client machine.
     */
    EXECUTE("execute"),
    
    /**
     * A command asking the client to save ("put") a file on the client.
     */
    PUT("put"),
    
    /**
     * A command asking the client to return ("get") a file from the client.
     */
    GET("get");
    

    private String textName = "Unknown";
    
    
    /**
     * Set override text name/description for enum element during element construction.
     */
    CommandType(String textName) {
        this.textName = textName;
    }
    
    /**
     * Get an enum element based on its textName.
     */
    public static CommandType fromString(String text) {
        for (CommandType b : CommandType.values()) {
            if (b.textName.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }
    
    @Override
    public String toString() {
        return textName;
    }

}
