package org.icpc.vcss.core.commands;

/**
 * This enum defines the status values which a {@link Command} can have.
 * 
 * @author John Clevenger, ICPC VCSS Development Team (pc2@ecs.csus.edu)
 *
 */
public enum CommandStatusType {

    /**
     * Indicates an unknown status type.
     */
    UNDEFINED("undefined"),
    
    /**
     * Indicates that a command is newly-received and has not been processed (sent to any clients) yet.
     */
    NEW("new"),
    
    /**
     * Indicates that a command has been sent to its target machines but no responses have yet been received.
     */
    PENDING("pending"),
    
    /**
     * Indicates that a command has received responses from SOME of the machines to which it
     * was sent, but not from ALL of those machines.
     */
    PARTIALlY_COMPLETED("partially_completed"),
    
    /**
     * Indicates that a reply to a command has been received from all machines to which it was sent.
     * Note that this does not imply that the command completed *successfully* on all machines;
     * to check that condition, look at the {@link MachineCommandResultStatusType} in the
     * {@link MachineCommandResult} for each machine to which the command was sent.
     */
    COMPLETED("completed");
    

    private String textName = "undefined";
    
    
    /**
     * Set override text name/description for enum element during element construction.
     */
    CommandStatusType(String textName) {
        this.textName = textName;
    }
    
    /**
     * Get an enum element based on its textName.
     * 
     * @return an element of this CommandStatusType if the specified text String
     * 			matches (ignoring case) the text name of one of the enum elements, or null if
     * 			no enum text name matches.
     */
    public static CommandStatusType fromString(String text) {
        for (CommandStatusType b : CommandStatusType.values()) {
            if (b.textName.equalsIgnoreCase(text)) {
                return b;
            }
        }
        return null;
    }
    
    @Override
    public String toString() {
        return textName;
    }

}
