// Copyright (C) 1989-2019 PC2 Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.core.log;

/**
 * Listener for all StreamListeners.
 *
 * @author pc2@ecs.csus.edu
 */

public interface IStreamListener {

    void messageAdded(String inString);
}
