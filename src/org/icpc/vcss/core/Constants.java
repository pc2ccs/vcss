// Copyright (C) 1989-2020 PC2 Development Team: John Clevenger, Douglas Lane, Samir Ashoo, and Troy Boudreau.
package org.icpc.vcss.core;

/**
 * Constants used by multiple classes.
 *
 * @author Troy Boudreau  <pc2@ecs.csus.edu>
 */
public class Constants {
	/**
	 * The subjectDN of the cert that WebServer generates, and RunVCSSClient expects.
	 */
	public final static String SUBJECT_DN = "CN=vcss jetty, OU=VCSS, O=ICPC, L=Unknown, ST=Unknown, C=Unknown";
	/**
	 * Filename that contains the properties for vcss server and clients.
	 */
	public static final String VCSS_PROPERTIES_FILENAME = "vcss.properties";

	/**
	 * The domain name which all VCSS "cloud machines" FQDN's are expected to end with.
	 */
	//TODO: this should probably be a property read from vcss.properties
	public static final String CLOUD_DOMAIN_NAME = "amazonaws.com";

	/**
	 * The command which needs to be executed on a cloud machine to create a new user account.
	 * Note that code using this is expected to perform "variable substitution" for the values
	 * of "username" (the login name for the account to be created) and "password".
	 * TODO: the password really should be encrypted... but then that raises the question of
	 * decrypting it on the client...
	 */

	public static final String CLOUD_CREATE_USER_COMMAND = "useradd -m -p {password} --gid teams --shell /bin/bash {username}";

	/**
	 * Environment variable prefix that can be used to override properties
	 */
	public final static String ENV_PREFIX = "VCSS";

	public static final String OVERRIDE_CLIENT_UUID_KEY = "overrideClientUUID";

	public static final String SAVED_COMMAND_FOLDER_NAME = "Commands";
	public static final String SAVED_COMMAND_FILENAME_EXTENSION = "txt";
	public static final String SAVED_RESULT_FOLDER_NAME = "CommandResults";
	public static final String SAVED_RESULT_FILENAME_EXTENSION = "results.txt";

	public static final String AWS_CONTEST_NAME_KEY = "contestName";
	public static final String AWS_CONTEST_ID_KEY = "icpc-contest-id";
	public static final String AWS_VPC_ID_KEY = "vpcId";
	public static final String AWS_SUBNET_ID_KEY = "subnetId";
	public static final String AWS_KEYPAIR_ID_KEY = "keypairId";
	public static final String AWS_KEYPAIR_NAME_KEY = "keypairName";
	public static final String AWS_KEYPAIR_PRIVATE_KEY_KEY = "keypairPrivateKey";
	public static final String AWS_SECURITYGROUP_ID_KEY = "securityGroupId";
	public static final String AWS_AMI_ID_KEY = "amiId";

	/**
	 * Turn on console logging
	 */
	public static final String LOGGER_CONSOLE_KEY = "consolelogging";

//	public static final String AWS_AMI_ID = "ami-0479e1f5c848268f9";

}
