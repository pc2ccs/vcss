#!/bin/bash

# update the values as needed

echo "Refreshing 'admin2.icpc-vcss.org.keystore'"

openssl pkcs12 -export \
  -in /etc/letsencrypt/live/admin2.icpc-vcss.org/cert.pem \
  -inkey /etc/letsencrypt/live/admin2.icpc-vcss.org/privkey.pem \
  -out /tmp/admin2.icpc-vcss.org.p12 \
  -name admin2.icpc-vcss.org \
  -CAfile /etc/letsencrypt/live/admin2.icpc-vcss.org/fullchain.pem \
  -caname "Let's Encrypt Authority X3" \
  -password pass:"i don't care"

keytool -importkeystore \
	-deststorepass "i don't care" \
	-destkeypass "i don't care" \
	-deststoretype pkcs12 \
	-srckeystore /tmp/admin2.icpc-vcss.org.p12 \
	-srcstoretype PKCS12 \
	-srcstorepass "i don't care" \
	-destkeystore /tmp/admin2.icpc-vcss.org.keystore \
	-alias admin2.icpc-vcss.org
