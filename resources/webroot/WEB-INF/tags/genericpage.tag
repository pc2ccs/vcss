<%@tag description="main template" pageEncoding="UTF-8" %>
<%@attribute name="title" fragment="true" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>
<%@attribute name="menu" fragment="true" %>

<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>VCSS - <jsp:invoke fragment="title" /></title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <!-- <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css"> -->
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/plugins/fontawesome-free/css/all.min.css"/>
  <!-- Theme style -->
  <!-- <link rel="stylesheet" href="${pageContext.request.contextPath}/css/adminlte.min.css"> -->
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/adminlte.min.css"/>

  <!-- custom vcss stylesheet -->
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/vcss.css"/>

  <!-- jQuery -->
  <script src="${pageContext.request.contextPath}/plugins/jquery/jquery.min.js"></script>
  <jsp:invoke fragment="header" />
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="${pageContext.request.contextPath}/" class="nav-link">Virtual Contest Support System</a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <% if (request.getRemoteUser() == null) { %>
        <li class="nav-item dropdown">
          <a class="nav-link" href="${pageContext.request.contextPath}/login">
            <i class="fas fa-sign-in-alt"></i>&nbsp;&nbsp;Login
          </a>
        </li>
      <% } else { %>
        <li class="nav-item dropdown">
          <a class="nav-link" data-toggle="dropdown" href="#">
            <i class="fas fa-user"></i>&nbsp;&nbsp;
            <%= request.getRemoteUser() %>
          </a>
          <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
            <span class="dropdown-item dropdown-header">Role: </span>
            <div class="dropdown-divider"></div>
            <a href="${pageContext.request.contextPath}/logout" class="dropdown-item dropdown-footer">
              <i class="fas fa-sign-out-alt"></i>&nbsp;&nbsp;Logout
            </a>
          </div>
        </li>
      <% } %>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="${pageContext.request.contextPath}/" class="brand-link">
      <img src="${pageContext.request.contextPath}/icon/vcss.png" alt="vcss Logo" class="brand-image img-circle" style="opacity: .8">
      <span class="brand-text font-weight-light">VCSS</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">

        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <jsp:invoke fragment="menu" />
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><jsp:invoke fragment="title" /></h1>
          </div><!-- /.col -->

          <!-- breadcrumbs, right hand side-->
          <!--
            <div class="col-sm-6">
              <ol class="breadcrumb float-sm-right">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Starter Page</li>
              </ol>
            </div>
          -->

        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <div class="content">
      <jsp:doBody/>
    </div>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
    <div class="p-3">
      <h5>Title</h5>
      <p>Sidebar content</p>
    </div>
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      <!--  Anything you want -->
    </div>
    <!-- Default to the left -->
    <strong>Copyright (C) 2021 ICPC VCSS Development Team v0.94</strong>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- Bootstrap 4 -->
<script src="${pageContext.request.contextPath}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="${pageContext.request.contextPath}/js/adminlte.min.js"></script>
<jsp:invoke fragment="footer" />
</body>
</html>
