<%@tag description="public page template" pageEncoding="UTF-8" %>
<%@attribute name="title" fragment="true" %>
<%@attribute name="header" fragment="true" %>
<%@attribute name="footer" fragment="true" %>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>


<%
    String[] menuPages = {"/public/", "/public/cloudmachine/"};
    String[] menuTitles = {"Overview", "Control Cloud Machine"};
    String[] menuIcons = {"fa-info", "fa-desktop"};
%>

<c:set var="menu">
<% for (int i = 0; i < menuPages.length; i++) { %>
<li class="nav-item">
  <a href="${pageContext.request.contextPath}<%= menuPages[i] %>"
    class="nav-link<% if (request.getAttribute("javax.servlet.forward.request_uri").equals(menuPages[i])) { %> active<% } %>">
    <i class="nav-icon fas <%= menuIcons[i] %>"></i>
    <p><%= menuTitles[i] %></p>
  </a>
</li>
<% } %>
</c:set>

<t:genericpage>
  <jsp:attribute name="header"><jsp:invoke fragment="header" /></jsp:attribute>
  <jsp:attribute name="footer"><jsp:invoke fragment="footer" /></jsp:attribute>
  <jsp:attribute name="title"><jsp:invoke fragment="title" /></jsp:attribute>
  <jsp:attribute name="menu">${menu}</jsp:attribute>
  <jsp:body><jsp:doBody/></jsp:body>
</t:genericpage>
