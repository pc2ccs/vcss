<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>
<t:publicpage>
  <jsp:attribute name="title">Welcome</jsp:attribute>
  <jsp:body>
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-6 -->
        <div class="col-lg-6">
          <div class="card">
            <div class="card-header">
              <h5 class="m-0">Welcome</h5>
            </div>
            <div class="card-body">
              <p>Welcome to the Virtual Contest Support System(VCSS)!</p>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </jsp:body>
</t:publicpage>
