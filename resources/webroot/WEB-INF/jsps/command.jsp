
<%@page import="java.util.List" %>
<%@page import="java.util.Map" %>
<%@page import="org.icpc.vcss.entity.Command" %>
<%@page import="org.icpc.vcss.entity.GetCommand" %>
<%@page import="org.icpc.vcss.entity.PutCommand" %>
<%@page import="org.icpc.vcss.entity.ExecuteCommand" %>
<%
    Map<String,Object> model = (Map<String,Object>)request.getAttribute("model");
    Command command = (Command) model.get("command");
    request.setAttribute("title", "Command Details");
%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<c:set var="bodyContent">

  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Command Details</h3>
          </div>
          <div class="card-body">
            <table class="table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Text</th>
                  <th>Status</th>
                  <th>Created</th>
                </tr>
              </thead>
              <% Command c = command; %>
              <tr>
                <td><a href="${pageContext.request.contextPath}/admin/commands/<%= c.getId() %>/results"><%= c.getId() %></a></td>
                <% if (c instanceof PutCommand) { PutCommand pc = (PutCommand) c; %>
                  <td>
                    <dl>
                      <dt>Source</dt><dd><%= pc.getSourceFilePath() %></dd>
                      <dt>Destination</dt><dd><%= pc.getDestFilePath() %></dd>
                      <dt>Filemode</dt><dd><%= pc.getFilemode() %></dd>
                      <dt>Owner</dt><dd><%= pc.getOwner() %></dd>
                    </dl>
                  </td>
                <% } else if (c instanceof GetCommand) { GetCommand gc = (GetCommand) c; %>
                  <td><%= gc.getText() %></td>
                <% } else if (c instanceof ExecuteCommand) { ExecuteCommand ec = (ExecuteCommand) c; %>
                  <td><code><%= ec.getText() %></code></td>
                <% } %>
                <td><%= c.getStatus() %></td>
                <td><%= c.getCreationDate() %></td>
              </tr>
            </table>
          </div><!-- /.card -->
        </div>
      </div> <!-- /.col-lg-12 -->
    </div> <!-- /.row -->
  </div><!-- /.container-fluid -->

</c:set>

<t:adminpage>
  <jsp:attribute name="title">Command Details</jsp:attribute>
  <jsp:attribute name="footer"></jsp:attribute>
  <jsp:body>${bodyContent}</jsp:body>
</t:adminpage>
