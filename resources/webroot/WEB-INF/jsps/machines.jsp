<%@page import="java.util.List" %>
<%@page import="java.util.Map" %>
<%@page import="org.icpc.vcss.entity.Machine" %>
<%
    Map<String,Object> model = (Map<String,Object>)request.getAttribute("model");
    List<Machine> machines = (List<Machine>) model.get("machines");
%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<c:set var="bodyContent">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-12">

        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Machines List</h3>
          </div>
          <div class="card-body">
              <table class="table">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Hostname</th>
                    <th>IP Address</th>
                    <th>State</th>
                    <th>Registered</th>
                    <th>Connected</th>
                    <th>UUID</th>
                  </tr>
                </thead>
                <% for (Machine m : machines) { %>
                  <tr>
                      <td><%= m.getId() %></td>
                      <td><%= m.getName() %></td>
                      <td><%= m.getHostname()!=null ? m.getHostname() : "-unassigned-" %></td>
                      <td><%= m.getIpAddr()!=null ? m.getIpAddr() : "-unassigned-" %></td>
                      <td><%= m.getState() %></td>
                      <td><%= m.isRegistered() %></td>
                      <td><%= m.isConnected() %></td>
                      <td><%= m.getUuid()!=null ? m.getUuid() : "-unassigned-" %></td>
                    </tr>
                <% } %>
              </table>
          </div>
        </div><!-- /.card -->

      </div> <!-- /.col-lg-12 -->
    </div> <!-- /.row -->
  </div><!-- /.container-fluid -->
</c:set>

<t:adminpage>
  <jsp:attribute name="title">Machines</jsp:attribute>
  <jsp:attribute name="header"></jsp:attribute>
  <jsp:attribute name="footer"></jsp:attribute>
  <jsp:body>${bodyContent}</jsp:body>
</t:adminpage>
