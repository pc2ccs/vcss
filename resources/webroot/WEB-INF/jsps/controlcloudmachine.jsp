
<%@page import="java.util.List" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.Collections" %>
<%@page import="org.icpc.vcss.entity.Command" %>
<%@page import="org.icpc.vcss.entity.Machine" %>
<%@page import="org.icpc.vcss.entity.GetCommand" %>
<%@page import="org.icpc.vcss.entity.PutCommand" %>
<%@page import="org.icpc.vcss.entity.ExecuteCommand" %>
<%
    Map<String,Object> model = (Map<String,Object>)request.getAttribute("model");
    List<Machine> cloudMachines = (List<Machine>) model.get("cloudMachines");
    boolean enforceAutoShutoff = ((String) (model.get("enforceAutoShutoff"))).equalsIgnoreCase("true");
%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<c:set var="bodyContent">
      <div class="container-fluid">
        <div class="row">
          <div class="col-lg-6">
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Start Cloud Machine</h3>
              </div>
              <form class="form" action="${pageContext.request.contextPath}/public/startmachine" method="POST">
                <div class="card-body">
                  <p>Enter your account login details in order to power on your cloud machine.</p>

                    <div class="form-group">
                      <label for="machines">Select cloud machine:</label>
                      <select class="form-control" name="machineId" id="machineId" required>
                        <% if (cloudMachines.size() == 0) { %>
                          <option value="none">No cloud machines found</option>
                        <% } else { %>
                          <% for (Machine m : cloudMachines) { %>
                            <option value="<%= m.getId() %>"><%= m.getName() %></option>
                          <% } %>
                        <% } %>
                      </select>
                    </div>

                    <div class="form-group">
                      <label for="password">Enter your Cloud VM password:</label>
                      <input type="password" class="form-control" name="password" id="password" placeholder="team password'" required>
                    </div>

                    <div class="form-group">
                      <label for="machines">Desired Run Time:</label>
                      <select class="form-control" name="runtime" id="runtime" required>
                        <% if (enforceAutoShutoff) { %>
                        	<option value="5">5 minutes</option>
                        	<option value="30">30 minutes</option>
                        	<option value="60" selected>1 hour</option>
                        	<option value="120">2 hours</option>
                        	<option value="180">3 hours</option>
                        <% } else { %>
                         	<option value="-1">Until End of Contest</option>
                        <% } %>
                      </select>
                    </div>

                    <div class="form-group">
                      <button type="submit" class="btn btn-primary" id="submit" name="submit">Launch Machine</button>
                    </div>
                </div>
              </form>
            </div><!-- /.card -->
          </div><!-- .col-lg-6 -->


          <div class="col-lg-6">
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Cloud Machine Information</h3>
              </div>
              <div class="card-body">
                <p>
                  VCSS provides cloud machines for contestants and contest staff to use. These machines will be available during the contest, but if you would like
                  to test out the environment ahead of time you can use this interface to boot up your cloud machine.
                </p>
                <p>
                  For "Cloud VM password", any team member's Cloud VM password can be used.
                </p>
                <div class="callout callout-info">
                  <h5>Note:</h5>
                  Since these machines are for testing only, they will automatically be powered off after the selected timelimit in order to control costs.
                </div>
              </div>
            </div><!-- /.card -->
          </div><!-- .col-lg-6 -->

          <div class="col-lg-12">
            <div class="card card-default">
              <div class="card-header">
                <h3 class="card-title">Cloud Machine Status</h3>
              </div>
              <div class="card-body">
                <table class="table">
                  <thead>
                    <tr>
                      <th>Team</th>
                      <th>Status</th>
                    </tr>
                  </thead>
                  <% for (Machine m : cloudMachines) { %>
                    <tr>
                      <td><%= m.getName() %></td>
                      <td><%= m.getName() %></td>
                    </tr>
                  <% } %>
                </table>
              </div><!-- /.card -->
            </div>
          </div> <!-- /.col-lg-12 -->
        </div> <!-- /.row -->
      </div><!-- /.container-fluid -->
</c:set>

<t:publicpage>
  <jsp:attribute name="title">Cloud Machine Control</jsp:attribute>
  <jsp:attribute name="header"></jsp:attribute>
  <jsp:attribute name="footer"></jsp:attribute>
  <jsp:body>${bodyContent}</jsp:body>
</t:publicpage>
