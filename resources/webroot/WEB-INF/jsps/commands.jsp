
<%@page import="java.util.List" %>
<%@page import="java.util.Map" %>
<%@page import="java.util.Collections" %>
<%@page import="org.icpc.vcss.entity.Command" %>
<%@page import="org.icpc.vcss.entity.Machine" %>
<%@page import="org.icpc.vcss.entity.GetCommand" %>
<%@page import="org.icpc.vcss.entity.PutCommand" %>
<%@page import="org.icpc.vcss.entity.ExecuteCommand" %>
<%
    Map<String,Object> model = (Map<String,Object>)request.getAttribute("model");
    List<Command> commands = (List<Command>) model.get("commands");
    List<Machine> connectedMachines = (List<Machine>) model.get("connectedMachines");

    // Reverse the list of commands so they're in descending order
    Collections.reverse(commands);
%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="t" tagdir="/WEB-INF/tags" %>

<c:set var="bodyContent">
  <div class="container-fluid">
    <div class="row">
      <div class="col-lg-6">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Send Command</h3>
          </div>
          <form class="form" action="${pageContext.request.contextPath}/admin/dispatchCommand" method="POST">
            <div class="card-body">
              <p>This allows you to select a set of machines and send a command (such as 'execute' or 'put') to them.</p>

                <div class="form-group">
                  <label for="machines">Select target machine(s): <span id="machine_selected_count"></span></label>
                  <select style="height:173px" class="form-control" multiple name="machines[]" id="machines[]" required>
                    <% if (connectedMachines.size() == 0) { %>
                      <option value="none">No machines currently connected</option>

                    <% } else { %>
                      <% for (Machine m : connectedMachines) { %>
                        <option value="<%= m.getUuid() %>"><%= m.getName() + " : " + m.getHostname() + "@" + m.getIpAddr() + " (" + m.getUuid() + ")" %></option>
                      }
                      <% } %>
                    <% } %>
                  </select>
                </div>

                <div class="form-group">
                  <label for="command">Enter command to be sent to target machines:</label>
                  <input type="text" class="form-control" name="command" id="command" placholder="e.g. 'execute uptime'" required>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary" id="submit" name="submit">Send command to target(s)</button>
                </div>
            </div>
            <!-- <div class="card-footer">
              <button type="submit" class="btn btn-primary" id="submit" name="submit">Send command to target(s)</button>
            </div> -->
          </form>
        </div><!-- /.card -->
      </div><!-- .col-lg-6 -->

      <div class="col-lg-6">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Command Formats</h3>
          </div>
          <div class="card-body">
            <ul>
              <li>
                <b>Execute Command</b>
                <p>
                  <code>execute remoteCommandLine</code><br>
                  (where 'remoteCommandLine' is a command you want executed on the selected machines, including any command line arguments)
                </p>
              </li>
              <li>
                <b>Put Command</b>
                <p>
                  <code>put localFileName remotePath fileMode fileOwner</code><br>
                  where <code>localFileName</code> is a file on this machine, <code>remotePath</code> is the absolute path where the file is to be put on each target machine, <code>fileMode</code> is a Unix-style (octal) mode, and <code>fileOwner</code> is the file owner to be assigned on the target machine)
                </p>
                <div class="callout callout-info">
                  <h5>Note:</h5>
                  The destination directory specified in a <code>put</code> command must already exist on the client machine;
                  use the <code>execute</code> command to send a <code>mkdir -p full_dir_path</code> command, possibly with additional arguments to set file mode/owner,
                  prior to sending a <code>put</code> command to a client.

                </div>
              </li>
            </ul>
          </div>
        </div><!-- /.card -->
      </div><!-- .col-lg-6 -->

      <div class="col-lg-12">
        <div class="card card-default">
          <div class="card-header">
            <h3 class="card-title">Command List</h3>
          </div>
          <div class="card-body">
            <table class="table">
              <thead>
                <tr>
                  <th>ID</th>
                  <th>Text</th>
                  <th>Status</th>
                  <th>Created</th>
                </tr>
              </thead>
              <% for (Command c : commands) { %>
                <tr>
                    <td><a href="${pageContext.request.contextPath}/admin/commands/<%= c.getId() %>/results"><%= c.getId() %></a></td>
                    <% if (c instanceof PutCommand) { PutCommand pc = (PutCommand) c; %>
                      <td>
                        <dl>
                          <dt>Source</dt><dd><%= pc.getSourceFilePath() %></dd>
                          <dt>Destination</dt><dd><%= pc.getDestFilePath() %></dd>
                          <dt>Filemode</dt><dd><%= pc.getFilemode() %></dd>
                          <dt>Owner</dt><dd><%= pc.getOwner() %></dd>
                        </dl>
                      </td>
                    <% } else if (c instanceof GetCommand) { GetCommand gc = (GetCommand) c; %>
                      <td><%= gc.getText() %></td>
                    <% } else if (c instanceof ExecuteCommand) { ExecuteCommand ec = (ExecuteCommand) c; %>
                      <td><code><%= ec.getText() %></code></td>
                    <% } %>
                    <td><%= c.getStatus() %></td>
                    <td><%= c.getCreationDate() %></td>
                  </tr>
              <% } %>
            </table>
          </div><!-- /.card -->
        </div>
      </div> <!-- /.col-lg-12 -->
    </div> <!-- /.row -->
  </div><!-- /.container-fluid -->
</c:set>

<t:adminpage>
  <jsp:attribute name="title">Commands</jsp:attribute>
  <jsp:attribute name="header">
    <script type="text/javascript">
      function updateMachineCount() {
          element = $('#machines\\[\\]');
          total_items = element.find('option[value!="none"]').length;
          selected_items = element.find('option:selected[value!="none"]').length;
          $("#machine_selected_count").text(selected_items + "/"+ total_items + " machines selected");
      }
      $(document).ready(function () {
          $('#machines\\[\\]').on('change', function() {
              updateMachineCount();
          });
          updateMachineCount();
      });
  </script>
  </jsp:attribute>
  <jsp:attribute name="footer"></jsp:attribute>
  <jsp:body>${bodyContent}</jsp:body>
</t:adminpage>
